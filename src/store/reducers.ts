/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from '@reduxjs/toolkit';
import { InjectedReducersType } from 'utils/types/injector-typings';
import { signInReducer } from 'app/pages/user/authentication/screen/reducer';
import { adminListUserReducer } from 'app/pages/admin/admin-user-management/screen/reducer';
import { adminDetailUserReducer } from 'app/pages/admin/admin-user-detail-page/screen/reducer';
import { adminCreateUserReducer } from 'app/pages/admin/admin-user-create-page/screen/reducer';
import { adminUpdateUserReducer } from 'app/pages/admin/admin-user-update-page/screen/reducer';
import { adminCreateProductReducer } from 'app/pages/admin/admin-product-create-page/screen/reducer';
import { adminListProductReducer } from 'app/pages/admin/admin-product-list-page/screen/reducer';
import { adminUpdateProductReducer } from 'app/pages/admin/admin-product-update-page/screen/reducer';
import { adminListOrderReducer } from 'app/pages/admin/admin-order-list-page/screen/reducer';
import { adminDetailOrderReducer } from 'app/pages/admin/admin-order-detail-page/screen/reducer';
import { userCartReducer } from 'app/pages/user/shopping-cart/screen/reducer';
import { categoryListReducer } from 'app/pages/user/home-page/screen/reducer';
import { productListReducer } from 'app/pages/user/home-page/screen/reducer';
import { productDetailReducer } from 'app/pages/user/product-detail-page/screen/reducer';
import { myAcccountReducer } from 'app/pages/user/my-account-page/screen/reducer';
import { categoryProductReducer } from 'app/pages/user/product-page/screen/reducer';
import { keywordProductReducer } from 'app/pages/user/product-page/screen/reducer';
import { createCartItemReducer } from 'app/pages/user/product-detail-page/screen/reducer';
/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export function createReducer(injectedReducers: InjectedReducersType = {}) {
  // Initially we don't have any injectedReducers, so returning identity function to avoid the error
  // if (Object.keys(injectedReducers).length === 0) {
  //   return (state) => state;
  // } else {
  return combineReducers({
    ...injectedReducers,
    signInReducer,
    adminListUserReducer,
    adminDetailUserReducer,
    adminCreateUserReducer,
    adminUpdateUserReducer,
    adminCreateProductReducer,
    adminListProductReducer,
    adminUpdateProductReducer,
    adminListOrderReducer,
    adminDetailOrderReducer,
    userCartReducer,
    categoryListReducer,
    productListReducer,
    productDetailReducer,
    myAcccountReducer,
    categoryProductReducer,
    keywordProductReducer,
    createCartItemReducer
  });
  // }
}
