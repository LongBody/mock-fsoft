import { all } from 'redux-saga/effects';
import signInSagas from 'app/pages/user/authentication/screen/saga';
import adminListUserSagas from 'app/pages/admin/admin-user-management/screen/saga';
import adminDetailUserSagas from 'app/pages/admin/admin-user-detail-page/screen/saga';
import adminCreateUserSagas from 'app/pages/admin/admin-user-create-page/screen/saga';
import adminUpdateUserSagas from 'app/pages/admin/admin-user-update-page/screen/saga';
import adminCreateProductSagas from 'app/pages/admin/admin-product-create-page/screen/saga';
import adminListProductSagas from 'app/pages/admin/admin-product-list-page/screen/saga';
import adminUpdateProductSagas from 'app/pages/admin/admin-product-update-page/screen/saga';
import adminListOrderSagas from 'app/pages/admin/admin-order-list-page/screen/saga';
import adminDetailOrderSagas from 'app/pages/admin/admin-order-detail-page/screen/saga';
import userCartSagas from 'app/pages/user/shopping-cart/screen/saga';
import userHomePageSaga from 'app/pages/user/home-page/screen/saga';
import myAccountSagas from 'app/pages/user/my-account-page/screen/saga';
import userProductDetailSaga from 'app/pages/user/product-detail-page/screen/saga';
import searchingProductSaga from 'app/pages/user/product-page/screen/saga';

export default function* rootSaga() {
  yield all([
    signInSagas(),
    adminListUserSagas(),
    adminDetailUserSagas(),
    adminCreateUserSagas(),
    adminUpdateUserSagas(),
    adminCreateProductSagas(),
    adminListProductSagas(),
    adminUpdateProductSagas(),
    adminListOrderSagas(),
    adminDetailOrderSagas(),
    userCartSagas(),
    userHomePageSaga(),
    myAccountSagas(),
    userProductDetailSaga(),
    searchingProductSaga()
  ]);
}
