export interface RootState {
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
  signInReducer?: any;
  adminListUserReducer?: any;
  adminDetailUserReducer?: any;
  adminCreateUserReducer?: any;
  adminUpdateUserReducer?: any;
  adminCreateProductReducer?: any;
  adminListProductReducer?: any;
  adminUpdateProductReducer?: any;
  adminListOrderReducer?: any;
  adminDetailOrderReducer?: any;
  userCartReducer?: any;
  categoryListReducer?: any;
  productListReducer?: any;
  productDetailReducer?: any;
  myAcccountReducer?: any;
  categoryProductReducer?: any;
  keywordProductReducer?: any;
  createCartItemReducer?: any;
}
