import axios from 'axios';

//apply base url for axios
export const API_URL = process.env.REACT_APP_DEFAULT_URL_API;

const axiosApi = axios.create({
  baseURL: API_URL,
});

axios.interceptors.request.use(function (config) {
  return config;
});

axiosApi.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject(error)
);

export async function get(url, config) {
  return await axiosApi
    .get(url, {
      ...config,
    })
    .then((response) => response.data);
}





