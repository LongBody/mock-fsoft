import axios from 'axios';
import { notification } from 'antd';
import { API_URL } from 'utils/config';

export class ResponseError extends Error {
  public response: Response;

  constructor(response: Response) {
    super(response.statusText);
    this.response = response;
  }
}
/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response: Response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response: Response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new ResponseError(response);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export async function request(
  url: string,
  options?: RequestInit
): Promise<{} | { err: ResponseError }> {
  const fetchResponse = await fetch(url, options);
  const response = checkStatus(fetchResponse);
  return parseJSON(response);
}

export const apiMethod = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  HEAD: 'HEAD',
  DELETE: 'DELETE',
  PATCH: 'PATCH',
  OPTIONS: 'OPTIONS',
};

export const getCookie = (name: string) => {
  const value = `; ${document.cookie}`;
  const parts: any = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
};

export const setCookie = (name: string, value: any, days: any) => {
  var expires = '';
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = '; expires=' + date.toUTCString();
  }
  document.cookie = name + '=' + (value || '') + expires + '; path=/';
};

export const eraseCookie = (name) => {
  document.cookie = name + '=; Max-Age=-99999999;';
};

export function requestAPI(URL: string, method: string, value: any) {
  // `axios` function returns promise, you can use any ajax lib, which can
  // return promise, or wrap in promise ajax call

  if (method === apiMethod.GET) {
    return axios.get(URL, { params: value });
  }
  if (method === apiMethod.POST) {
    return axios.post(URL, value);
  }
  if (method === apiMethod.PATCH) {
    return axios.patch(URL, value);
  }
  if (method === apiMethod.PUT) {
    return axios.put(URL, value);
  }
  if (method === apiMethod.DELETE) {
    return axios.delete(URL, {
      data: value,
    });
  }
}

// const token = getCookie('token');
const token: any = localStorage.getItem('accessToken');
const accessToken = atob(token);

const axiosInstance = axios.create({
  headers: { Authorization: `Bearer ${accessToken}` },
});

const openNotificationWithIcon = (type: any) => {
  notification[type]({
    message: 'There is an error in the system',
    description: 'Please try again later!',
  });
};

axiosInstance.interceptors.request.use(
  async (config) => {
    const expiresTime: any = localStorage.getItem('expires');

    const tokenLocal: any = localStorage.getItem('accessToken');
    const TOKEN: any = atob(tokenLocal);
    const expires = Number(new Date(atob(expiresTime)));
    const current = Number(new Date());
    document.body.classList.add('loading');

    if (expires <= current) {
      const refreshToken: any = localStorage.getItem('refreshToken');
      const userInfoLocal = localStorage.getItem('user');
      let userInfo: any;

      if (userInfoLocal) {
        userInfo = JSON.parse(atob(userInfoLocal));
      }

      const body = {
        refreshToken: atob(refreshToken),
        deviceId: `deviceId-${userInfo?.email}`,
      };

      axios({
        method: 'post',
        url: `${API_URL}/v1/auth/refresh-tokens`,
        data: body,
        headers: { 'Content-Type': 'multipart/form-data' },
      })
        .then(function (res: any) {
          //handle success
          localStorage.setItem('accessToken', res.data.data.access.token);
          localStorage.setItem('expires', res.data.data.access.expires);
          localStorage.setItem('refreshToken', res.data.data.refresh.token);
          config.headers = {
            ...config.headers,
            Authorization: `Bearer ${res.data.data.access.token}`,
          };

          return config;
        })
        .catch((err) => console.log(err));
    }

    config.headers = {
      ...config.headers,
      Authorization: 'Bearer ' + TOKEN,
    };
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error?.response?.status === 401) {
      window.location.href = '/';
      localStorage.clear();
    }
    if (error?.response?.status === 500) {
      openNotificationWithIcon('error');
      // delete the cookie.
      // localStorage.clear();
      // setCookie('user-info', '', 0);
      // window.location.href = '/';
    }

    return Promise.reject(error);
  }
);

axiosInstance.interceptors.request.use(
  (config: any) => {
    // const token = getCookie('token');
    const tokenLocal: any = localStorage.getItem('accessToken');
    const getAccessToken = atob(tokenLocal);
    config.headers.Authorization = 'Bearer ' + getAccessToken;
    if (config.headers.Authorization === 'Bearer null') {
      delete axiosInstance.defaults.headers.common['Authorization'];
      config.headers.Authorization = '';
    }
    // // const tokenCookies = getCookie('token');
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export function requestAPIWithToken(URL: string, method: string, value: any) {
  // `axios` function returns promise, you can use any ajax lib, which can
  // return promise, or wrap in promise ajax call

  if (method === apiMethod.GET) {
    return axiosInstance.get(URL, { params: value });
  }
  if (method === apiMethod.POST) {
    return axiosInstance.post(URL, value);
  }
  if (method === apiMethod.PUT) {
    return axiosInstance.put(URL, value);
  }
  if (method === apiMethod.PATCH) {
    return axiosInstance.patch(URL, value);
  }
  if (method === apiMethod.DELETE) {
    return axiosInstance.delete(URL, {
      data: value,
    });
  }
}
