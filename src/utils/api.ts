export const API_CALL = {
  API_SIGN_IN: '/v1/auth/login',
  API_USERS: '/v1/users',
  API_PRODUCTS: '/v1/products',
  API_UPLOAD_IMAGE: '/v1/uploads',
  API_GET_PRODUCT_CATEGORIES: '/v1/products/get-all-categories',
  API_SEARCH_PRODUCT: '/v1/search',
  API_GET_ORDERS: '/v1/orders',
  API_SIGN_UP: '/v1/auth/register',
  API_GET_MY_CART: '/v1/cart/my-carts',
  API_DETAIL_CART: '/v1/cart',
  API_MANAGE_ITEM_CART: '/v1/cart/manage-item',
  // API_GET_CATEGORY: '/v1/products/get-all-categories'
  API_GET_MY_ORDER: '/v1/orders/my-orders',
  API_USER_AVATAR: '/v1/users/change-avatar',
  API_USER_PASSWORD: '/v1/users/change-password',
  API_GET_MY_PROFILE: '/v1/users/my-profile',
  API_CHANGE_USERNAME: '/v1/users/change-username',
  API_CHANGE_CONTACT: '/v1/users/change-contact',
  API_CREATE_ITEM_CART: '/v1/cart/create-item'
};
