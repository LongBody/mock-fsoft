import React, { Fragment } from 'react';
import { Rate } from 'antd';
// import {} from
import './style.scss';
import { Link } from 'react-router-dom';

export const ProductItem: React.FC<any> = (props) => {
  const statusClassname =
    props.status === 'Available' ? 'item-status' : 'item-warning-status';

  const detailLink = `/detail?id=${props.id}`;

  return (
    <Link to={detailLink} className="detail-link" key={props.key}>
      <div className="item-container" key={props.key}>
        <div className="img-container">
          <img className="item-img" src={props.img} />
        </div>
        <p className="item-name">{props.name}</p>
        <p className="item-id">ID: {props.id}</p>
        <div className="item-row">
          <Rate allowHalf value={props.rating} disabled/>
          <p className="item-percent-off">{props.percentOff}% Off</p>
        </div>
        <div className="item-row">
          <p className="item-price">$ {props.price.toFixed(2)}</p>
          <i className="fa-solid fa-cart-plus fa-xl"></i>
        </div>
        <div className={statusClassname}>
          <p>{props.status}</p>
        </div>
      </div>
    </Link>
  );
};
