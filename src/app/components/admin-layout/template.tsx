import { Menu } from 'antd';
import { Link } from 'react-router-dom';

export const menuChild = [
  {
    title: 'Dashboard',
    key: 'admin-dashboard',
    icon: <i className="fa-solid fa-chart-bar"></i>,
    path: '/admin/dashboard',
  },
  {
    title: 'Product',
    key: 'product-management',
    icon: <i className="fab fa-wpforms"></i>,
    itemChild: [
      {
        title: 'Product List',
        key: '/admin/product/list',
        path: '/admin/product/list',
      },
      {
        title: 'Add Product',
        key: '/admin/product/create',
        path: '/admin/product/create',
      },
    ],
  },
  {
    title: 'User',
    key: 'user-management',
    icon: <i className="fa-solid fa-user"></i>,
    path: '/admin/user',
    itemChild: [
      {
        title: 'User List',
        key: '/admin/user/list',
        path: '/admin/user/list',
      },
      {
        title: 'Add User',
        key: '/admin/user/create',
        path: '/admin/user/create',
      },
    ],
  },
  {
    title: 'Order',
    key: '/admin/order/list',
    icon: <i className="fa-solid fa-cart-shopping"></i>,
    path: '/admin/order/list',
  },
  {
    title: 'Settings',
    key: 'settings',
    icon: <i className="fa-solid fa-gear"></i>,
    path: '/admin/settings',
  },
];

const updateKey: any = (openKey, setOpenKey, key) => {};

export const menuItem = (location: any, setOpenKey, openKey) =>
  menuChild.map((item: any) => {
    return item?.itemChild?.length > 0 &&
      item?.itemChild?.length !== undefined ? (
      <Menu.SubMenu
        // onC={() => {
        //   let newArr: any = openKey;
        //   newArr.push(item?.key);
        //   setOpenKey(newArr);
        // }}
        key={item?.key}
        icon={item?.icon}
        title={item?.title}
      >
        {item?.itemChild?.map((itemChild: any) => {
          return (
            <Menu.Item
              // className={
              //   location.pathname === itemChild?.path
              //     ? 'active__menu--sider'
              //     : ''
              // }
              key={itemChild?.key}
              icon={itemChild?.icon}
            >
              <Link
                to={itemChild?.path}
                // className={
                //   location.pathname === itemChild?.path
                //     ? 'active__menu--sider-link'
                //     : ''
                // }
              >
                {itemChild?.title}
              </Link>
            </Menu.Item>
          );
        })}
      </Menu.SubMenu>
    ) : (
      <Menu.Item
        // className={
        //   location.pathname === item?.path ? 'active__menu--sider' : ''
        // }
        key={item?.key}
        icon={item?.icon}
      >
        <Link
          to={item?.path}
          // className={
          //   location.pathname === item?.path ? 'active__menu--sider-link' : ''
          // }
        >
          {item?.title}
        </Link>
      </Menu.Item>
    );
  });
