import React, { Fragment } from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import './style.scss';

export const CustomBreadcrumb: React.FC<any> = ({currentPage, previousPage, previousLocation}) => {
  return (
    <Fragment>
      <div className="breadcrumb-container">
        <Breadcrumb separator=">">
          <Breadcrumb.Item><Link to='/'>Home</Link></Breadcrumb.Item>
          {previousPage && previousLocation ? 
           <Breadcrumb.Item>
           <Link to={previousLocation}>{previousPage}</Link>
         </Breadcrumb.Item> : null}
          <Breadcrumb.Item>{currentPage}</Breadcrumb.Item>
        </Breadcrumb>
      </div>
    </Fragment>
  );
};
