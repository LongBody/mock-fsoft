import {
  Badge,
  Button,
  Col,
  Input,
  Modal,
  Popconfirm,
  Row,
  Spin,
  Tooltip,
} from 'antd';
import { SignInForm } from 'app/pages/user/authentication/base/sign-in-form';
import { SignUpForm } from 'app/pages/user/authentication/base/sign-up-form';
import React, { Fragment, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import {
  clearStateCart,
  clearStateCartItem,
  deleteCartRequest,
  deleteItemCartRequest,
  getDetailCartRequest,
  getMyCartRequest,
  updateCartRequest,
} from 'app/pages/user/shopping-cart/screen/action';
import './style.scss';
import { useDispatch, useSelector } from 'react-redux';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { RootState } from 'types';

const { Search } = Input;

export const Navbar: React.FC<any> = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const state = useSelector((state: RootState) => state?.userCartReducer);

  const [isModalSignInVisible, setModalSignInVisible] = useState(false);
  const [isModalSignUpVisible, setModalSignUpVisible] = useState(false);

  const token = localStorage.getItem('accessToken');
  const userInfoCookies = localStorage.getItem('user');
  // const userInfoCookies = getCookie('user-info');
  let userInfo: any;

  if (userInfoCookies) {
    userInfo = JSON.parse(atob(userInfoCookies));
  }

  useEffect(() => {
    if (token && userInfo) {
      dispatch(getMyCartRequest(''));
    }
  }, [token]);

  useEffect(() => {
    if (token && userInfo && state?.idCart) {
      dispatch(
        getDetailCartRequest({
          id: state?.idCart,
        })
      );
    }
  }, [state?.idCart]);

  const cartItem = state?.item?.length > 0 ? state?.item : [];

  useEffect(() => {
    if (
      (state?.message === 'updated' &&
        state.messageCartTotalUpdate === 'cart-updated') ||
      (state?.message === 'deleted' &&
        state.messageCartTotalUpdate === 'cart-updated')
    ) {
      dispatch(clearStateCart(''));
      if (state?.idCart) {
        dispatch(
          getDetailCartRequest({
            id: state?.idCart,
          })
        );
      }
    }
    if (state?.message === 'deleted-cart') {
      dispatch(clearStateCartItem(''));
      dispatch(getMyCartRequest(''));
    }
  }, [state?.message, state.messageCartTotalUpdate]);

  const onSearch = (value: string) => {
    history.push(`product?search=${value}`);
  };

  return (
    <Fragment>
      <div
        style={{
          width: '100%',
        }}
      >
        <div className="topbar">
          <ul className="topbar-list">
            <li>
              <a href="#" className="topbar-link">
                About Us
              </a>
            </li>
            <li>
              <a href="#" className="topbar-link">
                Contacts
              </a>
            </li>
            <li>
              <a href="#" className="topbar-link">
                Store
              </a>
            </li>
            <li>
              <a href="#" className="topbar-link">
                Track Orders
              </a>
            </li>
          </ul>
        </div>
        <div className="navbar">
          <div className="navbar-container">
            <Row
              style={{
                width: '100%',
              }}
            >
              <Col xl={8} xs={24}   style={{
                  paddingBottom:5
                }}>
                <div className="navbar-leftSide">
                  <Link to="/" className="navbar-brand">
                    shop app
                  </Link>
                </div>
              </Col>
              <Col
                xl={16}
                xs={24}
                style={{
                  display: 'flex',
                  justifyContent:"space-between",
                  paddingBottom:5
                }}
              >
                <div className="navbar-middleSide">
                  <Search
                    className="input-search"
                    placeholder="Search"
                    onSearch={onSearch}
                    width={"100%"}
                    defaultValue={
                      props.searchValue === undefined ? '' : props.searchValue
                    }
                    enterButton
                  />
                </div>
                <div className="navbar-rightSide">
                  <ul className="icon-list">
                    <li className="cursor-pointer">
                      {token && userInfo ? (
                        <Tooltip
                          placement="bottomRight"
                          title={
                            <>
                              {cartItem?.length > 0 ? (
                                <Spin spinning={state?.loading} delay={100}>
                                  <div
                                    style={{
                                      backgroundColor: 'white',
                                      padding: 15,
                                      borderRadius: 4,
                                      color: 'black',
                                    }}
                                  >
                                    <div
                                      style={{
                                        paddingBottom: 20,
                                        borderBottom: '1px solid #959292',
                                      }}
                                    >
                                      {cartItem?.map((item: any, key: any) => {
                                        return (
                                          <div
                                            style={{
                                              marginTop: 20,
                                              display: 'flex',
                                              justifyContent: 'space-between',
                                            }}
                                          >
                                            <div style={{ display: 'flex' }}>
                                              <img
                                                src={
                                                  item?.itemCartInfo?.images[0]
                                                    .url
                                                }
                                                style={{
                                                  height: 45,
                                                  width: 65,
                                                  borderRadius: 6,
                                                }}
                                              />
                                              <div
                                                style={{
                                                  paddingLeft: 15,
                                                }}
                                              >
                                                {' '}
                                                {item?.itemCartInfo?.name}{' '}
                                                <div>
                                                  {item?.quantity} x{' '}
                                                  <span className="bold">
                                                    ${item?.price}
                                                  </span>
                                                </div>
                                              </div>
                                            </div>
                                            <div
                                              style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                              }}
                                            >
                                              <i
                                                className="fa-solid fa-x cursor-pointer"
                                                onClick={() => {
                                                  if (cartItem?.length === 1) {
                                                    dispatch(
                                                      deleteCartRequest({
                                                        id: state?.cart?.id,
                                                      })
                                                    );
                                                  } else {
                                                    let totalPrice = 0;
                                                    cartItem?.map(
                                                      (itemChild: any) => {
                                                        if (
                                                          itemChild?.id ===
                                                          item?.id
                                                        ) {
                                                          totalPrice += 0;
                                                        } else {
                                                          totalPrice +=
                                                            parseInt(
                                                              item?.quantity
                                                            ) * item?.price;
                                                        }
                                                      }
                                                    );
                                                    dispatch(
                                                      deleteItemCartRequest({
                                                        id: item?.id,
                                                      })
                                                    );
                                                    dispatch(
                                                      updateCartRequest({
                                                        id: state?.cart?.id,
                                                        total: totalPrice,
                                                      })
                                                    );
                                                  }
                                                }}
                                              ></i>
                                            </div>
                                          </div>
                                        );
                                      })}
                                    </div>

                                    <div>
                                      <div
                                        style={{
                                          marginTop: 5,
                                          display: 'flex',
                                          justifyContent: 'space-between',
                                        }}
                                      >
                                        <span className="bold">SubTotal</span>
                                        <span
                                          style={{
                                            color: '#727070',
                                            fontWeight: 'bold',
                                          }}
                                        >
                                          ${state?.cart?.totalPrice}
                                        </span>
                                      </div>
                                      <div
                                        style={{
                                          marginTop: 5,
                                          display: 'flex',
                                          justifyContent: 'space-between',
                                        }}
                                      >
                                        <span className="bold">Shipping</span>
                                        <span
                                          style={{
                                            color: '#727070',
                                            fontWeight: 'bold',
                                          }}
                                        >
                                          $0
                                        </span>
                                      </div>
                                      <div
                                        style={{
                                          marginTop: 5,
                                          display: 'flex',
                                          justifyContent: 'space-between',
                                        }}
                                      >
                                        <span className="bold">Total</span>
                                        <span
                                          style={{
                                            color: '#727070',
                                            fontWeight: 'bold',
                                          }}
                                        >
                                          ${state?.cart?.totalPrice}
                                        </span>
                                      </div>
                                    </div>

                                    <div
                                      style={{
                                        marginTop: 20,
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                      }}
                                    >
                                      <Button
                                        style={{
                                          borderRadius: 6,
                                          backgroundColor: '#C4C4C4',
                                          borderColor: '#C4C4C4',
                                          color: 'black',
                                          fontWeight: 'bold',
                                        }}
                                        onClick={() => {
                                          history.push('/shoppingcart');
                                        }}
                                      >
                                        View Cart
                                      </Button>
                                      <Button
                                        style={{
                                          borderRadius: 6,
                                          backgroundColor: '#FFD333',
                                          borderColor: '#FFD333',
                                          color: 'black',
                                          fontWeight: 'bold',
                                        }}
                                        onClick={() => {
                                          history.push('/checkout');
                                        }}
                                      >
                                        Checkout
                                      </Button>
                                    </div>
                                  </div>
                                </Spin>
                              ) : (
                                <div
                                  style={{
                                    display: 'flex',
                                    color: 'black',
                                    fontWeight: 'bold',
                                    padding: '40px 5px',
                                  }}
                                >
                                  You don't have any product in your cart!
                                </div>
                              )}
                            </>
                          }
                        >
                          <Badge
                            count={cartItem?.length > 0 ? cartItem?.length : 0}
                            showZero
                          >
                            <i
                              className="fa-solid fa-cart-shopping fa-2x"
                              style={{ fontSize: 23 }}
                              onClick={() => {
                                if (token && userInfo) {
                                  history.push('/shoppingcart');
                                } else {
                                  setModalSignInVisible(true);
                                }
                              }}
                            ></i>
                          </Badge>
                        </Tooltip>
                      ) : (
                        <i
                          className="fa-solid fa-cart-shopping fa-2x"
                          onClick={() => {
                            setModalSignInVisible(true);
                          }}
                          style={{ fontSize: 23 }}
                        ></i>
                      )}
                    </li>
                    <li
                      onClick={() => {
                        if (token && userInfo) {
                          history.push('/my-account');
                        } else {
                          setModalSignInVisible(true);
                        }
                      }}
                      className="cursor-pointer"
                    >
                      {token && userInfo && userInfo?.avatar ? (
                        <img
                          src={userInfo?.avatar}
                          style={{ height: 30, width: 30, borderRadius: '50%' }}
                        />
                      ) : (
                        <i className="fa-solid fa-user fa-2x"></i>
                      )}
                    </li>
                  </ul>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>

      <SignInForm
        isModalSignInVisible={isModalSignInVisible}
        setModalSignInVisible={setModalSignInVisible}
        isModalSignUpVisible={isModalSignUpVisible}
        setModalSignUpVisible={setModalSignUpVisible}
      />
      <SignUpForm
        isModalSignInVisible={isModalSignInVisible}
        setModalSignInVisible={setModalSignInVisible}
        isModalSignUpVisible={isModalSignUpVisible}
        setModalSignUpVisible={setModalSignUpVisible}
      />
    </Fragment>
  );
};
