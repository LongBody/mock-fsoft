import React, { useState, useEffect } from 'react';
import { MenuProps, message, Space, Button } from 'antd';
// type Props = {}
import { getListCategoryRequest } from 'app/pages/user/home-page/screen/action';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

const handleMenuClick: MenuProps['onClick'] = (e) => {
  message.info('Click on menu item.');
  console.log('click', e);
};

export const SideBar: React.FC<any> = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const state = useSelector((state: RootState) => state?.categoryListReducer);
  const [data, setData] = useState([]);

  useEffect(() => {
    dispatch(getListCategoryRequest({}));
  }, []);

  useEffect(() => {
    if (state?.status === 200) {
      setData(state?.dataResponse);
    }
  }, [state]);

  const onClickToProductPage = (value) => {
    history.push(`/product?category=${value}`)
  }

  return (
    <div className="categories-container">
      <div className="categories-title">
      <i className="fa-solid fa-align-left"></i>
        Categories
      </div>
      {data.map((item) => {
        return (
          <div className="category" key={item}>
            <Button className="category-btn" onClick={() => onClickToProductPage(item)}>
              <Space>{item}</Space>
            </Button>
            {/* <a className=></a> */}
          </div>
        );
      })}
    </div>
  );
};
