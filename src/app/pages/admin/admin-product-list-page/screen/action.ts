import { createAction } from '@reduxjs/toolkit';
import { adminListProductEnum } from 'app/pages/admin/admin-product-list-page/screen/types';
import { actionPayload } from 'helper/index';

// admin get list products
export const getListProductRequest = createAction<any>(
  adminListProductEnum.GET_LIST_PRODUCT_REQUEST
);
export const getListProductSuccess = createAction(
  adminListProductEnum.GET_LIST_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getListProductFailure = createAction(
  adminListProductEnum.GET_LIST_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// admin search products
export const searchProductRequest = createAction<any>(
  adminListProductEnum.SEARCH_PRODUCT_REQUEST
);
export const searchProductSuccess = createAction(
  adminListProductEnum.SEARCH_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const searchProductFailure = createAction(
  adminListProductEnum.SEARCH_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// admin delete product
export const deleteProductRequest = createAction<any>(
  adminListProductEnum.DELETE_PRODUCT_REQUEST
);
export const deleteProductSuccess = createAction(
  adminListProductEnum.DELETE_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const deleteProductFailure = createAction(
  adminListProductEnum.DELETE_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);
