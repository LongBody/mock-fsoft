import { LayoutAdmin } from 'app/components/admin-layout';
import { ProductListTable } from 'app/pages/admin/admin-product-list-page/base/list-product-datatable';
import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet-async';

export const AdminProductListPage: React.FC<any> = () => {
  return (
    <Fragment>
      <Helmet>
        <title>Admin list product</title>
        <meta name="description" content="Admin list product" />
      </Helmet>
      <LayoutAdmin content={<ProductListTable />} />
    </Fragment>
  );
};
