import {
  getListProductRequest,
  getListProductSuccess,
  getListProductFailure,
  deleteProductRequest,
  deleteProductSuccess,
  deleteProductFailure,
  searchProductRequest,
  searchProductSuccess,
  searchProductFailure
} from 'app/pages/admin/admin-product-list-page/screen/action';
import { adminListProductEnum } from 'app/pages/admin/admin-product-list-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin get list product
 *
 * @param {Object} action
 *
 */

export function* getListProductSaga({
  payload,
}: ReturnType<typeof getListProductRequest>) {
  try {

    const url = `${API_URL}${API_CALL?.API_PRODUCTS}?size=${payload?.size}&page=${payload?.page}`;
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(getListProductSuccess(response?.data));
  } catch (error: any) {
    yield put(getListProductFailure(error?.response, error.message));
  }
}


/**
 * admin search product
 *
 * @param {Object} action
 *
 */

export function* searchProductSaga({
  payload,
}: ReturnType<typeof searchProductRequest>) {
  try {

    const url = `${API_URL}${API_CALL?.API_SEARCH_PRODUCT}?keyword=${payload?.keyword}`;
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(searchProductSuccess(response?.data));
  } catch (error: any) {
    yield put(searchProductFailure(error?.response, error.message));
  }
}

/**
 * admin delete product
 *
 * @param {Object} action
 *
 */

export function* deleteProductSaga({
  payload,
}: ReturnType<typeof deleteProductRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_PRODUCTS}/${payload?.id}`,
      apiMethod.DELETE,
      ''
    );
    yield put(deleteProductSuccess(response?.data));
  } catch (error: any) {
    yield put(deleteProductFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(adminListProductEnum.GET_LIST_PRODUCT_REQUEST, getListProductSaga),
    takeLatest(adminListProductEnum.DELETE_PRODUCT_REQUEST, deleteProductSaga),
    takeLatest(adminListProductEnum.SEARCH_PRODUCT_REQUEST, searchProductSaga),
  ]);
}
