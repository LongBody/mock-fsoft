export enum adminListProductEnum {
  GET_LIST_PRODUCT_REQUEST = 'APP/ADMIN/GET_LIST_PRODUCT_REQUEST',
  GET_LIST_PRODUCT_SUCCESS = 'APP/ADMIN/GET_LIST_PRODUCT_SUCCESS',
  GET_LIST_PRODUCT_FAILURE = 'APP/ADMIN/GET_LIST_PRODUCT_FAILURE',

  SEARCH_PRODUCT_REQUEST = 'APP/ADMIN/SEARCH_PRODUCT_REQUEST',
  SEARCH_PRODUCT_SUCCESS = 'APP/ADMIN/SEARCH_PRODUCT_SUCCESS',
  SEARCH_PRODUCT_FAILURE = 'APP/ADMIN/SEARCH_PRODUCT_FAILURE',

  DELETE_PRODUCT_REQUEST = 'APP/ADMIN/DELETE_PRODUCT_REQUEST',
  DELETE_PRODUCT_SUCCESS = 'APP/ADMIN/DELETE_PRODUCT_SUCCESS',
  DELETE_PRODUCT_FAILURE = 'APP/ADMIN/DELETE_PRODUCT_FAILURE',

  CLEAR_ADMIN_LIST_PRODUCT_STATE = 'APP/ADMIN/CLEAR_ADMIN_LIST_PRODUCT_STATE',
}
