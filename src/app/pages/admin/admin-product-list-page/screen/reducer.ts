import { createReducer } from '@reduxjs/toolkit';
import { adminListProductEnum } from 'app/pages/admin/admin-product-list-page/screen/types';
// The initial state of the App
export const initialState: any = {
  loading: false,
  message: '',
  status: '',
  total: '',
  loadingBtnDelete: false,
  dataResponse: {},
};

export const adminListProductReducer = createReducer(initialState, (builder) => {
  builder
    // get list product
    .addCase(adminListProductEnum.GET_LIST_PRODUCT_REQUEST, (state, action) => {
      state.dataResponse = {};
      state.status = '';
      state.message = '';
      state.loading = true;
      return state;
    })
    .addCase(adminListProductEnum.GET_LIST_PRODUCT_SUCCESS, (state, action: any) => {
      state.loading = false;
      state.status = action?.payload?.status;
      state.total = action?.payload?.data?.total;
      state.dataResponse = action?.payload?.data?.result;
      return state;
    })
    .addCase(adminListProductEnum.GET_LIST_PRODUCT_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.loading = false;
      return state;
    })


    // search product
    .addCase(adminListProductEnum.SEARCH_PRODUCT_REQUEST, (state, action) => {
      state.dataResponse = {};
      state.status = '';
      state.message = '';
      state.loading = true;
      return state;
    })
    .addCase(adminListProductEnum.SEARCH_PRODUCT_SUCCESS, (state, action: any) => {
      state.loading = false;
      state.status = action?.payload?.status;
      state.total = action?.payload?.data?.total;
      state.dataResponse = action?.payload?.data?.products?.result;
      return state;
    })
    .addCase(adminListProductEnum.SEARCH_PRODUCT_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.loading = false;
      return state;
    })

    // delete product

    .addCase(adminListProductEnum.DELETE_PRODUCT_REQUEST, (state, action) => {
      state.status = '';
      state.message = '';
      state.loadingBtnDelete = true;
      return state;
    })
    .addCase(adminListProductEnum.DELETE_PRODUCT_SUCCESS, (state, action: any) => {
      state.loadingBtnDelete = false;
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(adminListProductEnum.DELETE_PRODUCT_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.loadingBtnDelete = false;
      return state;
    });

});
