import { Breadcrumb, Button } from 'antd';
import React, { Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import './style.scss';

export const ProductListHeader: React.FC<any> = () => {
  const history = useHistory();

  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item>Product</Breadcrumb.Item>
      </Breadcrumb>

      <div className="productListHeader__detail">
        <h2 className="productListHeader__title">Product</h2>
        <div>
          <Button
            onClick={() => {
              history.push('/admin/product/create');
            }}
            className="productListHeader__btn"
          >
            New Product
          </Button>
        </div>
      </div>
    </Fragment>
  );
};
