import { Input, message, Modal, Pagination, Rate, Table } from 'antd';
import { ProductListHeader } from 'app/pages/admin/admin-product-list-page/base/list-product-header';
import {
  deleteProductRequest,
  getListProductRequest,
  searchProductRequest,
} from 'app/pages/admin/admin-product-list-page/screen/action';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

export const ProductListTable: React.FC<any> = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminListProductReducer
  );

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [userSelected, setUserSelected]: any = useState({});
  const [searchTerm, setSearchTerm] = useState('');

  const [pagination, setPagination]: any = useState({
    pageSize: 10,
    pageIndex: 1,
  });

  const columns: any = [
    {
      title: 'Num',
      dataIndex: 'i',
      align: 'center',
      key: 'i',
      render: (i, record, index) => <b>{index + 1}</b>,
    },
    {
      title: 'Product',
      dataIndex: 'name',
      key: 'name',
      // align: 'center',
      render: (name, record) => {
        return (
          <div className="productListTable--nameContentColumn">
            <div>
              <Link to={`/admin/product/update/${record?.id}`}>
                <img
                  className="productListTable--image"
                  src={record?.images[0]?.url}
                  alt={record?.avatar}
                />
              </Link>
            </div>
            <div>
              <div>
                <Link to={`/admin/product/update/${record?.id}`}> {name}</Link>
              </div>
              <div className="productListTable--id">ID: {record?.id}</div>
            </div>
          </div>
        );
      },
    },
    {
      title: 'Brand',
      dataIndex: 'brand',
      key: 'brand',
      align: 'center',
      render: (brand) => <span>{brand ? brand : ''}</span>,
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
      align: 'center',
      render: (category) => <span>{category}</span>,
    },
    {
      title: 'Stock',
      dataIndex: 'countInStock',
      key: 'countInStock',
      align: 'center',
      render: (countInStock) => {
        if (countInStock === '' || countInStock === '0') {
          return <span>Out of stock</span>;
        }
        if (parseInt(countInStock) > 0) {
          return <span>{countInStock} items</span>;
        } else return <span>{countInStock} item</span>;
      },
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      render: (price) => <span>${price}</span>,
    },
    {
      title: 'Rating',
      dataIndex: 'rating',
      key: 'rating',
      align: 'center',
      render: (rating) => (
        <Rate
          disabled
          style={{ fontSize: 14 }}
          defaultValue={rating}
          value={rating}
        />
      ),
    },
    {
      title: '',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (action, record) => (
        <div>
          <i
            className="fa-solid fa-pen-to-square userListTable--editIcon"
            onClick={() => {
              history.push(`/admin/product/update/${record?.id}`);
            }}
          ></i>
          <i
            className="fa-solid fa-trash-can userListTable--deleteIcon"
            onClick={() => {
              setUserSelected(record);
              setIsModalVisible(true);
            }}
          ></i>
        </div>
      ),
    },
  ];

  const getListProduct = () => {
    const body = {
      size: 10,
      page: 1,
    };
    // get data user
    dispatch(getListProductRequest(body));
  };

  useEffect(() => {
    getListProduct();
  }, []);

  useEffect(() => {
    if (state?.status === 200 && state?.message === '' && isModalVisible) {
      setIsModalVisible(false);
      getListProduct();
      message.success('Delete product successfully!');
    }
  }, [state]);

  /**
   * @function onChangepage
   * @param {number} value the page index
   */

  const onChangepage = (value: any) => {
    // set state for page index
    setPagination({
      ...pagination,
      pageIndex: value,
    });
    const body = {
      size: parseInt(pagination?.pageSize),
      page: value,
    };
    dispatch(getListProductRequest(body));
  };

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      const body = {
        size: 10,
        page: 1,
        keyword: searchTerm,
      };
      // get data product
      if (searchTerm !== '') {
        dispatch(searchProductRequest(body));
      }
    }, 500);

    return () => clearTimeout(delayDebounceFn);
  }, [searchTerm]);

  return (
    <Fragment>
      <ProductListHeader />
      {/* content of data table */}
      <div className="userListTable__container">
        {/* search view */}
        <div className="userListTable__search">
          <Input
            prefix={
              <i className="fa-solid fa-magnifying-glass userListTable__search-icon"></i>
            }
            onChange={(e: any) => {
              setSearchTerm(e.target.value);
            }}
            size="large"
            placeholder="Search producs"
          />
        </div>
        {/* table view */}
        <Table
          rowKey="id"
          bordered
          className="mb-10"
          columns={columns}
          loading={state.loading}
          scroll={{ x: 600 }}
          pagination={false}
          dataSource={
            state?.dataResponse?.length > 0 ? state?.dataResponse : []
          }
        />

        <div className="userListTable__pagination">
          <Pagination
            onChange={onChangepage}
            defaultCurrent={state?.dataResponse?.currentPage}
            total={state?.total ? state?.total : 1}
            pageSize={pagination?.pageSize}
          />

          <div>
            Items per page{' '}
            <input
              type="number"
              id="pageSize"
              min="1"
              className="itemPerPage"
              onChange={(value: any) => {
                // set state for page size
                setPagination({
                  ...pagination,
                  pageSize: value?.target?.value,
                });

                const body = {
                  size: parseInt(value?.target?.value),
                  page: pagination?.pageIndex,
                };

                // check if data page is not empty to call api
                if (value?.target?.value !== '') {
                  dispatch(getListProductRequest(body));
                }
              }}
              defaultValue={pagination?.pageSize}
              max="50"
            ></input>
          </div>
        </div>
      </div>

      <Modal
        title="Confirm delete"
        visible={isModalVisible}
        onOk={() => {
          dispatch(
            deleteProductRequest({
              id: userSelected?.id,
            })
          );
        }}
        confirmLoading={state?.loadingBtnDelete}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        okText="Delete"
        okButtonProps={{
          style: { backgroundColor: '#F02020', borderColor: '#F02020' },
        }}
      >
        <p>Are you sure to delete product #{userSelected?.id}</p>
      </Modal>
    </Fragment>
  );
};
