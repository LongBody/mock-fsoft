import { LayoutAdmin } from 'app/components/admin-layout';
import React, { Fragment, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { ProductUpdateForm } from 'app/pages/admin/admin-product-update-page/base/product-update-form';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { message } from 'antd';
import {
  getDetailProductRequest,
  clearStateAdminUpdateProduct,
} from 'app/pages/admin/admin-product-update-page/screen/action';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';

export const AdminProductUpdatePage: React.FC<any> = () => {
  let { id }: any = useParams();
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminUpdateProductReducer
  );

  useEffect(() => {
    if (id) {
      dispatch(getDetailProductRequest({ id: id }));
    }
  }, [id]);

  useEffect(() => {
    if (state?.status === 200 && state?.messageSucess === 'updated') {
      dispatch(clearStateAdminUpdateProduct(''));
      message.success('Update product successfully!');
      history.push('/admin/product/list');
    }
  }, [state?.messageSucess]);

  useEffect(() => {
    return () => {
      dispatch(clearStateAdminUpdateProduct(''));
    };
  }, []);

  return (
    <Fragment>
      <Helmet>
        <title>Admin Product Update Dashboard</title>
        <meta name="description" content="Admin Dashboard" />
      </Helmet>
      <LayoutAdmin content={<ProductUpdateForm />} />
    </Fragment>
  );
};
