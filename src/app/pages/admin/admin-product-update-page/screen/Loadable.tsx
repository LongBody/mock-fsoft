/**
 * Asynchronously loads the component for NotFoundPage
 */

import { lazyLoad } from 'utils/loadable';
import { LoadingIndicator } from 'app/components/loading-indicator';

export const AdminProductUpdatePage = lazyLoad(
  () => import('./index'),
  (module) => module.AdminProductUpdatePage,
  {
    fallback: <LoadingIndicator />,
  }
);
