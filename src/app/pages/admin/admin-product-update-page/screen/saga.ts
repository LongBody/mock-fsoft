import {
  updateProductRequest,
  updateProductSuccess,
  updateProductFailure,
  getDetailProductRequest,
  getDetailProductSuccess,
  getDetailProductFailure,
} from 'app/pages/admin/admin-product-update-page/screen/action';
import { adminUpdateProductEnum } from 'app/pages/admin/admin-product-update-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin create user
 *
 * @param {Object} action
 *
 */

export function* updateProductSaga({
  payload,
}: ReturnType<typeof updateProductRequest>) {
  try {
    const body = {
      name: payload?.name,
      brand: payload?.brand,
      category: payload?.category,
      description: payload?.description,
      price: payload?.price,
      rating: payload?.rating,
      countInStock: payload?.countInStock,
      // images: payload?.images,
    };
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_PRODUCTS}/${payload?.id}`,
      apiMethod.PATCH,
      body
    );
    yield put(updateProductSuccess(response?.data));
  } catch (error: any) {
    yield put(updateProductFailure(error?.response, error.message));
  }
}

/**
 * admin get detail product
 *
 * @param {Object} action
 *
 */

export function* getDetailProductSaga({
  payload,
}: ReturnType<typeof getDetailProductRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_PRODUCTS}/${payload?.id}`,
      apiMethod.GET,
      ''
    );
    yield put(getDetailProductSuccess(response?.data));
  } catch (error: any) {
    yield put(getDetailProductFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(
      adminUpdateProductEnum.UPDATE_PRODUCT_REQUEST,
      updateProductSaga
    ),
    takeLatest(
      adminUpdateProductEnum.GET_DETAIL_PRODUCT_REQUEST,
      getDetailProductSaga
    ),
  ]);
}
