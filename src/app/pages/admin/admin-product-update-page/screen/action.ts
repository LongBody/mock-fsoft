import { createAction } from '@reduxjs/toolkit';
import { adminUpdateProductEnum } from 'app/pages/admin/admin-product-update-page/screen/types';
import { actionPayload } from 'helper/index';

// Update Product

export const updateProductRequest = createAction<any>(
  adminUpdateProductEnum.UPDATE_PRODUCT_REQUEST
);
export const updateProductSuccess = createAction(
  adminUpdateProductEnum.UPDATE_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updateProductFailure = createAction(
  adminUpdateProductEnum.UPDATE_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// get detail product in 
export const getDetailProductRequest = createAction<any>(adminUpdateProductEnum.GET_DETAIL_PRODUCT_REQUEST);
export const getDetailProductSuccess = createAction(
  adminUpdateProductEnum.GET_DETAIL_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getDetailProductFailure = createAction(
  adminUpdateProductEnum.GET_DETAIL_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// clear state

export const clearStateAdminUpdateProduct = createAction<any>(
  adminUpdateProductEnum.CLEAR_ADMIN_UPDATE_PRODUCT_STATE
);
