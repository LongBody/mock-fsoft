import { createReducer } from '@reduxjs/toolkit';
import { adminUpdateProductEnum } from 'app/pages/admin/admin-product-update-page/screen/types';

// The initial state of the App
export const initialState: any = {
  loading: false,
  loadingUpdate: false,
  message: '',
  status: '',
  dataResponse: {},
};

export const adminUpdateProductReducer = createReducer(
  initialState,
  (builder) => {
    builder

      // get detail product

      .addCase(
        adminUpdateProductEnum.GET_DETAIL_PRODUCT_REQUEST,
        (state, action) => {
          state.dataResponse = {};
          state.loading = true;
          return state;
        }
      )
      .addCase(
        adminUpdateProductEnum.GET_DETAIL_PRODUCT_SUCCESS,
        (state, action: any) => {
          state.loading = false;
          state.dataResponse = action?.payload?.data?.product;
          return state;
        }
      )
      .addCase(
        adminUpdateProductEnum.GET_DETAIL_PRODUCT_FAILURE,
        (state, action: any) => {
          state.loading = false;
          return state;
        }
      )

      // update product
      .addCase(
        adminUpdateProductEnum.UPDATE_PRODUCT_REQUEST,
        (state, action) => {
          state.status = '';
          state.message = '';
          state.loadingUpdate = true;
          state.messageSucess = '';
          return state;
        }
      )
      .addCase(
        adminUpdateProductEnum.UPDATE_PRODUCT_SUCCESS,
        (state, action: any) => {
          state.loadingUpdate = false;
          state.message = '';
          state.messageSucess = 'updated';
          state.status = action?.payload?.status;
          return state;
        }
      )
      .addCase(
        adminUpdateProductEnum.UPDATE_PRODUCT_FAILURE,
        (state, action: any) => {
          state.status = action?.payload?.data?.status;
          state.message = action?.payload?.data?.message;
          state.messageSucess = '';
          state.loadingUpdate = false;
          return state;
        }
      )
      // clear state

      .addCase(
        adminUpdateProductEnum.CLEAR_ADMIN_UPDATE_PRODUCT_STATE,
        (state) => {
          state.loading = false;
          state.loadingUpdate = false;
          state.message = '';
          state.status = '';
          state.dataResponse = {};
          state.messageSucess = '';
          return state;
        }
      );
  }
);
