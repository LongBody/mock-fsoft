import { Col, Form, Row, Spin } from 'antd';
import { ProductcategoriesForm } from 'app/pages/admin/admin-product-create-page/base/product-categories-form';
import { ProductImageForm } from 'app/pages/admin/admin-product-create-page/base/product-image-form';
import { ProductUpdateHeader } from 'app/pages/admin/admin-product-update-page/base/product-update-header';
import { ProductBasicInformationForm } from 'app/pages/admin/admin-product-create-page/base/product-basic-information-form';
import { uploadProductImageRequest } from 'app/pages/admin/admin-product-create-page/screen/action';
import { updateProductRequest } from 'app/pages/admin/admin-product-update-page/screen/action';
import { ProductUpdate } from 'app/pages/admin/admin-product-update-page/screen/types';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './style.scss';
import { RootState } from 'types';
import { ProductRatingForm } from 'app/pages/admin/admin-product-create-page/base/product-rating-form';

export const ProductUpdateForm: React.FC<any> = () => {
  const state = useSelector(
    (state: RootState) => state?.adminUpdateProductReducer
  );

  const stateCreateProduct = useSelector(
    (state: RootState) => state?.adminCreateProductReducer
  );

  const [form] = Form.useForm();
  const [fileList, setFileList]: any = useState([]);
  const [previewImage, setPreviewImage]: any = useState('');
  const [values, setValues]: any = useState({});

  const dispatch = useDispatch();

  useEffect(() => {
    if (state?.dataResponse && state?.dataResponse?.images?.length > 0) {
      setPreviewImage(state?.dataResponse?.images[0]?.url);
      setFileList([
        {
          uid: '-1',
          name: 'anh',
          url: state?.dataResponse?.images[0]?.url,
          status: 'done',
        },
      ]);
    }
  }, [state?.dataResponse?.images]);

  const onFinish = (values: any) => {
    // create new FormData
    setValues(values);
    const formData = new FormData();
    let file = fileList[0];

    // append file in form data
    if (fileList.length > 0 && fileList[0]?.status !== 'done' && file) {
      formData.append('image', file?.originFileObj);
      dispatch(uploadProductImageRequest(formData));
    } else {
      updateProduct(state?.dataResponse?.images[0]?.url);
    }
  };

  const updateProduct = (image: string) => {
    const imageArr: any = [];
    imageArr.push(image);
    const body: ProductUpdate = {
      id: state?.dataResponse?.id,
      name: form.getFieldValue('name'),
      brand: form.getFieldValue('brand'),
      category: form.getFieldValue('category'),
      description: form.getFieldValue('description'),
      price: form.getFieldValue('price'),
      rating: form.getFieldValue('rating'),
      countInStock:
        form.getFieldValue('countInStock') !== ''
          ? form.getFieldValue('countInStock')
          : 0,
      images: imageArr,
    };
    if (values && body) {
      dispatch(updateProductRequest(body));
    }
  };

  useEffect(() => {
    if (
      stateCreateProduct?.uploadImageStatus === 201 &&
      stateCreateProduct?.imageUrl &&
      stateCreateProduct?.message === ''
    ) {
      updateProduct(stateCreateProduct?.imageUrl);
    }
  }, [stateCreateProduct]);

  return (
    <Fragment>
      <Spin spinning={state?.loading} delay={100}>
        <Form form={form} layout="vertical" onFinish={onFinish}>
          <ProductUpdateHeader />
          <div>
            <Row>
              <Col xs={12} xl={16} className="userProductForm__leftSide">
                <ProductBasicInformationForm fileList={fileList} form={form} />
              </Col>
              <Col xs={12} xl={8} className="userProductForm__rightSide">
                <ProductImageForm
                  form={form}
                  fileList={fileList}
                  setFileList={setFileList}
                  previewImage={previewImage}
                  setPreviewImage={setPreviewImage}
                />
                <ProductcategoriesForm form={form} />
                <ProductRatingForm form={form} />
              </Col>
            </Row>
          </div>
        </Form>
      </Spin>
    </Fragment>
  );
};
