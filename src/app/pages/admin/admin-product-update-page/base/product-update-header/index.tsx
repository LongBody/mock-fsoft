import { Breadcrumb, Button } from 'antd';
import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

export const ProductUpdateHeader: React.FC<any> = () => {
  const histoty = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminUpdateProductReducer
  );

  const stateCreateProduct = useSelector(
    (state: RootState) => state?.adminCreateProductReducer
  );

  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item
          className="cursor-pointer"
          onClick={() => {
            histoty.push('/admin/product/list');
          }}
        >
          Product
        </Breadcrumb.Item>
        <Breadcrumb.Item>Update Product</Breadcrumb.Item>
      </Breadcrumb>

      <div className="productUpdateHeader__detail">
        <div>
          <h2 className="productUpdateHeader__title">
            Update Product #{state?.dataResponse?.id}
          </h2>
          <span className="productupdateHeader__userId">
            Product ID: {state?.dataResponse?.id}
          </span>
        </div>

        <div>
          <Button
            loading={
              state?.loadingUpdate || stateCreateProduct?.loadingBtnCreate
            }
            htmlType="submit"
            className="productUpdateHeader__btn"
          >
            Update Product
          </Button>
        </div>
      </div>
    </Fragment>
  );
};
