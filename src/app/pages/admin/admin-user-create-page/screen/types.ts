export enum adminCreateUserEnum {
  CREATE_USER_REQUEST = 'APP/ADMIN/CREATE_USER_REQUEST',
  CREATE_USER_SUCCESS = 'APP/ADMIN/CREATE_USER_SUCCESS',
  CREATE_USER_FAILURE = 'APP/ADMIN/CREATE_USER_FAILURE',

  UPLOAD_IMAGE_REQUEST = 'APP/ADMIN/UPLOAD_IMAGE_REQUEST',
  UPLOAD_IMAGE_SUCCESS = 'APP/ADMIN/UPLOAD_IMAGE_SUCCESS',
  UPLOAD_IMAGE_FAILURE = 'APP/ADMIN/UPLOAD_IMAGE_FAILURE',

  CLEAR_ADMIN_CREATE_USER_STATE = 'APP/ADMIN/CLEAR_ADMIN_CREATE_USER_STATE',
}

export interface UserCreate {
  username: string;
  email: string;
  password: string;
  isActive: boolean;
  isContactVerified: boolean;
  isEmailVerified: boolean;
  avatar?: string;
  contact: string;
  role: string;
}
