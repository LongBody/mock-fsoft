import { createAction } from '@reduxjs/toolkit';
import { adminCreateUserEnum } from 'app/pages/admin/admin-user-create-page/screen/types';
import { actionPayload } from 'helper/index';

// create User

export const createUserRequest = createAction<any>(
  adminCreateUserEnum.CREATE_USER_REQUEST
);
export const createUserSuccess = createAction(
  adminCreateUserEnum.CREATE_USER_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const createUserFailure = createAction(
  adminCreateUserEnum.CREATE_USER_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// upload image

export const uploadImageRequest = createAction<any>(
  adminCreateUserEnum.UPLOAD_IMAGE_REQUEST
);
export const uploadImageSuccess = createAction(
  adminCreateUserEnum.UPLOAD_IMAGE_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const uploadImageFailure = createAction(
  adminCreateUserEnum.UPLOAD_IMAGE_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// clear state

export const clearStateAdminCreateUserCreate = createAction<any>(
  adminCreateUserEnum.CLEAR_ADMIN_CREATE_USER_STATE
);
