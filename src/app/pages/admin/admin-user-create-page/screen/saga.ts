import {
  createUserRequest,
  createUserSuccess,
  createUserFailure,
  uploadImageRequest,
  uploadImageSuccess,
  uploadImageFailure
} from 'app/pages/admin/admin-user-create-page/screen/action';
import { adminCreateUserEnum } from 'app/pages/admin/admin-user-create-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin create user
 *
 * @param {Object} action
 *
 */

export function* createUserSaga({
  payload,
}: ReturnType<typeof createUserRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_USERS}`,
      apiMethod.POST,
      payload
    );
    yield put(createUserSuccess(response?.data));
  } catch (error: any) {
    yield put(createUserFailure(error?.response, error.message));
  }
}


/**
 * upload image 
 *
 * @param {Object} action
 *
 */

 export function* uploadImageSaga({
  payload,
}: ReturnType<typeof uploadImageRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_UPLOAD_IMAGE}`,
      apiMethod.POST,
      payload
    );
    yield put(uploadImageSuccess(response?.data));
  } catch (error: any) {
    yield put(uploadImageFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(adminCreateUserEnum.CREATE_USER_REQUEST, createUserSaga),
    takeLatest(adminCreateUserEnum.UPLOAD_IMAGE_REQUEST, uploadImageSaga),
  ]);
}
