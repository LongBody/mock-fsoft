import { LayoutAdmin } from 'app/components/admin-layout';
import React, { Fragment, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { UserCreateForm } from 'app/pages/admin/admin-user-create-page/base/user-create-form';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { message } from 'antd';
import { clearStateAdminCreateUserCreate } from 'app/pages/admin/admin-user-create-page/screen/action';
import { useHistory } from 'react-router-dom';

export const AdminUserCreatePage: React.FC<any> = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminCreateUserReducer
  );

  useEffect(() => {
    if (state?.status === 201 && state?.messageSucess === 'created') {
      dispatch(clearStateAdminCreateUserCreate(''));
      message.success('Create user successfully!');
      history.push('/admin/user/list');
    }
    return () => {
      dispatch(clearStateAdminCreateUserCreate(''));
    };
  }, [state?.messageSucess]);

  useEffect(() => {
    dispatch(clearStateAdminCreateUserCreate(''));
    return () => {
      dispatch(clearStateAdminCreateUserCreate(''));
    };
  }, []);

  return (
    <Fragment>
      <Helmet>
        <title>Admin User Create Dashboard</title>
        <meta name="description" content="Admin Dashboard" />
      </Helmet>
      <LayoutAdmin content={<UserCreateForm />} />
    </Fragment>
  );
};
