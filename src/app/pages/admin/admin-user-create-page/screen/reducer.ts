import { createReducer } from '@reduxjs/toolkit';
import { adminCreateUserEnum } from 'app/pages/admin/admin-user-create-page/screen/types';
// import { setCookie } from 'utils/request';
// The initial state of the App
export const initialState: any = {
  loading: false,
  message: '',
  status: '',
  uploadImageStatus: '',
  messageSucess: '',
  imageUrl: '',
};

export const adminCreateUserReducer = createReducer(initialState, (builder) => {
  builder

    // create user
    .addCase(adminCreateUserEnum.CREATE_USER_REQUEST, (state, action) => {
      state.status = '';
      state.message = '';
      state.messageSucess = '';
      state.loading = true;
      return state;
    })
    .addCase(adminCreateUserEnum.CREATE_USER_SUCCESS, (state, action: any) => {
      state.loading = false;
      state.message = '';
      state.status = action?.payload?.status;
      state.messageSucess = 'created'
      return state;
    })
    .addCase(adminCreateUserEnum.CREATE_USER_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.messageSucess = '';
      state.loading = false;
      return state;
    })

    // upload image
    .addCase(adminCreateUserEnum.UPLOAD_IMAGE_REQUEST, (state, action) => {
      state.status = '';
      state.uploadImageStatus = '';
      state.message = '';
      state.imageUrl = '';
      state.loading = true;
      return state;
    })
    .addCase(adminCreateUserEnum.UPLOAD_IMAGE_SUCCESS, (state, action: any) => {
      state.loading = true;
      state.message = '';
      state.imageUrl = action?.payload?.data?.imageURL;
      state.uploadImageStatus = action?.payload?.status;
      return state;
    })
    .addCase(adminCreateUserEnum.UPLOAD_IMAGE_FAILURE, (state, action: any) => {
      state.uploadImageStatus = action?.payload?.data?.status;
      state.imageUrl = '';
      state.loading = false;
      return state;
    })

    // clear state

    .addCase(adminCreateUserEnum.CLEAR_ADMIN_CREATE_USER_STATE, (state) => {
      state.loading = false;
      state.uploadImageStatus = '';
      state.message = '';
      state.imageUrl = '';
      state.messageSucess = '';
      state.status = '';
      return state;
    });
});
