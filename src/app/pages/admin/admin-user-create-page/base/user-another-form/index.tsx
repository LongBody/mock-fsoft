import { Col, Form, Input, Radio, Row } from 'antd';
import React, { Fragment, useEffect } from 'react';
import type { RadioChangeEvent } from 'antd';
import './style.scss';
import { useSelector } from 'react-redux';
import { RootState } from 'types';
import { validatePatternPhoneNumber } from 'utils/validate';

interface UserAnotherInfoFormIT {
  setStatusUser: any;
  statusUser: any;
  form: any;
}

export const UserAnotherInfoForm: React.FC<any> = (
  props: UserAnotherInfoFormIT
) => {
  const state = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  useEffect(() => {
    if (state?.dataResponse) {
      props?.form.setFieldsValue({
        contact: state?.dataResponse?.contact?.substring(1),
      });
    }
  }, [state?.dataResponse]);

  return (
    <Fragment>
      <div className="userAnotherForm__container">
        <div className="userAnotherForm__title">Another Info</div>

        <div className="userAnotherForm__detailForm">
          <Form.Item
            label="Contact"
            name="contact"
            rules={[
              {
                pattern: validatePatternPhoneNumber,
                message: 'Please enter the correct phone number format!',
              },
            ]}
          >
            <Input
              maxLength={9}
              onChange={(e: any) => {
                let value = e?.target?.value;
                while (value.charAt(0) === '0') {
                  value = value.substring(1);
                }
                props?.form.setFieldsValue({
                  contact: value,
                });
              }}
              prefix={'(+84)'}
              style={{ width: '100%' }}
              placeholder="Enter contact"
            />
          </Form.Item>

          <div>
            <Row className="userAnotherForm__row">
              <Col xl={12}>Status</Col>
              <Col xl={12}>
                <Radio.Group
                  onChange={(e: RadioChangeEvent) => {
                    props?.setStatusUser({
                      ...props?.statusUser,
                      isActive: e.target.value,
                    });
                  }}
                  value={props?.statusUser?.isActive}
                  // defaultValue={props?.statusUser?.isActive}
                  className="userAnotherForm__radio"
                >
                  <Radio value={true}>Active</Radio>
                  <Radio value={false}>Deactive</Radio>
                </Radio.Group>
              </Col>
            </Row>

            <Row className="userAnotherForm__row">
              <Col xl={12}>Verify Email</Col>
              <Col xl={12}>
                <Radio.Group
                  value={props?.statusUser?.isEmailVerified}
                  className="userAnotherForm__radio"
                  onChange={(e: RadioChangeEvent) => {
                    props?.setStatusUser({
                      ...props?.statusUser,
                      isEmailVerified: e.target.value,
                    });
                  }}
                >
                  <Radio value={true}>Yes</Radio>
                  <Radio value={false}>No</Radio>
                </Radio.Group>
              </Col>
            </Row>

            <Row className="userAnotherForm__row">
              <Col xl={12}>Verify Contact</Col>
              <Col xl={12}>
                <Radio.Group
                  value={props?.statusUser?.isContactVerified}
                  className="userAnotherForm__radio"
                  onChange={(e: RadioChangeEvent) => {
                    props?.setStatusUser({
                      ...props?.statusUser,
                      isContactVerified: e.target.value,
                    });
                  }}
                >
                  <Radio value={true}>Yes</Radio>
                  <Radio value={false}>No</Radio>
                </Radio.Group>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
