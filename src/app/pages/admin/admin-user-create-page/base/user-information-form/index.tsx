import { Form, Input, Select } from 'antd';
import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'types';
import './style.scss';
const { Option } = Select;

export const UserInformationForm: React.FC<any> = (props: any) => {
  const state = useSelector(
    (state: RootState) => state?.adminCreateUserReducer
  );

  return (
    <Fragment>
      <div
        className={`userInformationForm__container ${
          props?.fileList?.length > 0
            ? 'userInformationForm__heightWithAvatar'
            : 'userInformationForm__height'
        }`}
      >
        <div className="userInformationForm__title">User Information</div>
        <div className="userInformationForm__detailForm">
          <Form.Item
            label="Name"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please enter name!',
              },
            ]}
          >
            <Input style={{ width: '100%' }} placeholder="Enter name" />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please enter email!',
              },
              {
                pattern:
                  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                message: 'Please enter the correct email format!',
              },
            ]}
          >
            <Input style={{ width: '100%' }} placeholder="Enter email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please enter password!',
              },
              {
                pattern:
                  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                message:
                  'Password must be at least eight characters, at least one letter, one number and one special character!',
              },
            ]}
          >
            <Input.Password
              style={{ width: '100%' }}
              placeholder="Enter password"
            />
          </Form.Item>

          <Form.Item
            label="Retype password"
            name="confirmPassword"
            rules={[
              {
                required: true,
                message: 'Please enter retype password!',
              },
              {
                pattern:
                  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                message:
                  'Password must be at least eight characters, at least one letter, one number and one special character!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error('Password and retype password do not match!')
                  );
                },
              }),
            ]}
          >
            <Input.Password
              style={{ width: '100%' }}
              placeholder="Enter retype password"
            />
          </Form.Item>

          <Form.Item
            label="Role"
            name="role"
            rules={[
              {
                required: true,
                message: 'Please choose a role!',
              },
            ]}
          >
            <Select style={{ width: '100%' }}>
              <Option value="admin">Admin</Option>
              <Option value="user">Customer</Option>
            </Select>
          </Form.Item>

          <div className="mt-10 color-error">
            {state?.message ? state?.message : ''}
          </div>
        </div>
      </div>
    </Fragment>
  );
};
