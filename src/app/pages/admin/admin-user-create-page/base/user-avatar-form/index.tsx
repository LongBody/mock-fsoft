import { UploadOutlined } from '@ant-design/icons';
import { Button, Upload, Image, Form } from 'antd';
import React, { Fragment, useEffect } from 'react';
import { getBase64, getFile } from 'helper/handle-upload';

import './style.scss';
import { useSelector } from 'react-redux';
import { RootState } from 'types';

export const UserAvatarForm: React.FC<any> = (props: any) => {
  const handleChangeUpload: any = async (fileLists: any) => {
    props?.setFileList(fileLists.fileList);
    const image = await getBase64(fileLists.fileList[0].originFileObj);
    props?.setPreviewImage(image);
  };

  const state = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  useEffect(() => {
    if (state?.dataResponse) {
      props?.form.setFieldsValue({
        image: state?.dataResponse?.avatar,
      });
    }
  }, [state]);

  return (
    <Fragment>
      <div className="userAvatarForm__container">
        <div className="userAvatarForm__title">Avatar</div>

        <div className="userAvatarForm__upload">
          <div className="userAvatarForm__upload-icon">
            {props?.fileList?.length > 0 ? (
              props?.previewImage ? (
                <Image
                  preview={false}
                  alt="images"
                  width={100}
                  height={100}
                  src={props?.previewImage}
                />
              ) : (
                ''
              )
            ) : (
              <i className="fa-solid fa-file-arrow-up"></i>
            )}
          </div>
          <div>
            <Form.Item
              name="image"
              label=""
              className="userAvatarForm__upload-field"
              getValueFromEvent={getFile}
              rules={[{ required: true, message: 'Please upload a image!' }]}
            >
              <Upload
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                showUploadList={false}
                maxCount={1}
                defaultFileList={[...props?.fileList]}
                beforeUpload={() => {
                  return false;
                }}
                onChange={handleChangeUpload}
              >
                <Button
                  className="userAvatarForm__upload-button"
                  icon={<UploadOutlined />}
                >
                  Choose file
                </Button>
              </Upload>
            </Form.Item>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
