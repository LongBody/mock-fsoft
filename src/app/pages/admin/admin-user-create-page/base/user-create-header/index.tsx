import { Breadcrumb, Button } from 'antd';
import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

export const UserCreateHeader: React.FC<any> = () => {
  const histoty = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminCreateUserReducer
  );

  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item
          className="cursor-pointer"
          onClick={() => {
            histoty.push('/admin/user/list');
          }}
        >
          User
        </Breadcrumb.Item>
        <Breadcrumb.Item>Create User</Breadcrumb.Item>
      </Breadcrumb>

      <div className="userCreateHeader__detail">
        <h2 className="userCreateHeader__title">Create User</h2>
        <div>
          <Button
            loading={state?.loading}
            htmlType="submit"
            className="userCreateHeader__btn"
          >
            Add User
          </Button>
        </div>
      </div>
    </Fragment>
  );
};
