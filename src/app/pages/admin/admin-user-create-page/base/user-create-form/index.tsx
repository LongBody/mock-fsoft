import { Col, Form, Row } from 'antd';
import { UserAnotherInfoForm } from 'app/pages/admin/admin-user-create-page/base/user-another-form';
import { UserAvatarForm } from 'app/pages/admin/admin-user-create-page/base/user-avatar-form';
import { UserCreateHeader } from 'app/pages/admin/admin-user-create-page/base/user-create-header';
import { UserInformationForm } from 'app/pages/admin/admin-user-create-page/base/user-information-form';
import {
  createUserRequest,
  uploadImageRequest,
} from 'app/pages/admin/admin-user-create-page/screen/action';
import { UserCreate } from 'app/pages/admin/admin-user-create-page/screen/types';
import React, { Fragment, useEffect, useState } from 'react';
import { API_URL } from 'utils/config';
import { useDispatch, useSelector } from 'react-redux';
import './style.scss';
import { RootState } from 'types';

export const UserCreateForm: React.FC<any> = () => {
  const state = useSelector(
    (state: RootState) => state?.adminCreateUserReducer
  );
  const [form] = Form.useForm();
  const [fileList, setFileList]: any = useState([]);
  const [previewImage, setPreviewImage]: any = useState('');
  const [values, setValues]: any = useState({});

  const [statusUser, setStatusUser] = useState({
    isActive: true,
    isContactVerified: false,
    isEmailVerified: false,
  });

  const dispatch = useDispatch();

  const onFinish = (values: any) => {
    // create new FormData
    const formData = new FormData();
    let file = fileList[0];
    // append file in form data
    setValues(values);
    if (file) {
      formData.append('image', file?.originFileObj);
      dispatch(uploadImageRequest(formData));
    } else {
      const body: UserCreate = {
        username: values?.username,
        email: values?.email,
        password: values?.password,
        role: values?.role,
        isContactVerified: statusUser?.isContactVerified,
        isEmailVerified: statusUser?.isEmailVerified,
        contact: '0' + values?.contact,
        isActive: statusUser?.isActive,
      };
      if (values && body) {
        dispatch(createUserRequest(body));
      }
    }
  };

  const creatNewuser = () => {
    const body: UserCreate = {
      username: values?.username,
      email: values?.email,
      password: values?.password,
      role: values?.role,
      isContactVerified: statusUser?.isContactVerified,
      isEmailVerified: statusUser?.isEmailVerified,
      contact: '0' + values?.contact,
      avatar: state?.imageUrl,
      isActive: statusUser?.isActive,
    };
    if (values && body) {
      dispatch(createUserRequest(body));
    }
  };

  useEffect(() => {
    if (
      state?.uploadImageStatus !== '' &&
      state?.imageUrl &&
      state?.message === ''
    ) {
      creatNewuser();
    }
  }, [state]);

  return (
    <Fragment>
      <Form form={form} layout="vertical" onFinish={onFinish}>
        <UserCreateHeader />
        <div>
          <Row>
            <Col xs={12} xl={16} className="userCreateForm__leftSide">
              <UserInformationForm fileList={fileList} />
            </Col>
            <Col xs={12} xl={8} className="userCreateForm__rightSide">
              <UserAvatarForm
                form={form}
                fileList={fileList}
                setFileList={setFileList}
                previewImage={previewImage}
                setPreviewImage={setPreviewImage}
              />
              <UserAnotherInfoForm
                form={form}
                setStatusUser={setStatusUser}
                statusUser={statusUser}
              />
            </Col>
          </Row>
        </div>
      </Form>
    </Fragment>
  );
};
