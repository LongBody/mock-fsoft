import { Input, message, Modal, Pagination, Table, Tag } from 'antd';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import {
  getListUserRequest,
  deleteUserRequest,
} from 'app/pages/admin/admin-user-management/screen/action';
import { UserListHeader } from 'app/pages/admin/admin-user-management/base/list-user-header';
import './style.scss';
import { Link, useHistory } from 'react-router-dom';

export const UserListTable: React.FC<any> = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector((state: RootState) => state?.adminListUserReducer);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [userSelected, setUserSelected]: any = useState({});
  const [searchTerm, setSearchTerm] = useState('');

  const [pagination, setPagination]: any = useState({
    pageSize: 10,
    pageIndex: 1,
  });

  const columns: any = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
      render: (id) => <span>{id}</span>,
    },
    {
      title: 'User',
      dataIndex: 'username',
      key: 'username',
      // align: 'center',
      render: (username, record) => {
        return (
          <div className="userListTable--usernameContentColumn">
            <div>
              <Link to={`/admin/user/detail/${record?.id}`}>
                <img
                  className="userListTable--image"
                  src={record?.avatar}
                  alt={record?.avatar}
                />
              </Link>
            </div>
            <div>
              <div>
                <Link to={`/admin/user/detail/${record?.id}`}> {username}</Link>

                <span className="ml-10">
                  {record?.role === 'admin' ? (
                    <Tag color={'success'}>Admin</Tag>
                  ) : (
                    <Tag color={'error'}>Customer</Tag>
                  )}
                </span>
              </div>
              <div>{record?.email}</div>
            </div>
          </div>
        );
      },
    },
    {
      title: 'Contact',
      dataIndex: 'contact',
      key: 'contact',
      align: 'center',
      render: (contact) => <span>{contact ? contact : ''}</span>,
    },
    {
      title: 'Status',
      dataIndex: 'isActive',
      key: 'isActive',
      align: 'center',
      render: (isActive) => <span>{isActive ? 'Active' : 'Deactive'}</span>,
    },
    {
      title: 'Verify Email',
      dataIndex: 'isEmailVerified',
      key: 'isEmailVerified',
      align: 'center',
      render: (isEmailVerified) => (
        <span>{isEmailVerified ? 'Yes' : 'No'}</span>
      ),
    },
    {
      title: 'Verify Contact',
      dataIndex: 'isContactVerified',
      key: 'isContactVerified',
      align: 'center',
      render: (isContactVerified) => (
        <span>{isContactVerified ? 'Yes' : 'No'}</span>
      ),
    },
    {
      title: '',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (action, record) => (
        <div>
          <i
            className="fa-solid fa-pen-to-square userListTable--editIcon"
            onClick={() => {
              history.push(`/admin/user/update/${record?.id}`);
            }}
          ></i>
          <i
            className="fa-solid fa-trash-can userListTable--deleteIcon"
            onClick={() => {
              setUserSelected(record);
              setIsModalVisible(true);
            }}
          ></i>
        </div>
      ),
    },
  ];

  const getListUser = () => {
    const body = {
      size: 10,
      page: 1,
    };
    // get data user
    dispatch(getListUserRequest(body));
  };

  useEffect(() => {
    getListUser();
  }, []);

  useEffect(() => {
    if (state?.status === 200 && state?.message === '' && isModalVisible) {
      setIsModalVisible(false);
      getListUser();
      message.success('Delete user successfully!');
    }
  }, [state]);

  /**
   * @function onChangepage
   * @param {number} value the page index
   */

  const onChangepage = (value: any) => {
    // set state for page index
    setPagination({
      ...pagination,
      pageIndex: value,
    });
    const body = {
      size: parseInt(pagination?.pageSize),
      page: value,
    };

    dispatch(getListUserRequest(body));
  };

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      const body = {
        size: 10,
        page: 1,
        username: searchTerm,
      };
      // get data user
      if (searchTerm !== '') {
        dispatch(getListUserRequest(body));
      }
    }, 500);

    return () => clearTimeout(delayDebounceFn);
  }, [searchTerm]);

  return (
    <Fragment>
      <UserListHeader />
      {/* content of data table */}
      <div className="userListTable__container">
        {/* search view */}
        <div className="userListTable__search">
          <Input
            prefix={
              <i className="fa-solid fa-magnifying-glass userListTable__search-icon"></i>
            }
            onChange={(e: any) => {
              setSearchTerm(e.target.value);
            }}
            size="large"
            placeholder="Search users"
          />
        </div>
        {/* table view */}
        <Table
          rowKey="id"
          bordered
          className="mb-10"
          columns={columns}
          loading={state.loading}
          scroll={{ x: 600 }}
          pagination={false}
          dataSource={
            state?.dataResponse?.length > 0 ? state?.dataResponse : []
          }
        />

        <div className="userListTable__pagination">
          <Pagination
            onChange={onChangepage}
            defaultCurrent={state?.dataResponse?.currentPage}
            total={state?.total ? state?.total : 1}
            pageSize={pagination?.pageSize}
          />

          <div>
            Items per page{' '}
            <input
              type="number"
              id="pageSize"
              min="1"
              className="itemPerPage"
              onChange={(value: any) => {
                // set state for page size
                setPagination({
                  ...pagination,
                  pageSize: value?.target?.value,
                });

                const body = {
                  size: parseInt(value?.target?.value),
                  page: pagination?.pageIndex,
                };

                // check if data page is not empty to call api
                if (value?.target?.value !== '') {
                  dispatch(getListUserRequest(body));
                }
              }}
              defaultValue={pagination?.pageSize}
              max="50"
            ></input>
          </div>
        </div>
      </div>

      <Modal
        title="Confirm delete"
        visible={isModalVisible}
        onOk={() => {
          dispatch(
            deleteUserRequest({
              id: userSelected?.id,
            })
          );
        }}
        confirmLoading={state?.loadingBtnDelete}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        okText="Delete"
        okButtonProps={{
          style: { backgroundColor: '#F02020', borderColor: '#F02020' },
        }}
      >
        <p>Are you sure to delete user #{userSelected?.id}</p>
      </Modal>
    </Fragment>
  );
};
