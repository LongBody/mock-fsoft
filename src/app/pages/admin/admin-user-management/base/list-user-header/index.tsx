import { Breadcrumb, Button } from 'antd';
import React, { Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import './style.scss';

export const UserListHeader: React.FC<any> = () => {
  const history = useHistory();

  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item>User</Breadcrumb.Item>
      </Breadcrumb>

      <div className="userListHeader__detail">
        <h2 className="userListHeader__title">User</h2>
        <div>
          <Button
            onClick={() => {
              history.push('/admin/user/create');
            }}
            className="userListHeader__btn"
          >
            New User
          </Button>
        </div>
      </div>
    </Fragment>
  );
};
