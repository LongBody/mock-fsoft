import { LayoutAdmin } from 'app/components/admin-layout';
import { UserListTable } from 'app/pages/admin/admin-user-management/base/list-user-datatable';
import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet-async';

export const AdminUserManagementPage: React.FC<any> = () => {
  return (
    <Fragment>
      <Helmet>
        <title>Admin list user</title>
        <meta name="description" content="Admin list user" />
      </Helmet>
      <LayoutAdmin content={<UserListTable />} />
    </Fragment>
  );
};
