import {
  getListUserRequest,
  getListUserSuccess,
  getListUserFailure,
  deleteUserRequest,
  deleteUserSuccess,
  deleteUserFailure,
} from 'app/pages/admin/admin-user-management/screen/action';
import { adminListUserEnum } from 'app/pages/admin/admin-user-management/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin get list user
 *
 * @param {Object} action
 *
 */

export function* getListUserSaga({
  payload,
}: ReturnType<typeof getListUserRequest>) {
  try {
    let url = '';
    if (payload?.username) {
      url = `${API_URL}${API_CALL?.API_USERS}?size=${payload?.size}&page=${payload?.page}&username=${payload?.username}`;
    } else {
      url = `${API_URL}${API_CALL?.API_USERS}?size=${payload?.size}&page=${payload?.page}`;
    }
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(getListUserSuccess(response?.data));
  } catch (error: any) {
    yield put(getListUserFailure(error?.response, error.message));
  }
}

/**
 * admin delete user
 *
 * @param {Object} action
 *
 */

export function* deleteUserSaga({
  payload,
}: ReturnType<typeof deleteUserRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_USERS}/${payload?.id}`,
      apiMethod.DELETE,
      ''
    );
    yield put(deleteUserSuccess(response?.data));
  } catch (error: any) {
    yield put(deleteUserFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(adminListUserEnum.GET_LIST_USER_REQUEST, getListUserSaga),
    takeLatest(adminListUserEnum.DELETE_USER_REQUEST, deleteUserSaga),
  ]);
}
