import { LayoutAdmin } from 'app/components/admin-layout';
import React, { Fragment, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { OrderDetailForm } from 'app/pages/admin/admin-order-detail-page/base/detail-order-info';
import {
  clearStateAdminDetailOrder
} from 'app/pages/admin/admin-order-detail-page/screen/action';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { message } from 'antd';
import { useHistory } from 'react-router-dom';

export const AdminOrderDetailPage: React.FC<any> = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminDetailOrderReducer
  );

  useEffect(() => {
    if (state?.message === 'updated' && state?.status === 200) {
      dispatch(clearStateAdminDetailOrder(''));
      message.success('Update order successfully!');
      history.push('/admin/order/list');
    }
  }, [state?.message]);

  useEffect(() => {
    return () => {
      dispatch(clearStateAdminDetailOrder(''));
    };
  }, []);

  return (
    <Fragment>
      <Helmet>
        <title>Admin Detail Order</title>
        <meta name="description" content="Admin Dashboard" />
      </Helmet>
      <LayoutAdmin content={<OrderDetailForm />} />
    </Fragment>
  );
};
