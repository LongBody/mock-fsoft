import { createReducer } from '@reduxjs/toolkit';
import { adminDetailOrderEnum } from 'app/pages/admin/admin-order-detail-page/screen/types';

// The initial state of the App
export const initialState: any = {
  loading: false,
  dataResponse: {},
  status: '',
  loadingUpdateBtn: false,
  message: '',
};

export const adminDetailOrderReducer = createReducer(
  initialState,
  (builder) => {
    builder
      // get detail order
      .addCase(
        adminDetailOrderEnum.GET_DETAIL_ORDER_REQUEST,
        (state, action) => {
          state.dataResponse = {};
          state.loading = true;
          return state;
        }
      )
      .addCase(
        adminDetailOrderEnum.GET_DETAIL_ORDER_SUCCESS,
        (state, action: any) => {
          state.loading = false;
          state.dataResponse = action?.payload?.data;
          return state;
        }
      )
      .addCase(
        adminDetailOrderEnum.GET_DETAIL_ORDER_FAILURE,
        (state, action: any) => {
          state.loading = false;
          return state;
        }
      )

      // update order

      .addCase(
        adminDetailOrderEnum.UPDATE_DETAIL_ORDER_REQUEST,
        (state, action) => {
          state.loadingUpdateBtn = true;
          return state;
        }
      )
      .addCase(
        adminDetailOrderEnum.UPDATE_DETAIL_ORDER_SUCCESS,
        (state, action: any) => {
          state.loadingUpdateBtn = false;
          state.message = 'updated';
          state.status = action?.payload?.status;
          return state;
        }
      )
      .addCase(
        adminDetailOrderEnum.UPDATE_DETAIL_ORDER_FAILURE,
        (state, action: any) => {
          state.status = action?.payload?.data?.status;
          state.message = action?.payload?.data?.message;
          state.loadingUpdateBtn = false;
          return state;
        }
      )

      // clear state

      .addCase(adminDetailOrderEnum.CLEAR_ADMIN_DETAIL_ORDER_STATE, (state) => {
        state.loading = false;
        state.message = '';
        state.status = '';
        state.dataResponse = {};
        return state;
      });
  }
);
