import { createAction } from '@reduxjs/toolkit';
import { adminDetailOrderEnum } from 'app/pages/admin/admin-order-detail-page/screen/types';
import { actionPayload } from 'helper/index';

// get detail order in admin page
export const getDetailOrderRequest = createAction<any>(
  adminDetailOrderEnum.GET_DETAIL_ORDER_REQUEST
);
export const getDetailOrderSuccess = createAction(
  adminDetailOrderEnum.GET_DETAIL_ORDER_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getDetailOrderFailure = createAction(
  adminDetailOrderEnum.GET_DETAIL_ORDER_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// update detail order in admin page
export const updateDetailOrderRequest = createAction<any>(
  adminDetailOrderEnum.UPDATE_DETAIL_ORDER_REQUEST
);
export const updateDetailOrderSuccess = createAction(
  adminDetailOrderEnum.UPDATE_DETAIL_ORDER_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updateDetailOrderFailure = createAction(
  adminDetailOrderEnum.UPDATE_DETAIL_ORDER_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// clear state

export const clearStateAdminDetailOrder = createAction<any>(
  adminDetailOrderEnum.CLEAR_ADMIN_DETAIL_ORDER_STATE
);
