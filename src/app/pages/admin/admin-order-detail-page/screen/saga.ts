import {
  getDetailOrderRequest,
  getDetailOrderSuccess,
  getDetailOrderFailure,
  updateDetailOrderRequest,
  updateDetailOrderSuccess,
  updateDetailOrderFailure,
} from 'app/pages/admin/admin-order-detail-page/screen/action';
import { adminDetailOrderEnum } from 'app/pages/admin/admin-order-detail-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin get detail order
 *
 * @param {Object} action
 *
 */

export function* getDetailOrderSaga({
  payload,
}: ReturnType<typeof getDetailOrderRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_GET_ORDERS}/${payload?.id}`,
      apiMethod.GET,
      ''
    );
    yield put(getDetailOrderSuccess(response?.data));
  } catch (error: any) {
    yield put(getDetailOrderFailure(error?.response, error.message));
  }
}

/**
 * admin update detail order
 *
 * @param {Object} action
 *
 */

export function* updateDetailOrderSaga({
  payload,
}: ReturnType<typeof updateDetailOrderRequest>) {
  try {
    const body = {
      status: payload?.status,
      isPaid: payload?.isPaid,
    };
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_GET_ORDERS}/${payload?.id}`,
      apiMethod.PATCH,
      body
    );
    yield put(updateDetailOrderSuccess(response?.data));
  } catch (error: any) {
    yield put(updateDetailOrderFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(
      adminDetailOrderEnum.GET_DETAIL_ORDER_REQUEST,
      getDetailOrderSaga
    ),
    takeLatest(
      adminDetailOrderEnum.UPDATE_DETAIL_ORDER_REQUEST,
      updateDetailOrderSaga
    ),
  ]);
}
