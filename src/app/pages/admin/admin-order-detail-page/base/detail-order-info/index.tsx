import { Button, Col, Form, Row, Select, Spin } from 'antd';
import { OrderDetailHeader } from 'app/pages/admin/admin-order-detail-page/base/detail-order-header';
import { OrderDetailTable } from 'app/pages/admin/admin-order-detail-page/base/detail-order-datatable';
import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { UserOutlined } from '@ant-design/icons';
import {
  getDetailOrderRequest,
  updateDetailOrderRequest,
} from 'app/pages/admin/admin-order-detail-page/screen/action';
import './style.scss';
import { useParams } from 'react-router-dom';
import moment from 'moment';
import { getDetailUserRequest } from 'app/pages/admin/admin-user-detail-page/screen/action';
const { Option } = Select;

export const OrderDetailForm: React.FC<any> = () => {
  const [form] = Form.useForm();
  const state = useSelector(
    (state: RootState) => state?.adminDetailOrderReducer
  );

  const stateDetailUser = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  let { id }: any = useParams();

  const dispatch = useDispatch();

  useEffect(() => {
    if (id) {
      dispatch(getDetailOrderRequest({ id: id }));
    }
  }, []);

  useEffect(() => {
    if (state?.dataResponse?.order?.userId) {
      dispatch(
        getDetailUserRequest({ id: state?.dataResponse?.order?.userId })
      );
    }
  }, [state?.dataResponse?.order]);

  useEffect(() => {
    if (state?.dataResponse?.order) {
      form.setFieldsValue({
        paid: state?.dataResponse?.order?.isPaid ? 'Yes' : 'No',
        status: state?.dataResponse?.order?.status,
      });
    }
  }, [state?.dataResponse?.order]);

  const onFinish = (values: any) => {
    const body = {
      id: state?.dataResponse?.order?.id,
      status: values?.status,
      isPaid: values?.paid === 'Yes' || values?.paid === true ? true : false,
    };
    dispatch(updateDetailOrderRequest(body));
  };

  return (
    <Fragment>
      <Spin spinning={state?.loading} delay={100}>
        <Form
          layout="vertical"
          onFinish={onFinish}
          form={form}
          className="orderDetail__formContainer"
        >
          <OrderDetailHeader />

          <div className="orderDetail__form">
            <div className="orderDetail__form-detail">
              <div className="orderDetail__form-handle">
                <div className="orderDetail__form-content">
                  <div className="orderDetail__form-content-date">
                    Created at:{' '}
                    {moment(state?.dataResponse?.order?.createdAt).format(
                      'DD/MM/YYY'
                    )}
                  </div>
                  <div className="orderDetail__form-content-date">
                    Updated at:{' '}
                    {moment(state?.dataResponse?.order?.updateddAt).format(
                      'DD/MM/YYY'
                    )}
                  </div>
                  <div className="orderDetail__form-content-orderId">
                    Order ID: {state?.dataResponse?.order?.id}
                  </div>
                  <div></div>
                </div>
                <div>
                  <div className="flex">
                    <Form.Item name={'status'} label="Status">
                      <Select style={{ width: 120 }}>
                        <Option value="Processing">Processing</Option>
                        <Option value="Shipping">Shipping</Option>
                        <Option value="Cancel">Cancel</Option>
                      </Select>
                    </Form.Item>

                    <Form.Item name={'paid'} label="Paided" className="ml-20">
                      <Select style={{ width: 120 }}>
                        <Option value={true}>Yes</Option>
                        <Option value={false}>No</Option>
                      </Select>
                    </Form.Item>

                    <Form.Item label=" " className="ml-20">
                      <Button
                        loading={state?.loadingUpdateBtn}
                        htmlType="submit"
                        className="orderDetail__form-content__btn"
                      >
                        Update Order
                      </Button>
                    </Form.Item>
                  </div>
                </div>
              </div>
              <div>
                <Row>
                  <Col xl={8}>
                    <div className="orderDetail__info">
                      <div className="orderDetail__icon">
                        <UserOutlined className="orderDetail__icon-user" />
                      </div>
                      <div>
                        <div className="orderDetail__title">Customer</div>
                        <div className="orderDetail__desciption">
                          Name: {stateDetailUser?.dataResponse?.username}
                        </div>
                        <div className="orderDetail__desciption">
                          Email: {stateDetailUser?.dataResponse?.email}
                        </div>
                        <div className="orderDetail__desciption">
                          Phone: {stateDetailUser?.dataResponse?.contact}
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col xl={8}>
                    <div className="orderDetail__info">
                      <div className="orderDetail__icon">
                        <i className="fa-solid fa-truck orderDetail__icon-truck"></i>
                      </div>
                      <div>
                        <div className="orderDetail__title">Order Info</div>
                        <div className="orderDetail__desciption">
                          Status: {state?.dataResponse?.order?.status}
                        </div>
                        <div className="orderDetail__desciption">
                          Pay method:{' '}
                          {state?.dataResponse?.order?.paymentMethod}
                        </div>
                        <div className="orderDetail__desciption">
                          Paid:{' '}
                          {state?.dataResponse?.order?.isPaid ? 'Yes' : 'No'}
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col xl={8}>
                    <div className="orderDetail__info">
                      <div className="orderDetail__icon">
                        <i className="fa-solid fa-location-dot orderDetail__icon-location"></i>
                      </div>
                      <div>
                        <div className="orderDetail__title">Deliver to</div>
                        <div className="orderDetail__desciption-address">
                          Address: {state?.dataResponse?.order?.address}
                        </div>
                        <div className="orderDetail__desciption">
                          Contact: {state?.dataResponse?.order?.contact}
                        </div>
                        <div className="orderDetail__desciption">
                          Shipper: {state?.dataResponse?.order?.shipper}
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
            <div className='orderDetail__itemDataTable-title'>
              Items
            </div>
            <OrderDetailTable />
          </div>
        </Form>
      </Spin>
    </Fragment>
  );
};
