import { Table } from 'antd';
import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'types';
import './style.scss';

export const OrderDetailTable: React.FC<any> = () => {
  const state = useSelector(
    (state: RootState) => state?.adminDetailOrderReducer
  );

  const dataSource =
    state?.dataResponse?.items?.length > 0 ? state?.dataResponse?.items : [];
  const columns: any = [
    {
      title: 'Num',
      dataIndex: 'i',
      align: 'center',
      key: 'i',
      render: (i, record, index) => <b>{index + 1}</b>,
    },
    {
      title: 'Product',
      dataIndex: 'name',
      key: 'name',
      // align: 'center',
      render: (name, record) => {
        return (
          <div className="productListTable--nameContentColumn">
            <div>
              <img
                className="productListTable--image"
                src={record?.itemInfo?.images[0]?.url}
                alt={'product-image'}
              />
            </div>
            <div>
              <div>{record?.itemInfo?.name}</div>
              <div className="productListTable--id">
                ID: {record?.itemInfo?.id}
              </div>
            </div>
          </div>
        );
      },
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      render: (price) => <span>{price ? price : ''}</span>,
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      key: 'quantity',
      align: 'center',
      render: (quantity) => <span>{quantity}</span>,
    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
      align: 'center',
      render: (i, record) => {
        return <span>${record?.price * record?.quantity}</span>;
      },
    },
  ];

  return (
    <Fragment>
      <Table
        rowKey="id"
        // bordered
        className="mb-10"
        columns={columns}
        scroll={{ x: 600 }}
        pagination={false}
        dataSource={dataSource}
        summary={(pageData) => {
          let total = 0;
          pageData.forEach(({ price, quantity }) => {
            total += price * quantity;
          });
          return (
            <>
              <Table.Summary.Row>
                <Table.Summary.Cell index={0}></Table.Summary.Cell>
                <Table.Summary.Cell index={0}></Table.Summary.Cell>
                <Table.Summary.Cell index={0} align="center" className="bold">
                  Total Amount:
                </Table.Summary.Cell>
                <Table.Summary.Cell index={0}></Table.Summary.Cell>
                <Table.Summary.Cell index={0} align="center" className="bold" >
                  ${total}
                </Table.Summary.Cell>
              </Table.Summary.Row>
            </>
          );
        }}
      />
    </Fragment>
  );
};
