import { Breadcrumb } from 'antd';
import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

export const OrderDetailHeader: React.FC<any> = () => {
  const histoty = useHistory();
  const state = useSelector(
    (state: RootState) => state?.adminDetailOrderReducer
  );

  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item
          className="cursor-pointer"
          onClick={() => {
            histoty.push('/admin/order/list');
          }}
        >
          Order
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          User Order #{state?.dataResponse?.order?.id}
        </Breadcrumb.Item>
      </Breadcrumb>

      <div className="orderDetailHeader__detail">
        <h2 className="orderDetailHeader__title">
          Order Detail #{state?.dataResponse?.order?.id}
        </h2>
        <span className="orderDetailHeader__userId">
          Order ID: {state?.dataResponse?.order?.id}
        </span>
      </div>
    </Fragment>
  );
};
