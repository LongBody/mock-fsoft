import { LayoutAdmin } from 'app/components/admin-layout';
import React, { Fragment, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { ProductCreateForm } from 'app/pages/admin/admin-product-create-page/base/product-create-form';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { message } from 'antd';
import { clearStateAdminCreateProductCreate } from 'app/pages/admin/admin-product-create-page/screen/action';
import { useHistory } from 'react-router-dom';

export const AdminProductCreatePage: React.FC<any> = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminCreateProductReducer
  );

  useEffect(() => {
    if (state?.status === 201 && state?.messageSucess === 'created') {
      dispatch(clearStateAdminCreateProductCreate(''));
      message.success('Create product successfully!');
      history.push('/admin/product/list');
    }
  }, [state?.messageSucess]);

  useEffect(() => {
    return () => {
      dispatch(clearStateAdminCreateProductCreate(''));
    };
  }, []);

  return (
    <Fragment>
      <Helmet>
        <title>Admin Product Create Dashboard</title>
        <meta name="description" content="Admin Dashboard" />
      </Helmet>
      <LayoutAdmin content={<ProductCreateForm />} />
    </Fragment>
  );
};
