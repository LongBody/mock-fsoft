import { createReducer } from '@reduxjs/toolkit';
import { adminCreateProductEnum } from 'app/pages/admin/admin-product-create-page/screen/types';
// The initial state of the App
export const initialState: any = {
  loading: false,
  loadingBtnCreate: false,
  message: '',
  status: '',
  uploadImageStatus: '',
  dataResponse: {},
  messageSucess: '',
  imageUrl: '',
};

export const adminCreateProductReducer = createReducer(
  initialState,
  (builder) => {
    builder

      // create product
      .addCase(
        adminCreateProductEnum.CREATE_PRODUCT_REQUEST,
        (state, action) => {
          state.loadingBtnCreate = true;
          state.status = '';
          state.message = '';
          state.imageUrl = '';
          state.messageSucess = '';
          return state;
        }
      )
      .addCase(
        adminCreateProductEnum.CREATE_PRODUCT_SUCCESS,
        (state, action: any) => {
          state.loadingBtnCreate = false;
          state.message = '';
          state.messageSucess = 'created';
          state.status = action?.payload?.status;
          return state;
        }
      )
      .addCase(
        adminCreateProductEnum.CREATE_PRODUCT_FAILURE,
        (state, action: any) => {
          state.status = action?.payload?.data?.status;
          state.messageSucess = '';
          state.message = action?.payload?.data?.message;
          state.loadingBtnCreate = false;
          return state;
        }
      )

      // get product categories
      .addCase(
        adminCreateProductEnum.GET_PRODUCT_CATEGORIES_REQUEST,
        (state, action) => {
          state.loading = true;
          state.dataResponse = {};
          return state;
        }
      )
      .addCase(
        adminCreateProductEnum.GET_PRODUCT_CATEGORIES_SUCCESS,
        (state, action: any) => {
          state.loading = false;
          state.dataResponse = action?.payload?.data;
          return state;
        }
      )
      .addCase(
        adminCreateProductEnum.GET_PRODUCT_CATEGORIES_FAILURE,
        (state, action: any) => {
          state.dataResponse = {};
          state.loading = false;
          return state;
        }
      )

      // upload image
      .addCase(
        adminCreateProductEnum.UPLOAD_IMAGE_PRODUCT_REQUEST,
        (state, action) => {
          state.status = '';
          state.uploadImageStatus = '';
          state.message = '';
          state.imageUrl = '';
          state.loadingBtnCreate = true;
          return state;
        }
      )
      .addCase(
        adminCreateProductEnum.UPLOAD_IMAGE_PRODUCT_SUCCESS,
        (state, action: any) => {
          state.message = '';
          state.imageUrl = action?.payload?.data?.imageURL;
          state.uploadImageStatus = action?.payload?.status;
          return state;
        }
      )
      .addCase(
        adminCreateProductEnum.UPLOAD_IMAGE_PRODUCT_FAILURE,
        (state, action: any) => {
          state.uploadImageStatus = action?.payload?.data?.status;
          state.imageUrl = '';
          state.loading = false;
          return state;
        }
      )

      // clear state

      .addCase(
        adminCreateProductEnum.CLEAR_ADMIN_CREATE_PRODUCT_STATE,
        (state) => {
          state.loading = false;
          state.uploadImageStatus = '';
          state.message = '';
          state.imageUrl = '';
          state.status = '';
          state.messageSucess = '';
          return state;
        }
      );
  }
);
