import {
  createProductRequest,
  createProductSuccess,
  createProductFailure,
  uploadProductImageRequest,
  uploadProductImageSuccess,
  uploadProductImageFailure,
  getProductCategoriesRequest,
  getProductCategoriesSuccess,
  getProductCategoriesFailure
} from 'app/pages/admin/admin-product-create-page/screen/action';
import { adminCreateProductEnum } from 'app/pages/admin/admin-product-create-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin create products
 *
 * @param {Object} action
 *
 */

export function* createProductSaga({
  payload,
}: ReturnType<typeof createProductRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_PRODUCTS}`,
      apiMethod.POST,
      payload
    );
    yield put(createProductSuccess(response?.data));
  } catch (error: any) {
    yield put(createProductFailure(error?.response, error.message));
  }
}


/**
 * admin get products categories
 *
 * @param {Object} action
 *
 */

export function* getProductCategoriesSaga({
  payload,
}: ReturnType<typeof getProductCategoriesRequest>) {
  try {
    
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_GET_PRODUCT_CATEGORIES}`,
      apiMethod.GET,
      payload
    );
    yield put(getProductCategoriesSuccess(response?.data));
  } catch (error: any) {
    yield put(getProductCategoriesFailure(error?.response, error.message));
  }
}


/**
 * upload product image 
 *
 * @param {Object} action
 *
 */

export function* uploadProductImageSaga({
  payload,
}: ReturnType<typeof uploadProductImageRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_UPLOAD_IMAGE}`,
      apiMethod.POST,
      payload
    );
    yield put(uploadProductImageSuccess(response?.data));
  } catch (error: any) {
    yield put(uploadProductImageFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(adminCreateProductEnum.CREATE_PRODUCT_REQUEST, createProductSaga),
    takeLatest(adminCreateProductEnum.UPLOAD_IMAGE_PRODUCT_REQUEST, uploadProductImageSaga),
    takeLatest(adminCreateProductEnum.GET_PRODUCT_CATEGORIES_REQUEST, getProductCategoriesSaga),
  ]);
}
