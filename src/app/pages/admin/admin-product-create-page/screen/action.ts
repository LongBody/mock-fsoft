import { createAction } from '@reduxjs/toolkit';
import { adminCreateProductEnum } from 'app/pages/admin/admin-product-create-page/screen/types';
import { actionPayload } from 'helper/index';

// create product

export const createProductRequest = createAction<any>(
  adminCreateProductEnum.CREATE_PRODUCT_REQUEST
);
export const createProductSuccess = createAction(
  adminCreateProductEnum.CREATE_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const createProductFailure = createAction(
  adminCreateProductEnum.CREATE_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);


// get product categories

export const getProductCategoriesRequest = createAction<any>(
  adminCreateProductEnum.GET_PRODUCT_CATEGORIES_REQUEST
);
export const getProductCategoriesSuccess = createAction(
  adminCreateProductEnum.GET_PRODUCT_CATEGORIES_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getProductCategoriesFailure = createAction(
  adminCreateProductEnum.GET_PRODUCT_CATEGORIES_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);


// upload image product

export const uploadProductImageRequest = createAction<any>(
  adminCreateProductEnum.UPLOAD_IMAGE_PRODUCT_REQUEST
);
export const uploadProductImageSuccess = createAction(
  adminCreateProductEnum.UPLOAD_IMAGE_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const uploadProductImageFailure = createAction(
  adminCreateProductEnum.UPLOAD_IMAGE_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// clear state

export const clearStateAdminCreateProductCreate = createAction<any>(
  adminCreateProductEnum.CLEAR_ADMIN_CREATE_PRODUCT_STATE
);
