import { Form, Select } from 'antd';
import { getProductCategoriesRequest } from 'app/pages/admin/admin-product-create-page/screen/action';
import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import './style.scss';

const { Option } = Select;

interface UserAnotherInfoFormIT {
  setStatusUser: any;
  statusUser: any;
  form: any;
}

export const ProductcategoriesForm: React.FC<any> = (
  props: UserAnotherInfoFormIT
) => {
  const dispatch = useDispatch();
  const state = useSelector(
    (state: RootState) => state?.adminUpdateProductReducer
  );

  const stateCreateProduct = useSelector(
    (state: RootState) => state?.adminCreateProductReducer
  );

  useEffect(() => {
    if (state?.dataResponse) {
      props?.form.setFieldsValue({
        category: state?.dataResponse?.category,
      });
    }
  }, [state?.dataResponse])

  useEffect(() => {
    dispatch(getProductCategoriesRequest(''));
  }, []);

  useEffect(() => {
    if (state?.dataResponse) {
      props?.form.setFieldsValue({
        contact: state?.dataResponse?.contact?.substring(1),
      });
    }
  }, [state?.dataResponse]);

  return (
    <Fragment>
      <div className="productAnotherForm__container">
        <div className="productAnotherForm__title">Categories</div>

        <div className="productAnotherForm__detailForm">
          <Form.Item
            label=" "
            name="category"
            rules={[
              {
                required: true,
                message: 'Please select category!',
              },
            ]}
          >
            <Select
              // mode="tags"
              size={'middle'}
              placeholder="Select caterogy"
              style={{ width: '100%' }}
            >
              {stateCreateProduct?.dataResponse?.length > 0
                ? stateCreateProduct?.dataResponse?.map(
                    (item: any, key: any) => {
                      return (
                        <Option key={key} value={item}>
                          {item}
                        </Option>
                      );
                    }
                  )
                : ''}
            </Select>
          </Form.Item>
        </div>
      </div>
    </Fragment>
  );
};
