import { Form, Select } from 'antd';
import 'app/pages/admin/admin-product-create-page/base/product-categories-form/style.scss';
import { ratings } from 'app/pages/admin/admin-product-create-page/base/product-rating-form/template';
import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';

const { Option } = Select;

export const ProductRatingForm: React.FC<any> = (props: any) => {
  const dispatch = useDispatch();
  const state = useSelector(
    (state: RootState) => state?.adminUpdateProductReducer
  );

  useEffect(() => {
    if (state?.dataResponse) {
      props?.form.setFieldsValue({
        rating: state?.dataResponse?.rating,
      });
    }
  }, [state?.dataResponse]);

  return (
    <Fragment>
      <div className="productAnotherForm__container">
        <div className="productAnotherForm__title">Rating</div>

        <div className="productAnotherForm__detailForm">
          <Form.Item
            label=" "
            name="rating"
            rules={[
              {
                required: true,
                message: 'Please select rating!',
              },
            ]}
          >
            <Select
              size={'middle'}
              placeholder="Select rating"
              style={{ width: '100%' }}
            >
              {ratings.map((item: any, key: any) => {
                return (
                  <Option key={key} value={item?.star}>
                    {item?.star}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
        </div>
      </div>
    </Fragment>
  );
};
