import { Breadcrumb, Button } from 'antd';
import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

export const ProductCreateHeader: React.FC<any> = () => {
  const histoty = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminCreateProductReducer
  );

  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item
          className="cursor-pointer"
          onClick={() => {
            histoty.push('/admin/product/list');
          }}
        >
          Product
        </Breadcrumb.Item>
        <Breadcrumb.Item>Create Product</Breadcrumb.Item>
      </Breadcrumb>

      <div className="productCreateHeader__detail">
        <h2 className="productCreateHeader__title">Create Product</h2>
        <div>
          <Button
            loading={state?.loadingBtnCreate}
            htmlType="submit"
            className="productCreateHeader__btn"
          >
            Add Product
          </Button>
        </div>
      </div>
    </Fragment>
  );
};
