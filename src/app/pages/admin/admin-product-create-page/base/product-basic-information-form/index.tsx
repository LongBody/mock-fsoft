import { Col, Form, Input, InputNumber, Row } from 'antd';
import React, { Fragment, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'types';
import './style.scss';

const { TextArea } = Input;

export const ProductBasicInformationForm: React.FC<any> = (props: any) => {
  const state = useSelector(
    (state: RootState) => state?.adminUpdateProductReducer
  );

  useEffect(() => {
    if (state?.dataResponse) {
      props?.form.setFieldsValue({
        name: state?.dataResponse?.name,
        description: state?.dataResponse?.description,
        brand: state?.dataResponse?.brand,
        countInStock: state?.dataResponse?.countInStock,
        price: state?.dataResponse?.price,
      });
    }
  }, [state?.dataResponse]);

  return (
    <Fragment>
      <div
        className={`productInformationForm__container ${
          props?.fileList?.length > 0
            ? 'productInformationForm__heightWithAvatar'
            : 'productInformationForm__height'
        }`}
      >
        <div className="productInformationForm__title">
          Product Basic Information
        </div>
        <div className="productInformationForm__detailForm">
          <Form.Item
            label="Name"
            name="name"
            rules={[
              {
                required: true,
                message: 'Please enter name!',
              },
            ]}
          >
            <Input style={{ width: '100%' }} placeholder="Enter name" />
          </Form.Item>

          <Form.Item
            label="Description"
            name="description"
            rules={[
              {
                required: true,
                message: 'Please enter description!',
              },
            ]}
          >
            <TextArea
              rows={4}
              placeholder="Enter description"
              maxLength={1000}
            />
          </Form.Item>

          <Row>
            <Col xl={12} className="productInformationForm__paddingColRight">
              <Form.Item
                label="Price"
                name="price"
                rules={[
                  {
                    required: true,
                    message: 'Please enter price!',
                  },
                ]}
              >
                <InputNumber
                  type={'number'}
                  style={{ width: '100%' }}
                  placeholder="Enter price"
                  addonAfter="$"
                />
              </Form.Item>
            </Col>
            <Col xl={12} className="productInformationForm__paddingColLeft">
              <Form.Item
                label="Diccount Percent"
                name="discount"
                // rules={[
                //   {
                //     required: true,
                //     message: 'Please enter name!',
                //   },
                // ]}
              >
                <InputNumber
                  type={'number'}
                  style={{ width: '100%' }}
                  addonAfter="%"
                  placeholder="Enter discount percent"
                />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item
            label="Brand"
            name="brand"
            rules={[
              {
                required: true,
                message: 'Please enter brand!',
              },
            ]}
          >
            <Input style={{ width: '100%' }} placeholder="Enter brand" />
          </Form.Item>

          <Form.Item label="Stock quantity" name="countInStock">
            <Input
              style={{ width: '100%' }}
              placeholder="Enter stock quantity"
            />
          </Form.Item>

          <div className="mt-10 color-error">
            {state?.message ? state?.message : ''}
          </div>
        </div>
      </div>
    </Fragment>
  );
};
