import { Col, Form, Row } from 'antd';
import { ProductcategoriesForm } from 'app/pages/admin/admin-product-create-page/base/product-categories-form';
import { ProductImageForm } from 'app/pages/admin/admin-product-create-page/base/product-image-form';
import { ProductCreateHeader } from 'app/pages/admin/admin-product-create-page/base/product-create-header';
import { ProductBasicInformationForm } from 'app/pages/admin/admin-product-create-page/base/product-basic-information-form';
import {
  createProductRequest,
  uploadProductImageRequest,
} from 'app/pages/admin/admin-product-create-page/screen/action';
import { ProductCreate } from 'app/pages/admin/admin-product-create-page/screen/types';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './style.scss';
import { RootState } from 'types';
import { ProductRatingForm } from 'app/pages/admin/admin-product-create-page/base/product-rating-form';

export const ProductCreateForm: React.FC<any> = () => {
  const state = useSelector(
    (state: RootState) => state?.adminCreateProductReducer
  );
  const [form] = Form.useForm();
  const [fileList, setFileList]: any = useState([]);
  const [previewImage, setPreviewImage]: any = useState('');
  const [values, setValues]: any = useState({});

  const dispatch = useDispatch();

  const onFinish = (values: any) => {
    // create new FormData
    setValues(values);
    const formData = new FormData();
    let file = fileList[0];
    // append file in form data
    if (file) {
      formData.append('image', file?.originFileObj);
      dispatch(uploadProductImageRequest(formData));
    }
  };

  const creatNewProduct = (image: string) => {
    const imageArr: any = [];
    imageArr.push(image);
    const body: ProductCreate = {
      name: values?.name,
      brand: values?.brand,
      category: values?.category,
      description: values?.description,
      price: values?.price,
      rating: values?.rating,
      countInStock: values?.countInStock !== '' ? values?.countInStock : 0,
      imageUrls: imageArr,
    };
    if (values && body) {
      dispatch(createProductRequest(body));
    }
  };

  useEffect(() => {
    if (
      state?.uploadImageStatus === 201 &&
      state?.imageUrl &&
      state?.message === ''
    ) {
      creatNewProduct(state?.imageUrl);
    }
  }, [state]);

  return (
    <Fragment>
      <Form form={form} layout="vertical" onFinish={onFinish}>
        <ProductCreateHeader />
        <div>
          <Row>
            <Col xs={12} xl={16} className="userProductForm__leftSide">
              <ProductBasicInformationForm fileList={fileList} form={form} />
            </Col>
            <Col xs={12} xl={8} className="userProductForm__rightSide">
              <ProductImageForm
                form={form}
                fileList={fileList}
                setFileList={setFileList}
                previewImage={previewImage}
                setPreviewImage={setPreviewImage}
              />
              <ProductcategoriesForm form={form} />
              <ProductRatingForm form={form} />
            </Col>
          </Row>
        </div>
      </Form>
    </Fragment>
  );
};
