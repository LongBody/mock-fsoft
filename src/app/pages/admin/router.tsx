// ADMIN PAGE
import { Route } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { AdminDashboardPage } from 'app/pages/admin/admin-dashboard-page/screen/Loadable';
import { AdminUserManagementPage } from 'app/pages/admin/admin-user-management/screen/Loadable';
import { AdminDetailUserPage } from 'app/pages/admin/admin-user-detail-page/screen/Loadable';
import { AdminUserCreatePage } from 'app/pages/admin/admin-user-create-page/screen/Loadable';
import { AdminUserUpdatePage } from 'app/pages/admin/admin-user-update-page/screen/Loadable';
import { AdminProductCreatePage } from 'app/pages/admin/admin-product-create-page/screen/Loadable';
import { AdminProductListPage } from 'app/pages/admin/admin-product-list-page/screen/Loadable';
import { AdminProductUpdatePage } from 'app/pages/admin/admin-product-update-page/screen/Loadable';
import { AdminOrderListPage } from 'app/pages/admin/admin-order-list-page/screen/Loadable';
import { AdminOrderDetailPage } from 'app/pages/admin/admin-order-detail-page/screen/Loadable';

export const PrivateAdminRoute: React.FC<any> = (props) => {
  const token = localStorage.getItem('accessToken');
  const userInfoLocal = localStorage.getItem('user');
  let userInfo: any;

  if (userInfoLocal) {
    userInfo = JSON.parse(atob(userInfoLocal));
  }

  const { component: Component, ...restProps } = props;

  if (!Component) return null;

  return (
    <Route
      {...restProps}
      render={(routeRenderProps) =>
        token && userInfo?.role === 'admin' ? (
          <Component {...routeRenderProps} />
        ) : (
          <Redirect
            to={{
              pathname: '/',
              state: { from: routeRenderProps.location },
            }}
          />
        )
      }
    />
  );
};

export const adminRouter = [
  { path: '/admin/dashboard', component: AdminDashboardPage },
  { path: '/admin/user/list', component: AdminUserManagementPage },
  { path: '/admin/user/detail/:id', component: AdminDetailUserPage },
  { path: '/admin/user/create', component: AdminUserCreatePage },
  { path: '/admin/user/update/:id', component: AdminUserUpdatePage },
  { path: '/admin/product/create', component: AdminProductCreatePage },
  { path: '/admin/product/list', component: AdminProductListPage },
  { path: '/admin/product/update/:id', component: AdminProductUpdatePage },
  { path: '/admin/order/list', component: AdminOrderListPage },
  { path: '/admin/order/detail/:id', component: AdminOrderDetailPage },
];
