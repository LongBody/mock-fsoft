import {
  getDetailUserRequest,
  getDetailUserSuccess,
  getDetailUserFailure,
} from 'app/pages/admin/admin-user-detail-page/screen/action';
import { adminDetailUserEnum } from 'app/pages/admin/admin-user-detail-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin get detail user
 *
 * @param {Object} action
 *
 */

export function* getListUserSaga({
  payload,
}: ReturnType<typeof getDetailUserRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_USERS}/${payload?.id}`,
      apiMethod.GET,
      ''
    );
    yield put(getDetailUserSuccess(response?.data));
  } catch (error: any) {
    yield put(getDetailUserFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(adminDetailUserEnum.GET_DETAIL_USER_REQUEST, getListUserSaga),
  ]);
}
