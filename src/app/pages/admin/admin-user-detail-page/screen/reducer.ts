import { createReducer } from '@reduxjs/toolkit';
import { adminDetailUserEnum } from 'app/pages/admin/admin-user-detail-page/screen/types';

// The initial state of the App
export const initialState: any = {
  loading: false,
  dataResponse: {},
};

export const adminDetailUserReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(adminDetailUserEnum.GET_DETAIL_USER_REQUEST, (state, action) => {
      state.dataResponse = {};
      state.loading = true;
      return state;
    })
    .addCase(adminDetailUserEnum.GET_DETAIL_USER_SUCCESS, (state, action: any) => {
      state.loading = false;
      state.dataResponse = action?.payload?.data
      return state;
    })
    .addCase(adminDetailUserEnum.GET_DETAIL_USER_FAILURE, (state, action: any) => {
      state.loading = false;
      return state;
    })

    // clear state

    .addCase(adminDetailUserEnum.CLEAR_ADMIN_DETAIL_USER_STATE, (state) => {
      state.loading = false;
      state.dataResponse = {};
      return state;
    });
});
