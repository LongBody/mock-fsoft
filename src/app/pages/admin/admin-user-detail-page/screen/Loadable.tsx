/**
 * Asynchronously loads the component for NotFoundPage
 */

import { lazyLoad } from 'utils/loadable';
import { LoadingIndicator } from 'app/components/loading-indicator';

export const AdminDetailUserPage = lazyLoad(
  () => import('./index'),
  (module) => module.AdminDetailUserPage,
  {
    fallback: <LoadingIndicator />,
  }
);
