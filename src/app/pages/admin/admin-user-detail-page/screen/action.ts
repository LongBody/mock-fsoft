import { createAction } from '@reduxjs/toolkit';
import { adminDetailUserEnum } from 'app/pages/admin/admin-user-detail-page/screen/types';
import { actionPayload } from 'helper/index';

// get detail user in admin page
export const getDetailUserRequest = createAction<any>(adminDetailUserEnum.GET_DETAIL_USER_REQUEST);
export const getDetailUserSuccess = createAction(
  adminDetailUserEnum.GET_DETAIL_USER_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getDetailUserFailure = createAction(
  adminDetailUserEnum.GET_DETAIL_USER_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// clear state

export const clearStateAdminDetailUser = createAction<any>(
  adminDetailUserEnum.CLEAR_ADMIN_DETAIL_USER_STATE
);
