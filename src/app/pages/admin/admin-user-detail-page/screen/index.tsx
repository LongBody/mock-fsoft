import { LayoutAdmin } from 'app/components/admin-layout';
import React, { Fragment, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { DetailUser } from 'app/pages/admin/admin-user-detail-page/base/detail-user';
import { clearStateAdminDetailUser } from 'app/pages/admin/admin-user-detail-page/screen/action';
import { useDispatch } from 'react-redux';

export const AdminDetailUserPage: React.FC<any> = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    return () => {
      dispatch(clearStateAdminDetailUser(''));
    };
  }, []);

  return (
    <Fragment>
      <Helmet>
        <title>Admin Detail User</title>
        <meta name="description" content="Admin Dashboard" />
      </Helmet>
      <LayoutAdmin content={<DetailUser />} />
    </Fragment>
  );
};
