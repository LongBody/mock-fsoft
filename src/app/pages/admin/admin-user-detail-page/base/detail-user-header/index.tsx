import { Breadcrumb } from 'antd';
import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

export const UserDetailHeader: React.FC<any> = () => {
  const histoty = useHistory();
  const state = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item
          className="cursor-pointer"
          onClick={() => {
            histoty.push('/admin/user/list');
          }}
        >
          User
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          User Detail #{state?.dataResponse?.id}
        </Breadcrumb.Item>
      </Breadcrumb>

      <div className="userDetailHeader__detail">
        <h2 className="userDetailHeader__title">
          User Detail #{state?.dataResponse?.id}
        </h2>
        <span className="userDetailHeader__userId">
          User ID: {state?.dataResponse?.id}
        </span>
      </div>
    </Fragment>
  );
};
