import { Input, Pagination, Table, Tag, Row, Col, Spin } from 'antd';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { UserDetailHeader } from 'app/pages/admin/admin-user-detail-page/base/detail-user-header';
import { getDetailUserRequest } from 'app/pages/admin/admin-user-detail-page/screen/action';
import './style.scss';
import { useParams } from 'react-router-dom';

export const DetailUser: React.FC<any> = () => {
  const dispatch = useDispatch();
  let { id }: any = useParams();

  const state = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  useEffect(() => {
    if (id) {
      dispatch(getDetailUserRequest({ id: id }));
    }
  }, [id]);

  return (
    <Fragment>
      <Spin spinning={state?.loading} delay={100}>
        <UserDetailHeader />
        <div className="userDetail">
          {/* content of user detail */}
          <div className="userDetail__container">
            <div className="userDetail__sub">
              <div className="userDetail__profile">
                <div className="flex justify-center">
                  <img
                    className="userDetail__profile-detail-avatar"
                    src={state?.dataResponse?.avatar}
                    alt={state?.dataResponse?.avatar}
                  />
                </div>

                <div className="userDetail__profile-detail">
                  <div>
                    <div className="userDetail__profile-detail-username">
                      {state?.dataResponse?.username}
                    </div>
                    <div className="userDetail__profile-detail-email">
                      {state?.dataResponse?.email}
                    </div>
                    <div className="userDetail__profile-detail-contact">
                      {state?.dataResponse?.contact}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="flex justify-center">
            <div className="line__divide" />
          </div>

          <div className="userDetail__container__info-status">
            <div className="userDetail__sub">
              <div className="userDetail__info-status">
                <Row>
                  <Col xs={12} xl={12} className="bold userDetail__info-tag">
                    Role:
                  </Col>
                  <Col xs={12} xl={12} className="userDetail__info-tag">
                    {state?.dataResponse?.role === 'admin' ? (
                      <Tag color={'success'}>Admin</Tag>
                    ) : (
                      <Tag color={'error'}>Customer</Tag>
                    )}
                  </Col>
                </Row>

                <Row className="mt-20">
                  <Col xs={12} xl={12} className="bold userDetail__info-tag">
                    Status:
                  </Col>
                  <Col xs={12} xl={12} className="userDetail__info-tag">
                    {state?.dataResponse?.isActive ? (
                      <Tag color={'success'}>Active</Tag>
                    ) : (
                      <Tag color={'error'}>Deactive</Tag>
                    )}
                  </Col>
                </Row>

                <Row className="mt-20">
                  <Col xs={12} xl={12} className="bold userDetail__info-tag">
                    Verify Email:
                  </Col>
                  <Col xs={12} xl={12} className="userDetail__info-tag">
                    {state?.dataResponse?.isEmailVerified ? (
                      <i className="fa-solid fa-circle-check userDetail__info-verified"></i>
                    ) : (
                      <i className="fa-solid fa-circle-xmark userDetail__info-not-verify"></i>
                    )}
                  </Col>
                </Row>

                <Row className="mt-20">
                  <Col xs={12} xl={12} className="bold userDetail__info-tag">
                    Verify Contact:
                  </Col>
                  <Col xs={12} xl={12} className="userDetail__info-tag">
                    {state?.dataResponse?.isContactVerified ? (
                      <i className="fa-solid fa-circle-check userDetail__info-verified"></i>
                    ) : (
                      <i className="fa-solid fa-circle-xmark userDetail__info-not-verify"></i>
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        </div>
      </Spin>
    </Fragment>
  );
};
