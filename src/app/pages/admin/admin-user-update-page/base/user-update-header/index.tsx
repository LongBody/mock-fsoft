import { Breadcrumb, Button } from 'antd';
import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

export const UserUpdateHeader: React.FC<any> = () => {
  const histoty = useHistory();

  const stateUpdate = useSelector(
    (state: RootState) => state?.adminUpdateUserReducer
  );

  const stateDetail = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item
          className="cursor-pointer"
          onClick={() => {
            histoty.push('/admin/user/list');
          }}
        >
          User
        </Breadcrumb.Item>
        <Breadcrumb.Item>Update User </Breadcrumb.Item>
      </Breadcrumb>

      <div className="userUpdateHeader__detail">
        <div>
          <h2 className="userUpdateHeader__title">
            Update User #{stateDetail?.dataResponse?.id}
          </h2>
          <span className="userupdateHeader__userId">
            User ID: {stateDetail?.dataResponse?.id}
          </span>
        </div>

        <div>
          <Button
            loading={stateUpdate?.loading}
            htmlType="submit"
            className="userUpdateHeader__btn"
          >
            Update User
          </Button>
        </div>
      </div>
    </Fragment>
  );
};
