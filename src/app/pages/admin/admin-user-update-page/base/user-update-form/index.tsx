import { Col, Form, Row, Spin } from 'antd';
import { UserAnotherInfoForm } from 'app/pages/admin/admin-user-create-page/base/user-another-form';
import { UserAvatarForm } from 'app/pages/admin/admin-user-create-page/base/user-avatar-form';
import { UserUpdateHeader } from 'app/pages/admin/admin-user-update-page/base/user-update-header';
import { UserInformationForm } from 'app/pages/admin/admin-user-update-page/base/user-information-form';
import { uploadImageRequest } from 'app/pages/admin/admin-user-create-page/screen/action';
import { updateUserRequest } from 'app/pages/admin/admin-user-update-page/screen/action';
import { UserUpdate } from 'app/pages/admin/admin-user-update-page/screen/types';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './style.scss';
import { RootState } from 'types';

export const UserUpdateForm: React.FC<any> = () => {
  const state = useSelector(
    (state: RootState) => state?.adminCreateUserReducer
  );

  const [form] = Form.useForm();

  const [fileList, setFileList]: any = useState([]);
  const [previewImage, setPreviewImage]: any = useState('');
  const [values, setValues]: any = useState({});

  const [statusUser, setStatusUser] = useState({
    isActive: true,
    isContactVerified: false,
    isEmailVerified: false,
  });

  const stateDetail = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  useEffect(() => {
    if (stateDetail?.dataResponse) {
      setStatusUser({
        isActive: stateDetail?.dataResponse?.isActive,
        isContactVerified: stateDetail?.dataResponse?.isContactVerified,
        isEmailVerified: stateDetail?.dataResponse?.isEmailVerified,
      });

      setPreviewImage(stateDetail?.dataResponse?.avatar);
      setFileList([
        {
          uid: '-1',
          name: 'anh',
          url: stateDetail?.dataResponse?.avatar,
          status: 'done',
        },
      ]);
    }
  }, [stateDetail?.dataResponse]);

  const dispatch = useDispatch();

  const updateUser = (avatar: string) => {
    const contactSubmit = '0' + form.getFieldValue('contact');
    const body: UserUpdate = {
      id: stateDetail?.dataResponse?.id,
      username: form.getFieldValue('username'),
      role: form.getFieldValue('role'),
      isContactVerified: statusUser?.isContactVerified,
      isEmailVerified: statusUser?.isEmailVerified,
      contact: '0' + form.getFieldValue('contact'),
      avatar: avatar,
      isActive: statusUser?.isActive,
    };
    if (contactSubmit === stateDetail?.dataResponse?.contact) {
      delete body?.contact;
    }
    if (values && body && body?.id) {
      dispatch(updateUserRequest(body));
    }
  };

  const onFinish = (values: any) => {
    // create new FormData
    const formData = new FormData();
    let file = fileList[0];
    // append file in form data
    setValues(values);

    if (fileList.length > 0 && fileList[0]?.status !== 'done' && file) {
      formData.append('image', file?.originFileObj);
      dispatch(uploadImageRequest(formData));
    } else {
      updateUser(stateDetail?.dataResponse?.avatar);
    }
  };

  useEffect(() => {
    if (
      state?.uploadImageStatus === 201 &&
      state?.imageUrl &&
      state?.message === ''
    ) {
      updateUser(state?.imageUrl);
    }
  }, [state]);

  return (
    <Fragment>
      <Spin spinning={stateDetail?.loading} delay={100}>
        <Form layout="vertical" onFinish={onFinish} form={form}>
          <UserUpdateHeader />
          <div>
            <Row>
              <Col xs={12} xl={16} className="userCreateForm__leftSide">
                <UserInformationForm form={form} />
              </Col>
              <Col xs={12} xl={8} className="userCreateForm__rightSide">
                <UserAvatarForm
                  form={form}
                  fileList={fileList}
                  setFileList={setFileList}
                  previewImage={previewImage}
                  setPreviewImage={setPreviewImage}
                />
                <UserAnotherInfoForm
                  form={form}
                  setStatusUser={setStatusUser}
                  statusUser={statusUser}
                />
              </Col>
            </Row>
          </div>
        </Form>
      </Spin>
    </Fragment>
  );
};
