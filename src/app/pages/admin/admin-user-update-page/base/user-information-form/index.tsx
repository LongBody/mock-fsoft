import { Form, Input, Select } from 'antd';
import React, { Fragment, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'types';
import './style.scss';
const { Option } = Select;

export const UserInformationForm: React.FC<any> = (props: any) => {
  const state = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  useEffect(() => {
    if (state?.dataResponse) {
      props?.form.setFieldsValue({
        email: state?.dataResponse?.email,
        password: '!@#$%^&',
        confirmPassword: '!@#$%^&',
        username: state?.dataResponse?.username,
        role : state?.dataResponse?.role,
      });
    }
  }, [state]);

  return (
    <Fragment>
      <div className="userInformationForm__container">
        <div className="userInformationForm__title">User Information</div>
        <div className="userInformationForm__detailForm">
          <Form.Item
            label="Name"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please enter name!',
              },
            ]}
          >
            <Input style={{ width: '100%' }} placeholder="Enter name" />
          </Form.Item>

          <Form.Item label="Email" name="email">
            <Input
              disabled
              style={{ width: '100%' }}
              placeholder="Enter email"
            />
          </Form.Item>

          <Form.Item label="Password" name="password">
            <Input.Password
              disabled
              style={{ width: '100%' }}
              placeholder="Enter password"
            />
          </Form.Item>

          <Form.Item label="Retype password" name="confirmPassword">
            <Input.Password
              disabled
              style={{ width: '100%' }}
              placeholder="Enter retype password"
            />
          </Form.Item>

          <Form.Item
            label="Role"
            name="role"
            rules={[
              {
                required: true,
                message: 'Please choose a role!',
              },
            ]}
          >
            <Select style={{ width: '100%' }}>
              <Option value="admin">Admin</Option>
              <Option value="user">Customer</Option>
            </Select>
          </Form.Item>

          <div className="mt-10 color-error">
            {state?.message ? state?.message : ''}
          </div>
        </div>
      </div>
    </Fragment>
  );
};
