import { LayoutAdmin } from 'app/components/admin-layout';
import React, { Fragment, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { UserUpdateForm } from 'app/pages/admin/admin-user-update-page/base/user-update-form';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { message } from 'antd';
import { clearStateAdminUpdateUser } from 'app/pages/admin/admin-user-update-page/screen/action';
import { clearStateAdminDetailUser } from 'app/pages/admin/admin-user-detail-page/screen/action';
import { useHistory } from 'react-router-dom';
import { getDetailUserRequest } from 'app/pages/admin/admin-user-detail-page/screen/action';
import { useParams } from 'react-router-dom';

export const AdminUserUpdatePage: React.FC<any> = () => {
  let { id }: any = useParams();
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector(
    (state: RootState) => state?.adminUpdateUserReducer
  );

  useEffect(() => {
    if (id) {
      dispatch(getDetailUserRequest({ id: id }));
    }
  }, [id]);

  useEffect(() => {
    if (state?.status === 200 && state?.messageSucess === 'updated') {
      dispatch(clearStateAdminUpdateUser(''));
      dispatch(clearStateAdminDetailUser(''));
      message.success('Update user successfully!');
      history.push('/admin/user/list');
    }
  }, [state?.messageSucess]);

  return (
    <Fragment>
      <Helmet>
        <title>Admin User Update Dashboard</title>
        <meta name="description" content="Admin Dashboard" />
      </Helmet>
      <LayoutAdmin content={<UserUpdateForm />} />
    </Fragment>
  );
};
