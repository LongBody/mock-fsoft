import { createAction } from '@reduxjs/toolkit';
import { adminUpdateUserEnum } from 'app/pages/admin/admin-user-update-page/screen/types';
import { actionPayload } from 'helper/index';

// Update User

export const updateUserRequest = createAction<any>(
  adminUpdateUserEnum.UPDATE_USER_REQUEST
);
export const updateUserSuccess = createAction(
  adminUpdateUserEnum.UPDATE_USER_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updateUserFailure = createAction(
  adminUpdateUserEnum.UPDATE_USER_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// clear state

export const clearStateAdminUpdateUser = createAction<any>(
  adminUpdateUserEnum.CLEAR_ADMIN_UPDATE_USER_STATE
);
