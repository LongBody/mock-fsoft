import {
  updateUserRequest,
  updateUserSuccess,
  updateUserFailure,
} from 'app/pages/admin/admin-user-update-page/screen/action';
import { adminUpdateUserEnum } from 'app/pages/admin/admin-user-update-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin update user
 *
 * @param {Object} action
 *
 */

export function* updateUserSaga({
  payload,
}: ReturnType<typeof updateUserRequest>) {
  try {
    const body = {
      username: payload?.username,
      role: payload?.role,
      isContactVerified: payload?.isContactVerified,
      isEmailVerified: payload?.isEmailVerified,
      contact: payload?.contact,
      avatar: payload?.avatar,
      isActive: payload?.isActive,
    }
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_USERS}/${payload?.id}`,
      apiMethod.PATCH,
      body
    );
    yield put(updateUserSuccess(response?.data));
  } catch (error: any) {
    yield put(updateUserFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(adminUpdateUserEnum.UPDATE_USER_REQUEST, updateUserSaga),
  ]);
}
