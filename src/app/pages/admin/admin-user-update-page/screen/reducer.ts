import { createReducer } from '@reduxjs/toolkit';
import { adminUpdateUserEnum } from 'app/pages/admin/admin-user-update-page/screen/types';
// import { setCookie } from 'utils/request';
// The initial state of the App
export const initialState: any = {
  loading: false,
  message: '',
  status: '',
  messageSucess: '',
};

export const adminUpdateUserReducer = createReducer(initialState, (builder) => {
  builder

    // create user
    .addCase(adminUpdateUserEnum.UPDATE_USER_REQUEST, (state, action) => {
      state.status = '';
      state.message = '';
      state.loading = true;
      state.messageSucess = '';
      return state;
    })
    .addCase(adminUpdateUserEnum.UPDATE_USER_SUCCESS, (state, action: any) => {
      state.loading = false;
      state.message = '';
      state.messageSucess = 'updated';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(adminUpdateUserEnum.UPDATE_USER_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.messageSucess = '';
      state.loading = false;
      return state;
    })
    // clear state

    .addCase(adminUpdateUserEnum.CLEAR_ADMIN_UPDATE_USER_STATE, (state) => {
      state.loading = false;
      state.message = '';
      state.status = '';
      state.messageSucess = '';
      return state;
    });
});
