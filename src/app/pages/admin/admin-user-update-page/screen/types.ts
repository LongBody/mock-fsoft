export enum adminUpdateUserEnum {
  UPDATE_USER_REQUEST = 'APP/ADMIN/UPDATE_USER_REQUEST',
  UPDATE_USER_SUCCESS = 'APP/ADMIN/UPDATE_USER_SUCCESS',
  UPDATE_USER_FAILURE = 'APP/ADMIN/UPDATE_USER_FAILURE',

  CLEAR_ADMIN_UPDATE_USER_STATE = 'APP/ADMIN/CLEAR_ADMIN_UPDATE_USER_STATE',
}

export interface UserUpdate {
  id: any,
  username: string;
  isActive: boolean;
  isContactVerified: boolean;
  isEmailVerified: boolean;
  avatar?: string;
  contact?: string;
  role: string;
}
