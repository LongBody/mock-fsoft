import { message, Modal, Pagination, Select, Table, Tag } from 'antd';
import { OrderListHeader } from 'app/pages/admin/admin-order-list-page/base/list-order-header';
import { getListOrderRequest } from 'app/pages/admin/admin-order-list-page/screen/action';
import { deleteProductRequest } from 'app/pages/admin/admin-product-list-page/screen/action';
import moment from 'moment';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';
const { Option } = Select;

export const OrderListTable: React.FC<any> = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector((state: RootState) => state?.adminListOrderReducer);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [userSelected, setUserSelected]: any = useState({});
  const [searchTerm, setSearchTerm] = useState('');

  const [pagination, setPagination]: any = useState({
    pageSize: 10,
    pageIndex: 1,
  });

  const columns: any = [
    {
      title: 'Num',
      dataIndex: 'i',
      align: 'center',
      key: 'i',
      render: (i, record, index) => <b>{index + 1}</b>,
    },
    {
      title: 'Order ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
      render: (id) => <Link to={`/admin/order/detail/${id}`}>{id}</Link>,
    },
    {
      title: 'Amount',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      align: 'center',
      render: (totalPrice) => <span>{totalPrice ? totalPrice : ''}</span>,
    },
    {
      title: 'Contact',
      dataIndex: 'contact',
      key: 'contact',
      align: 'center',
      render: (contact) => <span>{contact}</span>,
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
      render: (address) => <span>{address}</span>,
    },
    {
      title: 'Date',
      dataIndex: 'createdAt',
      key: 'createdAt',
      align: 'center',
      render: (createdAt) => (
        <span>{moment(createdAt).format('DD/MM/YYYY')}</span>
      ),
    },
    {
      title: 'Paided',
      dataIndex: 'isPaid',
      key: 'isPaid',
      align: 'center',
      render: (isPaid) => {
        if (isPaid) {
          return <Tag color={'#FFD333'}>Yes</Tag>;
        } else return <Tag color={'#366AB8'}>No</Tag>;
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render: (status) => <span>{status}</span>,
    },
    {
      title: '',
      dataIndex: 'action',
      key: 'action',
      align: 'center',
      render: (action, record) => (
        <div>
          <i
            className="fa-solid fa-pen-to-square orderListTable--editIcon"
            onClick={() => {
              history.push(`/admin/order/detail/${record?.id}`);
            }}
          ></i>
        </div>
      ),
    },
  ];

  const getListOrder = () => {
    const body = {
      size: 10,
      page: 1,
      id: 453,
    };
    // get data user
    dispatch(getListOrderRequest(body));
  };

  useEffect(() => {
    getListOrder();
  }, []);

  useEffect(() => {
    if (state?.status === 200 && state?.message === '' && isModalVisible) {
      setIsModalVisible(false);
      getListOrder();
      message.success('Delete product successfully!');
    }
  }, [state]);

  /**
   * @function onChangepage
   * @param {number} value the page index
   */

  const onChangepage = (value: any) => {
    // set state for page index
    setPagination({
      ...pagination,
      pageIndex: value,
    });
    const body = {
      size: parseInt(pagination?.pageSize),
      page: value,
    };
    dispatch(getListOrderRequest(body));
  };

  return (
    <Fragment>
      <OrderListHeader />
      {/* content of data table */}
      <div className="orderListTable__container">
        {/* search view */}
        <div className="orderListTable__search">
          <Select
            defaultValue="All"
            style={{ width: '100%' }}
            onChange={(value: any) => {
              const body = {
                size: pagination?.pageSize,
                page: 1,
                status: value,
              };
              dispatch(getListOrderRequest(body));
            }}
          >
            <Option value="">All</Option>
            <Option value="Processing">Processing</Option>
            <Option value="Shipping">Shipping</Option>
            <Option value="Cancel">Cancel</Option>
          </Select>
          {/* <Input
            prefix={
              <i className="fa-solid fa-magnifying-glass orderListTable__search-icon"></i>
            }
            onChange={(e: any) => {
              setSearchTerm(e.target.value);
            }}
            size="large"
            placeholder="Search producs"
          /> */}
        </div>
        {/* table view */}
        <Table
          rowKey="id"
          bordered
          className="mb-10"
          columns={columns}
          loading={state.loading}
          scroll={{ x: 600 }}
          pagination={false}
          dataSource={
            state?.dataResponse?.length > 0 ? state?.dataResponse : []
          }
        />

        <div className="orderListTable__pagination">
          <Pagination
            onChange={onChangepage}
            defaultCurrent={state?.dataResponse?.currentPage}
            total={state?.total ? state?.total : 1}
            pageSize={pagination?.pageSize}
          />

          <div>
            Items per page{' '}
            <input
              type="number"
              id="pageSize"
              min="1"
              className="itemPerPage"
              onChange={(value: any) => {
                // set state for page size
                setPagination({
                  ...pagination,
                  pageSize: value?.target?.value,
                });

                const body = {
                  size: parseInt(value?.target?.value),
                  page: pagination?.pageIndex,
                };

                // check if data page is not empty to call api
                if (value?.target?.value !== '') {
                  dispatch(getListOrderRequest(body));
                }
              }}
              defaultValue={pagination?.pageSize}
              max="50"
            ></input>
          </div>
        </div>
      </div>

      <Modal
        title="Confirm delete"
        visible={isModalVisible}
        onOk={() => {
          dispatch(
            deleteProductRequest({
              id: userSelected?.id,
            })
          );
        }}
        confirmLoading={state?.loadingBtnDelete}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        okText="Delete"
        okButtonProps={{
          style: { backgroundColor: '#F02020', borderColor: '#F02020' },
        }}
      >
        <p>Are you sure to delete product #{userSelected?.id}</p>
      </Modal>
    </Fragment>
  );
};
