import { Breadcrumb } from 'antd';
import React, { Fragment } from 'react';
import './style.scss';

export const OrderListHeader: React.FC<any> = () => {
  return (
    <Fragment>
      <Breadcrumb>
        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
        <Breadcrumb.Item>Order</Breadcrumb.Item>
      </Breadcrumb>

      <div className="orderListHeader__detail">
        <h2 className="orderListHeader__title">Order</h2>
      </div>
    </Fragment>
  );
};
