import { createReducer } from '@reduxjs/toolkit';
import { adminListOrderEnum } from 'app/pages/admin/admin-order-list-page/screen/types';
// The initial state of the App
export const initialState: any = {
  loading: false,
  message: '',
  status: '',
  total: '',
  loadingBtnDelete: false,
  dataResponse: {},
};

export const adminListOrderReducer = createReducer(initialState, (builder) => {
  builder
    // get list orders
    .addCase(adminListOrderEnum.GET_LIST_ORDER_REQUEST, (state, action) => {
      state.dataResponse = {};
      state.status = '';
      state.message = '';
      state.loading = true;
      return state;
    })
    .addCase(
      adminListOrderEnum.GET_LIST_ORDER_SUCCESS,
      (state, action: any) => {
        state.loading = false;
        state.status = action?.payload?.status;
        state.total = action?.payload?.data?.orders?.total;
        state.dataResponse = action?.payload?.data?.orders?.result;
        return state;
      }
    )
    .addCase(
      adminListOrderEnum.GET_LIST_ORDER_FAILURE,
      (state, action: any) => {
        state.status = action?.payload?.data?.status;
        state.loading = false;
        return state;
      }
    );
});
