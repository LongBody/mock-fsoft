import { createAction } from '@reduxjs/toolkit';
import { adminListOrderEnum } from 'app/pages/admin/admin-order-list-page/screen/types';
import { actionPayload } from 'helper/index';

// admin get list orders
export const getListOrderRequest = createAction<any>(
  adminListOrderEnum.GET_LIST_ORDER_REQUEST
);
export const getListOrderSuccess = createAction(
  adminListOrderEnum.GET_LIST_ORDER_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getListOrderFailure = createAction(
  adminListOrderEnum.GET_LIST_ORDER_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);
