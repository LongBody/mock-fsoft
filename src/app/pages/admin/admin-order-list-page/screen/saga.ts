import {
  getListOrderRequest,
  getListOrderSuccess,
  getListOrderFailure,
} from 'app/pages/admin/admin-order-list-page/screen/action';
import { adminListOrderEnum } from 'app/pages/admin/admin-order-list-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * admin get list orders
 *
 * @param {Object} action
 *
 */

export function* getListOrderSaga({
  payload,
}: ReturnType<typeof getListOrderRequest>) {
  try {
    let url = '';
    if (payload?.status) {
      url = `${API_URL}${API_CALL?.API_GET_ORDERS}?size=${payload?.size}&page=${payload?.page}&status=${payload?.status}`;
    } else {
      url = `${API_URL}${API_CALL?.API_GET_ORDERS}?size=${payload?.size}&page=${payload?.page}`;
    }
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(getListOrderSuccess(response?.data));
  } catch (error: any) {
    yield put(getListOrderFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(adminListOrderEnum.GET_LIST_ORDER_REQUEST, getListOrderSaga),
  ]);
}
