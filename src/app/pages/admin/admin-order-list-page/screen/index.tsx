import { LayoutAdmin } from 'app/components/admin-layout';
import { OrderListTable } from 'app/pages/admin/admin-order-list-page/base/list-order-datatable';
import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet-async';

export const AdminOrderListPage: React.FC<any> = () => {
  return (
    <Fragment>
      <Helmet>
        <title>Admin list order</title>
        <meta name="description" content="Admin list order" />
      </Helmet>
      <LayoutAdmin content={<OrderListTable />} />
    </Fragment>
  );
};
