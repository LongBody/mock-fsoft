import { Pagination, Table } from 'antd';
import { getMyOrderRequest } from 'app/pages/user/my-account-page/screen/action';
import moment from 'moment';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import './style.scss';

export const OrderHistoryTable: React.FC<any> = () => {
  const dispatch = useDispatch();

  const state = useSelector((state: RootState) => state?.myAcccountReducer);

  const [pagination, setPagination]: any = useState({
    pageSize: 10,
    pageIndex: 1,
  });

  const columns: any = [
    {
      title: 'Order',
      dataIndex: 'id',
      align: 'center',
      key: 'id',
      render: (id) => <b>#{id}</b>,
    },
    {
      title: 'Date',
      dataIndex: 'createdAt',
      key: 'createdAt',
      align: 'center',
      render: (createdAt) => (
        <span>{moment(createdAt).format('DD/MM/YYYY')}</span>
      ),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render: (status) => <span>{status}</span>,
    },
    {
      title: 'Total',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      align: 'center',
      render: (totalPrice) => <span>${totalPrice}</span>,
    },
  ];

  const getListProduct = () => {
    const body = {
      size: 10,
      page: 1,
    };
    // get my order
    dispatch(getMyOrderRequest(body));
  };

  useEffect(() => {
    getListProduct();
  }, []);

  const onChangepage = (value: any) => {
    // set state for page index
    setPagination({
      ...pagination,
      pageIndex: value,
    });
    const body = {
      size: parseInt(pagination?.pageSize),
      page: value,
    };
    dispatch(getMyOrderRequest(body));
  };

  return (
    <Fragment>
      <p
        style={{
          fontSize: 16,
          fontWeight: 'bold',
          padding: '10px 20px',
        }}
      >
        Order History
      </p>
      {/* content of data table */}
      <div className="userListTable__container">
        {/* table view */}
        <Table
          rowKey="id"
          bordered
          className="mb-10"
          columns={columns}
          loading={state.loading}
          scroll={{ x: 500 }}
          pagination={false}
          dataSource={
            state?.dataResponse?.length > 0 ? state?.dataResponse : []
          }
        />

        <div className="userListTable__pagination">
          <Pagination
            onChange={onChangepage}
            defaultCurrent={state?.dataResponse?.currentPage}
            total={state?.total ? state?.total : 1}
            pageSize={pagination?.pageSize}
          />

          <div>
            Items per page{' '}
            <input
              type="number"
              id="pageSize"
              min="1"
              className="itemPerPage"
              onChange={(value: any) => {
                // set state for page size
                setPagination({
                  ...pagination,
                  pageSize: value?.target?.value,
                });

                const body = {
                  size: parseInt(value?.target?.value),
                  page: pagination?.pageIndex,
                };

                // check if data page is not empty to call api
                if (value?.target?.value !== '') {
                  dispatch(getMyOrderRequest(body));
                }
              }}
              defaultValue={pagination?.pageSize}
              max="50"
            ></input>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
