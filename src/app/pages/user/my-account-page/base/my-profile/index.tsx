import { UploadOutlined } from '@ant-design/icons';
import { Button, Col, Form, message, Row, Spin, Upload } from 'antd';
import {
  clearStateAdminCreateUserCreate,
  uploadImageRequest,
} from 'app/pages/admin/admin-user-create-page/screen/action';
import { MyProfileEditModal } from 'app/pages/user/my-account-page/base/change-profile-modal';
import { OrderHistoryTable } from 'app/pages/user/my-account-page/base/order-history-table';
import {
  clearState,
  getMyOrderRequest,
  getMyProfileRequest,
  updateAvatarRequest,
} from 'app/pages/user/my-account-page/screen/action';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';

export const MyProfileTab: React.FC<any> = (props: any) => {
  const dispatch = useDispatch();
  const userInfoCookies = localStorage.getItem('user');
  // const userInfoCookies = getCookie('user-info');
  let userInfo: any;

  if (userInfoCookies) {
    userInfo = JSON.parse(atob(userInfoCookies));
  }

  const state: any = useSelector(
    (state: RootState) => state?.myAcccountReducer
  );

  const stateUser = useSelector(
    (state: RootState) => state?.adminCreateUserReducer
  );

  console.log(userInfo);

  const [fileList, setFileList]: any = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [updateContact, setUpdateContact] = useState(false);

  useEffect(() => {
    if (userInfo?.id) {
      dispatch(getMyProfileRequest(''));
    }
  }, [userInfo?.id]);

  const handleUpdateUserSuccess = () => {
    dispatch(clearState(''));
    message.success('Update user information successfully!');
    dispatch(getMyProfileRequest(''));
    const body = {
      size: 10,
      page: 1,
    };
    // get my order
    dispatch(getMyOrderRequest(body));
    setIsModalVisible(false);
    props?.setActiveTab('profile');
  };

  useEffect(() => {
    if (
      updateContact &&
      state?.updateUsername === 'updated' &&
      state?.updateContact === 'updated' &&
      isModalVisible
    ) {
      handleUpdateUserSuccess();
    }
    if (
      !updateContact &&
      state?.updateUsername === 'updated' &&
      isModalVisible
    ) {
      handleUpdateUserSuccess();
    }
  }, [state?.updateUsername, state?.updateContact]);

  useEffect(() => {
    if (state?.messageSucess === 'user-avatar-updated') {
      dispatch(clearState(''));
      const userUpdate = {
        ...userInfo,
        avatar: stateUser?.imageUrl,
      };
      localStorage.setItem('user', btoa(JSON.stringify(userUpdate)));
      message.success('Update avatar successfully!');
      dispatch(clearStateAdminCreateUserCreate(''));
      dispatch(getMyProfileRequest(''));
      props?.setActiveTab('profile');
    }
  }, [state?.messageSucess]);

  const handleChangeUpload: any = async (fileLists: any) => {
    setFileList(fileLists.fileList);

    const formData = new FormData();
    let file = fileLists.fileList[0];
    // append file in form data
    if (file) {
      formData.append('image', file?.originFileObj);
      dispatch(uploadImageRequest(formData));
    }
  };

  useEffect(() => {
    if (
      stateUser?.uploadImageStatus !== '' &&
      stateUser?.imageUrl &&
      stateUser?.message === ''
    ) {
      const body = {
        avatar: stateUser?.imageUrl,
      };
      dispatch(updateAvatarRequest(body));
    }
  }, [stateUser]);

  return (
    <Fragment>
      <Spin spinning={state?.loadingUser || stateUser?.loading}>
        <div
          style={{
            backgroundColor: '#E2E1E1',
          }}
        >
          <Row
            style={{
              padding: '20px 20px 20px 0px',
            }}
          >
            <Col
              xl={5}
              style={{
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              <div>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                  }}
                >
                  <img
                    src={userInfo?.avatar}
                    style={{ width: 100, height: 100, borderRadius: '50%' }}
                  />
                </div>
                <Form
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                  }}
                >
                  <Upload
                    action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                    showUploadList={false}
                    maxCount={1}
                    defaultFileList={fileList}
                    beforeUpload={() => {
                      return false;
                    }}
                    onChange={handleChangeUpload}
                  >
                    <Button
                      className="userAvatarForm__upload-button"
                      icon={<UploadOutlined />}
                      type="link"
                    >
                      Choose file
                    </Button>
                  </Upload>
                </Form>
              </div>
            </Col>
            <Col xl={19}>
              <div>
                <div
                  style={{
                    fontSize: 18,
                    fontWeight: ' bold',
                  }}
                >
                  {state?.dataResponseUser?.username}
                </div>
                <div>
                  <span className="bold">Email</span>:{' '}
                  {state?.dataResponseUser?.email}
                </div>
                <div>
                  <span className="bold">Address</span>:{' '}
                  {state?.dataResponseUser?.address}
                </div>
                <div>
                  <span className="bold">Phone</span>:{' '}
                  {state?.dataResponseUser?.contact}
                </div>
                <Button
                  type="primary"
                  style={{
                    backgroundColor: '#FFD333',
                    borderColor: '#FFD333',
                    color: 'black',
                    marginTop: 20,
                    fontWeight: 'bold',
                  }}
                  onClick={() => {
                    setIsModalVisible(true);
                  }}
                >
                  Edit Profile
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      </Spin>
      <OrderHistoryTable />
      <MyProfileEditModal
        setUpdateContact={setUpdateContact}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </Fragment>
  );
};
