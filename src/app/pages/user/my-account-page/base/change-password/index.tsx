import { Button, Form, Input, message, Pagination, Table } from 'antd';
import moment from 'moment';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { clearState, getMyProfileRequest, updatePasswordRequest } from 'app/pages/user/my-account-page/screen/action';

export const Changepassword: React.FC<any> = () => {
  const dispatch = useDispatch();

  const state = useSelector((state: RootState) => state?.myAcccountReducer);

  const [msgError, setMsgError]: any = useState('');

  const onFinish = (values: any) => {
    setMsgError('');
    if (values?.password !== values?.confirmPassword) {
      setMsgError('New password and retype new password do not match!');
      return;
    }
    const body = {
      oldPassword: values?.oldPassword,
      newPassword: values?.password,
    };
    dispatch(updatePasswordRequest(body));
  };

  useEffect(() => {
    if (state?.messageSucess === 'user-password-updated') {
      dispatch(clearState(''));
      message.success('Update password successfully!');
      dispatch(getMyProfileRequest(''));
    }
  }, [state?.messageSucess]);

  return (
    <Fragment>
      <p
        style={{
          fontSize: 16,
          fontWeight: 'bold',
          padding: '10px 20px',
        }}
      >
        Change pasword
      </p>
      <div
        style={{
          padding: 30,
        }}
      >
        <Form layout="vertical" onFinish={onFinish}>
          <Form.Item
            name="oldPassword"
            rules={[
              {
                required: true,
                message: 'Please enter old password!',
              },
            ]}
          >
            <Input.Password placeholder="Enter old Password" size="middle" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Please enter new password!',
              },
              {
                pattern:
                  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                message:
                  'Password must be at least eight characters, at least one letter, one number and one special character!',
              },
            ]}
          >
            <Input.Password placeholder="Enter Password" size="middle" />
          </Form.Item>

          <Form.Item
            name="confirmPassword"
            rules={[
              {
                required: true,
                message: 'Please enter retype new password!',
              },
              {
                pattern:
                  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                message:
                  'Password must be at least eight characters, at least one letter, one number and one special character!',
              },
            ]}
          >
            <Input.Password
              placeholder="Enter Confirm Password"
              size="middle"
            />
          </Form.Item>

          {msgError ? (
            <div className={'color-error'}>{msgError}</div>
          ) : state?.message ? (
            <div
              className={
                state?.status === 200 ? 'color-success' : 'color-error'
              }
            >
              {state?.message}
            </div>
          ) : (
            ''
          )}
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
            }}
          >
            <Button
              type="primary"
              loading={state?.btnUpdatePswUserLoading}
              htmlType="submit"
              style={{
                backgroundColor: '#FFD333',
                borderColor: '#FFD333',
                color: 'black',
                marginTop: 20,
                fontWeight: 'bold',
              }}
            >
              Change password
            </Button>
          </div>
        </Form>
      </div>
    </Fragment>
  );
};
