import { Button, Form, Input, Modal } from 'antd';
import {
  updateUsernameInfoRequest,
  updateUserContactInfoRequest,
} from 'app/pages/user/my-account-page/screen/action';
import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { validatePatternPhoneNumber } from 'utils/validate';

export const MyProfileEditModal: React.FC<any> = (props: any) => {
  const dispatch = useDispatch();
  const state: any = useSelector(
    (state: RootState) => state?.myAcccountReducer
  );
  const userInfoCookies = localStorage.getItem('user');

  let userInfo: any;

  if (userInfoCookies) {
    userInfo = JSON.parse(atob(userInfoCookies));
  }

  const stateDetail = useSelector(
    (state: RootState) => state?.adminDetailUserReducer
  );

  useEffect(() => {
    if (state?.dataResponseUser) {
      form.setFieldsValue({
        username: state?.dataResponseUser?.username,
        email: state?.dataResponseUser?.email,
        contact: state?.dataResponseUser?.contact?.substring(1),
      });
    }
  }, [state?.dataResponseUser]);

  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    const contactSubmit = '0' + form.getFieldValue('contact');
    dispatch(
      updateUsernameInfoRequest({
        username: values?.username,
      })
    );

    if (contactSubmit !== state?.dataResponseUser?.contact) {
      props?.setUpdateContact(true);
      dispatch(
        updateUserContactInfoRequest({
          contact: '0' + values?.contact,
        })
      );
    }
  };

  return (
    <Fragment>
      <Modal
        title="Edit Profile"
        visible={props?.isModalVisible}
        onCancel={() => {
          props.setIsModalVisible(false);
        }}
        footer={null}
      >
        <Form layout="vertical" form={form} onFinish={onFinish}>
          <Form.Item
            label="Name"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please enter name!',
              },
            ]}
          >
            <Input style={{ width: '100%' }} placeholder="Enter name" />
          </Form.Item>

          <Form.Item label="Email" name="email">
            <Input
              disabled
              style={{ width: '100%' }}
              placeholder="Enter email"
            />
          </Form.Item>
          <Form.Item
            label="Contact"
            name="contact"
            rules={[
              {
                pattern: validatePatternPhoneNumber,
                message: 'Please enter the correct phone number format!',
              },
            ]}
          >
            <Input
              maxLength={9}
              onChange={(e: any) => {
                let value = e?.target?.value;
                while (value.charAt(0) === '0') {
                  value = value.substring(1);
                }
                form.setFieldsValue({
                  contact: value,
                });
              }}
              prefix={'(+84)'}
              style={{ width: '100%' }}
              placeholder="Enter contact"
            />
          </Form.Item>

          {state?.message ? (
            <div className="color-error">{state?.message}</div>
          ) : (
            ''
          )}

          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button
              onClick={() => {
                props.setIsModalVisible(false);
              }}
            >
              Cancel
            </Button>
            <Button
              type="primary"
              htmlType="submit"
              style={{
                backgroundColor: '#FFD333',
                borderColor: '#FFD333',
                color: 'black',
                marginLeft: 10,
                fontWeight: 'bold',
              }}
              loading={state?.btnUpdateUserLoading}
            >
              Update
            </Button>
          </div>
        </Form>
      </Modal>
    </Fragment>
  );
};
