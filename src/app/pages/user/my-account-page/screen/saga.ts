import {
  getMyOrderRequest,
  getMyOrderSuccess,
  getMyOrderFailure,
  getMyProfileRequest,
  getMyProfileSuccess,
  getMyProfileFailure,
  updateUsernameInfoRequest,
  updateUsernameInfoSuccess,
  updateUsernameInfoFailure,
  updateUserContactInfoRequest,
  updateUserContactInfoSuccess,
  updateUserContactInfoFailure,
  updateAvatarRequest,
  updateAvatarSuccess,
  updateAvatarFailure,
  updatePasswordRequest,
  updatePasswordSuccess,
  updatePasswordFailure,
} from 'app/pages/user/my-account-page/screen/action';
import { myAccountEnum } from 'app/pages/user/my-account-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * get my cart
 *
 * @param {Object} action
 *
 */

export function* getMyOrderSaga({
  payload,
}: ReturnType<typeof getMyOrderRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_GET_MY_ORDER}`,
      apiMethod.GET,
      ''
    );
    yield put(getMyOrderSuccess(response?.data));
  } catch (error: any) {
    yield put(getMyOrderFailure(error?.response, error.message));
  }
}

/**
 * get my cart
 *
 * @param {Object} action
 *
 */

export function* getMyProfileSaga({
  payload,
}: ReturnType<typeof getMyProfileRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_GET_MY_PROFILE}`,
      apiMethod.GET,
      payload
    );
    yield put(getMyProfileSuccess(response?.data));
  } catch (error: any) {
    yield put(getMyProfileFailure(error?.response, error.message));
  }
}

/**
 * update user name
 *
 * @param {Object} action
 *
 */

export function* updateUsernameSaga({
  payload,
}: ReturnType<typeof updateUsernameInfoRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_CHANGE_USERNAME}`,
      apiMethod.PATCH,
      payload
    );
    yield put(updateUsernameInfoSuccess(response?.data));
  } catch (error: any) {
    yield put(updateUsernameInfoFailure(error?.response, error.message));
  }
}

/**
 * update user contact
 *
 * @param {Object} action
 *
 */

export function* updateUserContactSaga({
  payload,
}: ReturnType<typeof updateUserContactInfoRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_CHANGE_CONTACT}`,
      apiMethod.PATCH,
      payload
    );
    yield put(updateUserContactInfoSuccess(response?.data));
  } catch (error: any) {
    yield put(updateUserContactInfoFailure(error?.response, error.message));
  }
}

/**
 * update avatar
 *
 * @param {Object} action
 *
 */

export function* updateUserAvatarSaga({
  payload,
}: ReturnType<typeof updateAvatarRequest>) {
  try {
    console.log(payload);

    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_USER_AVATAR}`,
      apiMethod.PATCH,
      payload
    );
    yield put(updateAvatarSuccess(response?.data));
  } catch (error: any) {
    yield put(updateAvatarFailure(error?.response, error.message));
  }
}

/**
 * update password
 *
 * @param {Object} action
 *
 */

export function* updatePasswordSaga({
  payload,
}: ReturnType<typeof updatePasswordRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_USER_PASSWORD}`,
      apiMethod.PATCH,
      payload
    );
    yield put(updatePasswordSuccess(response?.data));
  } catch (error: any) {
    yield put(updatePasswordFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(myAccountEnum.GET_MY_ORDER_REQUEST, getMyOrderSaga),
    takeLatest(myAccountEnum.GET_MY_PROFILE_REQUEST, getMyProfileSaga),
    takeLatest(myAccountEnum.UPDATE_USERNAME_REQUEST, updateUsernameSaga),
    takeLatest(myAccountEnum.UPDATE_CONTACT_REQUEST, updateUserContactSaga),
    takeLatest(myAccountEnum.UPDATE_AVATAR_REQUEST, updateUserAvatarSaga),
    takeLatest(myAccountEnum.UPDATE_PASSWORD_REQUEST, updatePasswordSaga),
  ]);
}
