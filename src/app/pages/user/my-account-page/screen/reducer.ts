import { createReducer } from '@reduxjs/toolkit';
import { myAccountEnum } from 'app/pages/user/my-account-page/screen/types';
// The initial state of the App
export const initialState: any = {
  loading: false,
  loadingUser: false,
  message: '',
  dataResponse: [],
  dataResponseUser: [],
  messageSucess: '',
  updateUsername: '',
  updateContact: '',
  btnUpdateUserLoading: false,
  btnUpdatePswUserLoading: false,
  status: '',
};

export const myAcccountReducer = createReducer(initialState, (builder) => {
  builder
    // get my order
    .addCase(myAccountEnum.GET_MY_ORDER_REQUEST, (state, action) => {
      state.status = '';
      state.dataResponse = [];
      state.loading = true;
      return state;
    })
    .addCase(myAccountEnum.GET_MY_ORDER_SUCCESS, (state, action: any) => {
      state.loading = false;
      state.total = action?.payload?.data?.orders?.total;
      state.dataResponse = action?.payload?.data?.orders?.result;
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(myAccountEnum.GET_MY_ORDER_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.dataResponse = [];
      state.loading = false;
      return state;
    })

    // get my profile

    .addCase(myAccountEnum.GET_MY_PROFILE_REQUEST, (state, action) => {
      state.dataResponseUser = [];
      state.loadingUser = true;
      return state;
    })
    .addCase(myAccountEnum.GET_MY_PROFILE_SUCCESS, (state, action: any) => {
      state.loadingUser = false;
      state.dataResponseUser = action?.payload?.data;
      return state;
    })
    .addCase(myAccountEnum.GET_MY_PROFILE_FAILURE, (state, action: any) => {
      state.loadingUser = false;
      return state;
    })

    // update user info

    .addCase(myAccountEnum.UPDATE_USERNAME_REQUEST, (state, action) => {
      state.status = '';
      state.message = '';
      state.updateUsername = '';
      state.btnUpdateUserLoading = true;
      return state;
    })
    .addCase(myAccountEnum.UPDATE_USERNAME_SUCCESS, (state, action: any) => {
      state.message = '';
      state.updateUsername = 'updated';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(myAccountEnum.UPDATE_USERNAME_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.messageSucess = '';
      state.updateUsername = '';
      state.btnUpdateUserLoading = false;
      return state;
    })

    // update user contact

    .addCase(myAccountEnum.UPDATE_CONTACT_REQUEST, (state, action) => {
      state.status = '';
      state.message = '';
      state.updateContact = '';
      state.btnUpdateUserLoading = true;
      return state;
    })
    .addCase(myAccountEnum.UPDATE_CONTACT_SUCCESS, (state, action: any) => {
      state.message = '';
      state.updateContact = 'updated';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(myAccountEnum.UPDATE_CONTACT_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.messageSucess = '';
      state.updateContact = '';
      state.btnUpdateUserLoading = false;
      return state;
    })

    // update avatar user

    .addCase(myAccountEnum.UPDATE_AVATAR_REQUEST, (state, action) => {
      state.status = '';
      state.message = '';
      state.messageSucess = '';
      state.btnUpdateUserLoading = true;
      return state;
    })
    .addCase(myAccountEnum.UPDATE_AVATAR_SUCCESS, (state, action: any) => {
      state.message = '';
      state.messageSucess = 'user-avatar-updated';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(myAccountEnum.UPDATE_AVATAR_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.messageSucess = '';
      return state;
    })

    // update password user

    .addCase(myAccountEnum.UPDATE_PASSWORD_REQUEST, (state, action) => {
      state.status = '';
      state.message = '';
      state.messageSucess = '';
      state.btnUpdatePswUserLoading = true;
      return state;
    })
    .addCase(myAccountEnum.UPDATE_PASSWORD_SUCCESS, (state, action: any) => {
      state.message = '';
      state.btnUpdatePswUserLoading = false;
      state.messageSucess = 'user-password-updated';
      state.status = 200;
      return state;
    })
    .addCase(myAccountEnum.UPDATE_PASSWORD_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.btnUpdatePswUserLoading = false;
      state.messageSucess = '';
      return state;
    })

    // clear cart and item

    .addCase(myAccountEnum.CLEAR_MY_ACCOUNT, (state) => {
      state.dataResponse = [];
      state.dataResponseUser = [];
      state.loading = false;
      state.btnUpdateUserLoading = false;
      state.messageSucess = '';
      state.message = '';
      state.updateUsername = '';
      state.status = '';
      state.updateContact = '';
      return state;
    });
});
