import { Col, Row } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { CustomBreadcrumb } from 'app/components/breadcrumb';
import { Navbar } from 'app/components/navbar';
import { Changepassword } from 'app/pages/user/my-account-page/base/change-password';
import { MyProfileTab } from 'app/pages/user/my-account-page/base/my-profile';
import { OrderHistoryTable } from 'app/pages/user/my-account-page/base/order-history-table';
import React, { Fragment, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useHistory } from 'react-router-dom';
import './style.scss';

interface DataType {
  key: string;
  img: string;
  product: string;
  total: number;
}

const columns: ColumnsType<DataType> = [
  {
    title: 'Image',
    dataIndex: 'img',
    key: 'img',
    width: '10%',
    render: (img) => <img src={img} style={{ maxWidth: '100px' }} />,
  },
  {
    title: 'Product',
    dataIndex: 'product',
    key: 'product',
    width: '30%',
  },
  {
    title: 'Total',
    key: 'total',
    dataIndex: 'total',
    width: '10%',
    render: (num) => <p>${num.toFixed(2)}</p>,
  },
];

export const MyAccountPage: React.FC<any> = () => {
  const history = useHistory();
  const [tabActive, setActiveTab]: any = useState('');

  useEffect(() => {
    setActiveTab('profile');
  }, []);

  const switchTab = (key: any) => {
    switch (key) {
      case 'profile':
        return <MyProfileTab setActiveTab={setActiveTab} />;
      case 'history':
        return <OrderHistoryTable />;
      case 'password':
        return <Changepassword />;
      default:
        return '';
    }
  };

  const menu = [
    { title: 'My Profile', key: 'profile' },
    { title: 'Order History', key: 'history' },
    { title: 'Change Password', key: 'password' },
  ];

  return (
    <Fragment>
      <Helmet>
        <title>My Account Page</title>
        <meta name="description" content="My Account Page" />
      </Helmet>

      <Navbar />
      <CustomBreadcrumb currentPage="My Account" />
      <div className="account-container">
        <h2>My Account</h2>
        <div
          style={{
            maxWidth: '100%',
          }}
        >
          <Row>
            <Col xl={6} xs={24} className="myAccount__leftSize mt-10">
              <div className="myAccount__leftSize-wrapper">
                <div className="mt-10 bold">My Navigation</div>
                {menu?.map((item: any, key: any) => {
                  return (
                    <div
                      key={key}
                      className={`mt-10 cursor-pointer ${
                        tabActive === item?.key
                          ? 'myAccount__tabActive'
                          : 'myAccount__tabNotActive'
                      } `}
                      onClick={() => {
                        setActiveTab(item?.key);
                      }}
                    >
                      {item?.title}
                    </div>
                  );
                })}

                <div
                  className="mt-10 cursor-pointer"
                  onClick={() => {
                    localStorage.clear();
                    history.push('/');
                  }}
                >
                  Logout
                </div>
              </div>
            </Col>
            <Col xl={1} xs={24} />
            <Col xl={17} xs={24} className="myAccount__rightSize mt-10">
              {/* {tabActive === 'history' ? (
                <OrderHistoryTable />
              ) : (
                
              )} */}
              {switchTab(tabActive)}
            </Col>
          </Row>
        </div>
      </div>
      <footer className="footer"></footer>
    </Fragment>
  );
};
