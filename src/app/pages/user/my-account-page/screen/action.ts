import { createAction } from '@reduxjs/toolkit';
import { myAccountEnum } from 'app/pages/user/my-account-page/screen/types';
import { actionPayload } from 'helper/index';

// get my order

export const getMyOrderRequest = createAction<any>(
  myAccountEnum.GET_MY_ORDER_REQUEST
);
export const getMyOrderSuccess = createAction(
  myAccountEnum.GET_MY_ORDER_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getMyOrderFailure = createAction(
  myAccountEnum.GET_MY_ORDER_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// get my profile

export const getMyProfileRequest = createAction<any>(
  myAccountEnum.GET_MY_PROFILE_REQUEST
);
export const getMyProfileSuccess = createAction(
  myAccountEnum.GET_MY_PROFILE_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getMyProfileFailure = createAction(
  myAccountEnum.GET_MY_PROFILE_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);


// update user name info

export const updateUsernameInfoRequest = createAction<any>(
  myAccountEnum.UPDATE_USERNAME_REQUEST
);
export const updateUsernameInfoSuccess = createAction(
  myAccountEnum.UPDATE_USERNAME_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updateUsernameInfoFailure = createAction(
  myAccountEnum.UPDATE_USERNAME_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// update user contact info

export const updateUserContactInfoRequest = createAction<any>(
  myAccountEnum.UPDATE_CONTACT_REQUEST
);
export const updateUserContactInfoSuccess = createAction(
  myAccountEnum.UPDATE_CONTACT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updateUserContactInfoFailure = createAction(
  myAccountEnum.UPDATE_CONTACT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// update avatar

export const updateAvatarRequest = createAction<any>(
  myAccountEnum.UPDATE_AVATAR_REQUEST
);
export const updateAvatarSuccess = createAction(
  myAccountEnum.UPDATE_AVATAR_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updateAvatarFailure = createAction(
  myAccountEnum.UPDATE_AVATAR_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);


// update password

export const updatePasswordRequest = createAction<any>(
  myAccountEnum.UPDATE_PASSWORD_REQUEST
);
export const updatePasswordSuccess = createAction(
  myAccountEnum.UPDATE_PASSWORD_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updatePasswordFailure = createAction(
  myAccountEnum.UPDATE_PASSWORD_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);


// clear state user 
export const clearState = createAction<any>(myAccountEnum.CLEAR_MY_ACCOUNT);
