import React from 'react';
import { Rate, Divider } from 'antd';
// import {} from
import './style.scss';
import userAvatar from 'img/user.jpg';

const FormatDate = (date) => {
  let today:any = new Date(date);
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = today.getFullYear();
  return today = mm + '/' + dd + '/' + yyyy;
};

export const ReviewItem: React.FC<any> = (props) => {
  // console.log(FormatDate(props.date));

  return (
    <div className="review-item">
      <div className="review-container">
        <img className="avatar" src={props.useravatar} />
        <div className="review-content">
          <p className="user-name">{props.username}</p>
          <Rate value={props.rating} disabled />
          <p className="content">{props.content}</p>
          <p className="review-date">{FormatDate(props.date)}</p>
        </div>
      </div>
      <Divider className="divider" />
    </div>
  );
};
