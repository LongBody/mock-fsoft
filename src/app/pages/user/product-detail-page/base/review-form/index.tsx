import React from 'react';
import { Rate, Form, Button, Input } from 'antd';
// import {} from
import './style.scss';
import { RootState } from 'types';
import { createReviewRequest } from 'app/pages/user/product-detail-page/screen/action';
import { useDispatch } from 'react-redux';

const commonStyle = {
  color: '#212529',
  borderRadius: '5px',
};

const { TextArea } = Input;

export const ReviewSubmitForm: React.FC<any> = (props) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const validateMessages = {
    required: '',
  };

  const onFinish = (values: any) => {
    const body = {
      content: values.review.text,
      rating: values.review.rating,
      productId: parseInt(props.id),
    };
    dispatch(createReviewRequest(body));
    form.resetFields();
  };

  return (
    <div className="write-review">
      <p className="title">Write Reviews</p>
      <Form
        form={form}
        name="nest-messages"
        onFinish={onFinish}
        validateMessages={validateMessages}
      >
        <Form.Item name={['review', 'rating']} rules={[{ required: true }]}>
          <Rate />
        </Form.Item>
        <Form.Item name={['review', 'text']} rules={[{ required: true }]}>
          <TextArea
            rows={4}
            // placeholder="Write Your Review..."
            style={{ ...commonStyle }}
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            style={{
              ...commonStyle,
              backgroundColor: '#ffd333',
              width: '200px',
              borderColor: '#ffd333',
            }}
          >
            Post Your Review
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
