import { createAction } from '@reduxjs/toolkit';
import {
  productDetailEnum
} from 'app/pages/user/product-detail-page/screen/types';
import { actionPayload } from 'helper/index';

export const getProductDetailRequest = createAction<any>(
  productDetailEnum.GET_PRODUCT_DETAIL_REQUEST
);
export const getProductDetailSuccess = createAction(
  productDetailEnum.GET_PRODUCT_DETAIL_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getProductDetailFailure = createAction(
  productDetailEnum.GET_PRODUCT_DETAIL_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

export const createReviewRequest = createAction<any>(
  productDetailEnum.CREATE_PRODUCT_REVIEW_REQUEST
);
export const createReviewSuccess = createAction(
  productDetailEnum.CREATE_PRODUCT_REVIEW_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const createReviewFailure = createAction(
  productDetailEnum.CREATE_PRODUCT_REVIEW_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

export const createCartRequest = createAction<any>(
  productDetailEnum.CREATE_CART_REQUEST
);
export const createCartSuccess = createAction(
  productDetailEnum.CREATE_CART_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const createCartFailure = createAction(
  productDetailEnum.CREATE_CART_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

export const createCartItemRequest = createAction<any>(
  productDetailEnum.CREATE_CART_ITEM_REQUEST
);
export const createCartItemSuccess = createAction(
  productDetailEnum.CREATE_CART_ITEM_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const createCartItemFailure = createAction(
  productDetailEnum.CREATE_CART_ITEM_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);