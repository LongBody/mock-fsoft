import React, { Fragment, useState, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { Navbar } from 'app/components/navbar';
import { CustomBreadcrumb } from 'app/components/breadcrumb';
import {
  Rate,
  Divider,
  Radio,
  InputNumber,
  Space,
  Skeleton,
  Form,
  Button,
  Menu,
  Pagination,
  message,
} from 'antd';
import { ReviewItem } from 'app/pages/user/product-detail-page/base/review-item/index';
import type { RadioChangeEvent } from 'antd';
import './style.scss';
import ultraboost from 'img/ultraboost.jpg';
import shoesImg from 'img/shoes.jpg';
import { ProductItem } from 'app/components/product-items';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import {
  getProductDetailRequest,
  createCartRequest,
  createCartItemRequest,
} from 'app/pages/user/product-detail-page/screen/action';
import {
  getMyCartRequest,
  clearStateCart,
  getDetailCartRequest,
} from '../../shopping-cart/screen/action';
import { ReviewSubmitForm } from 'app/pages/user/product-detail-page/base/review-form/index';

const productData = [
  {
    id: 123,
    name: 'Adidas Shoes',
    img: shoesImg,
    price: 120,
    percentOff: 50,
    status: 'Available',
  },
  {
    id: 456,
    name: 'Adidas Shoes',
    img: shoesImg,
    price: 120,
    percentOff: 50,
    status: 'Available',
  },
  {
    id: 789,
    name: 'Adidas Shoes',
    img: shoesImg,
    price: 120,
    percentOff: 50,
    status: 'Available',
  },
  {
    id: 321,
    name: 'Adidas Shoes',
    img: shoesImg,
    price: 120,
    percentOff: 50,
    status: 'Available',
  },
];

export const ProductDetailPage: React.FC<any> = () => {
  const [value, setValue] = useState(1);
  const [data, setData] = useState({
    name: '',
    description: '',
    countInStock: '',
    brand: '',
    rating: '',
    price: 0,
    images: [{ url: shoesImg }],
  });
  const [reviewData, setReviewData] = useState({ total: 0, result: [] });
  const dispatch = useDispatch();
  const state = useSelector((state: RootState) => state?.productDetailReducer);
  const cartState = useSelector((state: RootState) => state?.userCartReducer);
  const cart = useSelector((state: RootState) => state?.createCartItemReducer);

  const onChange = (e: RadioChangeEvent) => {
    console.log('radio checked', e.target.value);
    setValue(e.target.value);
  };

  //Get value id in URLParams
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const productId = urlParams.get('id');

  //Get user info
  const userInfoLocal = localStorage.getItem('user');

  let userInfo: any;

  if (userInfoLocal) {
    userInfo = JSON.parse(atob(userInfoLocal));
  }

  //Call API
  useEffect(() => {
    dispatch(getProductDetailRequest({ id: productId }));
  }, []);

  //Set data state
  useEffect(() => {
    if (state?.status === 200) {
      setData(state?.dataResponse?.product);
      setReviewData(state?.dataResponse?.reviews);
    } else if (state?.messageSucess === 'created-review') {
      dispatch(getProductDetailRequest({ id: productId }));
      message.success('Created review successfully!');
    }
    // console.log(state)
  }, [state]);

  useEffect(() => {
    if (state?.messageSucess === 'created-review') {
      dispatch(getProductDetailRequest({ id: productId }));
      message.success('Created review successfully!');
    }
  }, [state?.messageSucess]);

  const onFinish = (values: any) => {
    const total = data.price * values.value;
    const body = {
      cart: { totalPrice: total, userId: userInfo.id },
      itemArr: [
        {
          productId: productId,
          quantity: values.value,
          price: data.price,
          total: total,
        },
      ],
    };
    const createItemBody = {
      cartId: cartState?.cart?.id,
      productId: productId,
      quantity: values.value,
      price: data.price,
      total,
    };
    if (cartState?.idCart === undefined) {
      dispatch(createCartRequest(body));
    } else {
      dispatch(createCartItemRequest(createItemBody));
    }
  };

  useEffect(() => {
    if (cart?.messageSucess === 'created-item') {
      dispatch(clearStateCart(''));
      dispatch(
        getDetailCartRequest({
          id: cartState?.idCart,
        })
      );
      message.success('Add to cart successfully!');
    } else if (cart?.messageSucess === 'created') {
      dispatch(clearStateCart(''));
      dispatch(getMyCartRequest(''));
      message.success('Create cart successfully!');
    }
  }, [cart]);

  const [selectedMenuItem, setSelectedMenuItem] = useState(['reviews']);

  const componentSwitch = (key: any) => {
    switch (key[0]) {
      case 'reviews':
        return (
          <div className="menu-component-reviews">
            <p className="title"> Customer Reviews</p>
            {state?.loading ? (
              <Skeleton avatar active paragraph={{ rows: 3 }} />
            ) : reviewData.total > 0 ? (
              reviewData.result.map((item: any) => {
                return (
                  <ReviewItem
                    id={item.id}
                    content={item.content}
                    rating={item.rating}
                    useravatar={item.userReview.avatar}
                    username={item.userReview.username}
                    date={item.createdAt}
                  />
                );
              })
            ) : (
              <div>Not have reviews yet</div>
            )}

            <div className="pagination">
              <Pagination defaultCurrent={1} total={reviewData.total} />
            </div>
          </div>
        );
      case 'description':
        return (<div className="menu-component-reviews">
          <p className="title"> Description</p>
          <p>{data.description}</p>
        </div>);
      default:
        break;
    }
  };

  useEffect(() => {
    if (cart?.messageSucess === 'created-item') {
      dispatch(clearStateCart(''));
      dispatch(
        getDetailCartRequest({
          id: cartState?.idCart,
        })
      );
      message.success('Create cart successfully!');
    } else if (cart?.messageSucess === 'created') {
      dispatch(clearStateCart(''));
      dispatch(getMyCartRequest(''));
      message.success('Add to cart successfully!');
    }
  }, [cart]);

  return (
    <Fragment>
      <Helmet>
        <title>Product Detail Page</title>
        <meta name="description" content="Product Detail Page" />
      </Helmet>

      <Navbar />
      <CustomBreadcrumb
        currentPage="Adidas Shoes"
        previousPage="Shoes"
        previousLocation="#"
      />
      <div className="container">
        <div className="detail-header">
          <div className="img-container">
            <div className="main-img">
              <img src={data.images[0].url} className="lg-img" />
            </div>
            <div className="sm-img-container">
              <img className="sm-img" alt="ultraboost" src={ultraboost} />
              <img className="sm-img" alt="ultraboost" src={ultraboost} />
              <img className="sm-img" alt="ultraboost" src={ultraboost} />
              <img className="sm-img" alt="ultraboost" src={ultraboost} />
              <img className="sm-img" alt="ultraboost" src={ultraboost} />
            </div>
          </div>
          {state?.loading === true ? (
            <div className="product-info">
              <Skeleton active paragraph={{ rows: 7 }} />
            </div>
          ) : (
            <div className="product-info">
              <p className="title">{data.name}</p>
              <div className="rating">
                <Rate allowHalf value={parseFloat(data.rating)} disabled />
                <Divider type="vertical" />
                <p className="sold-info">{reviewData.total} Reviews</p>
                <Divider type="vertical" />
                <p className="sold-info">3k Sold</p>
              </div>
              <p className="description">{data.description}</p>
              <Divider className="main-divider" />
              <div className="available-container">
                <div className="available">
                  <p>Availability:</p>
                  <p className="status">
                    {parseInt(data.countInStock) > 0
                      ? 'In Stock'
                      : 'Not Available'}{' '}
                  </p>
                </div>
                <div>Brand: {data.brand}</div>
                <div>SKU: 83690/32</div>
              </div>
              <div className="price">
                <p>${data.price.toFixed(2)}</p>
                <p className="saleoff">50% Off</p>
              </div>
              <div className="color-selection">
                <p className="title">Select Color:</p>
                <Radio.Group onChange={onChange} value={value} size="large">
                  <Space direction="horizontal">
                    <Radio value={1} />
                    <Radio value={2} />
                    <Radio value={3} />
                    <Radio value={4} />
                  </Space>
                </Radio.Group>
              </div>
              <div className="quantity">
                <p className="title">Quantity:</p>
                <div className="btn-container">
                  <Form
                    name="add-to-cart"
                    onFinish={onFinish}
                    initialValues={{ value: 1 }}
                  >
                    <Form.Item wrapperCol={{ sm: 24 }} name="value">
                      <InputNumber
                        style={{ borderRadius: '5px', width: '150px' }}
                        min={1}
                        max={10}
                        defaultValue={1}
                      />
                    </Form.Item>
                    <Form.Item wrapperCol={{ sm: 24 }}>
                      <Button
                        htmlType="submit"
                        type="primary"
                        style={{
                          color: '#212529',
                          borderRadius: '5px',
                          backgroundColor: '#ffd333',
                          width: '150px',
                          borderColor: '#ffd333',
                        }}
                      >
                        <i className="fa-solid fa-cart-plus"></i> Add to cart
                      </Button>
                    </Form.Item>
                  </Form>
                  {/* <div className="addtocart-btn">
                    <i className="fa-solid fa-cart-plus"></i>
                    <p>Add to cart</p>
                  </div> */}
                </div>
              </div>
              <div className="rating-container">
                <p className="title">Rate:</p>
                <Rate allowHalf value={parseFloat(data.rating)} disabled />
              </div>
            </div>
          )}
        </div>
        {/* <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" itemProp={items} />; */}
        <div className="reviews-container">
          <Menu
            mode="horizontal"
            defaultSelectedKeys={['reviews']}
            style={{
              display: 'flex',
              justifyContent: 'center',
              fontWeight: 'bold',
            }}
            selectedKeys={selectedMenuItem}
            onClick={(e) => setSelectedMenuItem([e.key])}
          >
            <Menu.Item key="description">Description</Menu.Item>
            <Menu.Item>Specification</Menu.Item>
            <Menu.Item key="reviews">Reviews</Menu.Item>
          </Menu>
          {componentSwitch(selectedMenuItem)}
        </div>
        <ReviewSubmitForm id={productId} />
        <div className="related-products">
          <Divider orientation="left" orientationMargin={0}>
            <p className="divider-title">Related Product</p>
          </Divider>
          <div className="product-container">
            {productData.map((item) => {
              return (
                <ProductItem
                  key={item.id}
                  id={item.id}
                  name={item.name}
                  img={item.img}
                  price={item.price}
                  percentOff={item.percentOff}
                  status={item.status}
                />
              );
            })}
          </div>
        </div>
        {/* <Skeleton/> */}
      </div>
      <footer className="footer"></footer>
    </Fragment>
  );
};
