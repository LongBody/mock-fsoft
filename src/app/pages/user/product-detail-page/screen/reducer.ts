import { createReducer } from '@reduxjs/toolkit';
import { productDetailEnum } from 'app/pages/user/product-detail-page/screen/types';

export const initialState: any = {
  loading: false,
  loadingBtnCreate: false,
  message: '',
  status: '',
  dataResponse: {},
  messageSucess: '',
};

export const productDetailReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(productDetailEnum.GET_PRODUCT_DETAIL_REQUEST, (state, action) => {
      state.dataResponse = {};
      state.message = '';
      state.status = '';
      state.loading = true;
      return state;
    })
    .addCase(
      productDetailEnum.GET_PRODUCT_DETAIL_SUCCESS,
      (state, action: any) => {
        state.dataResponse = action?.payload?.data;
        state.message = action?.payload?.message;
        state.status = action?.payload?.status;
        state.loading = false;
        return state;
      }
    )
    .addCase(
      productDetailEnum.GET_PRODUCT_DETAIL_FAILURE,
      (state, action: any) => {
        state.status = action?.payload?.status;
        state.loading = false;
        return state;
      }
    )
    .addCase(
      productDetailEnum.CREATE_PRODUCT_REVIEW_REQUEST,
      (state, action) => {
        state.message = '';
        state.status = '';
        state.messageSucess = '';
        state.loadingBtnCreate = true;
        return state;
      }
    )
    .addCase(
      productDetailEnum.CREATE_PRODUCT_REVIEW_SUCCESS,
      (state, action: any) => {
        state.loadingBtnCreate = false;
        state.message = '';
        state.messageSucess = 'created-review';
        state.status = action?.payload?.status;
        return state;
      }
    )
    .addCase(
      productDetailEnum.CREATE_PRODUCT_REVIEW_FAILURE,
      (state, action: any) => {
        state.loadingBtnCreate = false;
        state.message = action?.payload?.message;
        state.messageSucess = '';
        state.status = action?.payload?.status;
        return state;
      }
    );
});

export const createCartItemReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(productDetailEnum.CREATE_CART_ITEM_REQUEST, (state, action) => {
      state.message = '';
      state.status = '';
      state.messageSucess = '';
      state.loadingBtnCreate = true;
      return state;
    })
    .addCase(
      productDetailEnum.CREATE_CART_ITEM_SUCCESS,
      (state, action: any) => {
        state.loadingBtnCreate = false;
        state.message = '';
        state.messageSucess = 'created-item';
        state.status = action?.payload?.status;
        return state;
      }
    )
    .addCase(
      productDetailEnum.CREATE_CART_ITEM_FAILURE,
      (state, action: any) => {
        state.loadingBtnCreate = false;
        state.message = action?.payload?.message;
        state.messageSucess = '';
        state.status = action?.payload?.status;
        return state;
      }
    )
    .addCase(productDetailEnum.CREATE_CART_REQUEST, (state, action) => {
      state.message = '';
      state.status = '';
      state.messageSucess = '';
      state.loadingBtnCreate = true;
      return state;
    })
    .addCase(productDetailEnum.CREATE_CART_SUCCESS, (state, action: any) => {
      state.loadingBtnCreate = false;
      state.message = '';
      state.messageSucess = 'created';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(productDetailEnum.CREATE_CART_FAILURE, (state, action: any) => {
      state.loadingBtnCreate = false;
      state.message = action?.payload?.message;
      state.messageSucess = '';
      state.status = action?.payload?.status;
      return state;
    });
});
