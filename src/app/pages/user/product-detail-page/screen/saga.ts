import {
  getProductDetailRequest,
  getProductDetailSuccess,
  getProductDetailFailure,
  createReviewRequest,
  createReviewSuccess,
  createReviewFailure,
  createCartRequest,
  createCartSuccess,
  createCartFailure,
  createCartItemRequest,
  createCartItemSuccess,
  createCartItemFailure
} from 'app/pages/user/product-detail-page/screen/action';
import { productDetailEnum } from 'app/pages/user/product-detail-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';


export function* getProductDetailSaga({
  payload,
}: ReturnType<typeof getProductDetailRequest>) {
  try {
    let url = '';
    if (payload?.status) {
      url = `${API_URL}${API_CALL?.API_PRODUCTS}/${payload?.id}`;
    } else {
      url = `${API_URL}${API_CALL?.API_PRODUCTS}/${payload?.id}`;
    }
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(getProductDetailSuccess(response?.data));
  } catch (error: any) {
    yield put(getProductDetailFailure(error?.response, error.message));
  }
}

export function* createReviewSaga({
  payload,
}: ReturnType<typeof createReviewRequest>) {
  try {
    const url = `${API_URL}${API_CALL?.API_PRODUCTS}/${payload?.productId}/reviews`;
    const response = yield call(requestAPIWithToken, url, apiMethod.POST, payload);
    yield put(createReviewSuccess(response?.data));
  } catch (error: any) {
    yield put(createReviewFailure(error?.response, error.message));
  }
}

export function* createCartSaga({
  payload,
}: ReturnType<typeof createCartRequest>) {
  try {
    const url = `${API_URL}${API_CALL?.API_DETAIL_CART}`;
    const response = yield call(requestAPIWithToken, url, apiMethod.POST, payload);
    yield put(createCartSuccess(response?.data));
  } catch (error: any) {
    yield put(createCartFailure(error?.response, error.message));
  }
}

export function* createCartItemSaga({
  payload,
}: ReturnType<typeof createCartItemRequest>) {
  try {
    const url = `${API_URL}${API_CALL?.API_CREATE_ITEM_CART}`;
    const response = yield call(requestAPIWithToken, url, apiMethod.POST, payload);
    yield put(createCartItemSuccess(response?.data));
  } catch (error: any) {
    yield put(createCartItemFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(productDetailEnum.GET_PRODUCT_DETAIL_REQUEST, getProductDetailSaga),
    takeLatest(productDetailEnum.CREATE_PRODUCT_REVIEW_REQUEST, createReviewSaga),
    takeLatest(productDetailEnum.CREATE_CART_REQUEST, createCartSaga),
    takeLatest(productDetailEnum.CREATE_CART_ITEM_REQUEST, createCartItemSaga)
  ]);
}