import { Col, RadioChangeEvent, Row } from 'antd';
import { Button, Divider, Form, Input, Radio, Table, message } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { CustomBreadcrumb } from 'app/components/breadcrumb';
import { Navbar } from 'app/components/navbar';
import {
  clearStateCart,
  clearStateCartItem,
  createOrderRequest,
  deleteCartRequest,
  getMyCartRequest,
} from 'app/pages/user/shopping-cart/screen/action';
import shoesImg from 'img/shoes.jpg';
import React, { Fragment, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

const price = 120;
const shipping = 20;
const total = price + shipping;

interface DataType {
  key: string;
  img: string;
  product: string;
  total: number;
}

const columns: ColumnsType<DataType> = [
  {
    title: 'Image',
    dataIndex: 'img',
    key: 'img',
    width: '10%',
    render: (img, record: any) => (
      <img
        src={record?.itemCartInfo?.images[0]?.url}
        style={{ height: 60, width: 60 }}
      />
    ),
  },
  {
    title: 'Product',
    dataIndex: 'product',
    key: 'product',
    width: '30%',
    render: (product, record: any) => (
      <div>
        <div
          style={{
            fontSize: 15,
            fontWeight: 'bold',
          }}
        >
          {record?.itemCartInfo?.name}
        </div>
        <div
          style={{
            fontSize: 14,
            color: '#5A5A5A',
          }}
        >
          Qty: {record?.quantity}
        </div>
      </div>
    ),
  },
  {
    title: 'Total',
    width: '10%',
    key: 'total',
    align: 'center',
    dataIndex: 'total',
    render: (num) => <p className="bold">${num.toFixed(1)}</p>,
  },
];

const data: DataType[] = [
  {
    key: '1',
    img: shoesImg,
    product: 'Adidas Shoes',
    total: 120,
  },
];

const buttonStyle = {
  fontWeight: 'bold',
  color: '#212529',
  backgroundColor: '#ffd333',
  borderColor: '#ffd333',
  borderRadius: '5px',
  padding: '0px 20px',
};

const radioStyle = {
  borderRadius: '5px',
  width: '100%',
};

export const CheckoutPage: React.FC<any> = () => {
  const state = useSelector((state: RootState) => state?.userCartReducer);

  const dispatch = useDispatch();

  const history = useHistory();

  const dataSource = state?.item?.length > 0 ? state?.item : [];
  const [value, setValue] = useState(1);

  const userInfoCookies = localStorage.getItem('user');
  // const userInfoCookies = getCookie('user-info');
  let userInfo: any;

  if (userInfoCookies) {
    userInfo = JSON.parse(atob(userInfoCookies));
  }

  const onChange = (e: RadioChangeEvent) => {
    setValue(e.target.value);
  };

  const onFinish = (values: any) => {
    const newArr: any = [];

    state?.item?.map((item: any) => {
      newArr.push({
        productId: item?.itemCartInfo?.id,
        quantity: item?.quantity,
        price: item?.price,
        total: item?.total,
      });
    });

    const body = {
      order: {
        paymentMethod: values?.paymentMethod,
        address: values?.address,
        contact: userInfo?.contact,
        totalPrice: state?.cart?.totalPrice,
        userId: userInfo?.id,
      },
      itemArr: newArr,
    };

    dispatch(createOrderRequest(body));
  };

  useEffect(() => {
    if (state?.message === 'order-created') {
      dispatch(
        deleteCartRequest({
          id: state?.cart?.id,
        })
      );
    }
  }, [state?.message]);

  useEffect(() => {
    if (state?.message === 'deleted-cart') {
      dispatch(clearStateCart(''));
      dispatch(clearStateCartItem(''));
      message.success('Create order successfully!');
      history.push('/');
    }
  }, [state?.message]);

  return (
    <Fragment>
      <Helmet>
        <title>Checkout Page</title>
        <meta name="description" content="Checkout Page" />
      </Helmet>

      <Navbar />
      <CustomBreadcrumb
        currentPage="Checkout"
        previousPage="Shopping Cart"
        previousLocation="/shoppingcart"
      />
      <div className="checkout-wrapper">
        <h2>Checkout</h2>
        <div className="checkout-container">
          <Row
            style={{
              width: '100%',
            }}
          >
            <Col
              xl={14}
              xs={24}
              style={{
                paddingBottom: 10,
              }}
            >
              <div className="table-container">
                <Table
                  columns={columns}
                  dataSource={dataSource}
                  pagination={false}
                  scroll={{ x: 500 }}
                />
              </div>
            </Col>
            <Col
              xl={10}
              xs={24}
              style={{
                paddingBottom: 10,
              }}
            >
              <div className="beside-container">
                <Form
                  layout="vertical"
                  style={{ width: '100%' }}
                  onFinish={onFinish}
                >
                  <div className="shipping-info-container">
                    <p className="title">Shipping Information</p>
                    <Form.Item
                      label="Your address"
                      name="address"
                      rules={[
                        {
                          required: true,
                          message: 'Please enter your address!',
                        },
                      ]}
                    >
                      <Input
                        style={{ width: '100%' }}
                        placeholder="Enter your address"
                      />
                    </Form.Item>
                    <div className="user-info">
                      <p className="title">Phone Number</p>
                      <p className="info">{userInfo?.contact}</p>
                    </div>
                    <div className="user-info">
                      <p className="title">Email Address</p>
                      <p className="info">{userInfo?.email}</p>
                    </div>
                    <a className="edit-link">Edit Address</a>
                  </div>
                  <div className="checkout-info-sm-container">
                    <p className="title">Checkout</p>
                    <div className="checkout-info">
                      <p className="title">Subtotal</p>
                      <p className="info">
                        {' '}
                        ${state?.cart?.totalPrice?.toFixed(1)}
                      </p>
                    </div>
                    <div className="checkout-info">
                      <p className="title">Shipping</p>
                      <p className="info">$0</p>
                    </div>
                    <Divider />
                    <div className="checkout-info">
                      <p className="title">Total</p>
                      <p className="info">
                        ${state?.cart?.totalPrice?.toFixed(1)}
                      </p>
                    </div>
                    <div className="payment-selection">
                      <Form.Item
                        label="Payment Method"
                        name="paymentMethod"
                        rules={[
                          {
                            required: true,
                            message: 'Please enter your payment method!',
                          },
                        ]}
                        style={{
                          width: '100%',
                          display: 'flex',
                          justifyContent: 'center',
                        }}
                      >
                        <Radio.Group
                          onChange={onChange}
                          value={value}
                          size="large"
                          buttonStyle="solid"
                          style={{
                            width: '100%',
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                          }}
                        >
                          <Radio.Button
                            value="cash"
                            className="mt-10"
                            style={radioStyle}
                          >
                            Cash on delivery
                          </Radio.Button>
                          <Radio.Button
                            value="checkpayment"
                            className="mt-10"
                            style={radioStyle}
                          >
                            Check payments
                          </Radio.Button>
                          <Radio.Button
                            value="paypal"
                            className="mt-10"
                            style={radioStyle}
                          >
                            Paypal
                          </Radio.Button>
                          <Radio.Button
                            value="mastercard"
                            className="mt-10"
                            style={radioStyle}
                          >
                            Master Card
                          </Radio.Button>
                        </Radio.Group>
                      </Form.Item>
                    </div>
                    <div className="checkout-btn">
                      <Button
                        type="primary"
                        htmlType="submit"
                        loading={state?.loadingBtnCreateOrder}
                        style={{
                          ...buttonStyle,
                          width: '60%',
                          fontSize: '20px',
                          padding: '16px 0px',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        Checkout
                      </Button>
                    </div>
                  </div>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <footer className="footer"></footer>
    </Fragment>
  );
};
