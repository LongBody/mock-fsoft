import { createReducer } from '@reduxjs/toolkit';
import { signInEnum } from 'app/pages/user/authentication/screen/types';
// import { setCookie } from 'utils/request';
// The initial state of the App
export const initialState: any = {
  loading: false,
  message: '',
  messageRegister: '',
  status: '',
  dataResponse: {},
};

export const signInReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(signInEnum.SIGN_IN_REQUEST, (state, action) => {
      state.dataResponse = {};
      state.status = '';
      state.message = '';
      state.loading = true;
      return state;
    })
    .addCase(signInEnum.SIGN_IN_SUCCESS, (state, action: any) => {
      state.loading = true;
      state.message = '';
      state.status = action?.payload?.status;
      state.dataResponse = action?.payload?.data?.user;

      localStorage.setItem(
        'user',
        btoa(JSON.stringify(action?.payload?.data?.user))
      );
      localStorage.setItem(
        'accessToken',
        btoa(action?.payload?.data?.tokens?.access?.token)
      );
      localStorage.setItem(
        'expires',
        btoa(action?.payload?.data?.tokens?.access?.expires)
      );
      localStorage.setItem(
        'refreshToken',
        btoa(action?.payload?.data?.tokens?.refresh?.token)
      );
      return state;
    })
    .addCase(signInEnum.SIGN_IN_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      if (action?.payload?.data?.status === 404) {
        state.message = action?.payload?.data?.message;
      }
      if (action?.payload?.data?.status === 401) {
        state.message = action?.payload?.data?.message;
      }
      state.loading = false;
      return state;
    })

    .addCase(signInEnum.SIGN_UP_REQUEST, (state, action) => {
      state.status = '';
      state.messageRegister = '';
      state.loading = true;
      return state;
    })
    .addCase(signInEnum.SIGN_UP_SUCCESS, (state, action: any) => {
      state.loading = false;
      state.messageRegister = 'registed';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(signInEnum.SIGN_UP_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.messageRegister = action?.payload?.data?.message;
      state.loading = false;
      return state;
    })

    // clear state

    .addCase(signInEnum.CLEAR_SIGN_IN_STATE, (state) => {
      state.loading = false;
      state.dataResponse = {};
      state.message = '';
      state.status = '';
      state.messageRegister = '';
      return state;
    });
});
