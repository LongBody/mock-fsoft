import { createAction } from '@reduxjs/toolkit';
import { signInEnum } from 'app/pages/user/authentication/screen/types';
import { actionPayload } from 'helper/index';

// sign in
export const signInRequest = createAction<any>(signInEnum.SIGN_IN_REQUEST);
export const signInSuccess = createAction(
  signInEnum.SIGN_IN_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const signInFailure = createAction(
  signInEnum.SIGN_IN_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// sign up
export const signUpRequest = createAction<any>(signInEnum.SIGN_UP_REQUEST);
export const signUpSuccess = createAction(
  signInEnum.SIGN_UP_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const signUpFailure = createAction(
  signInEnum.SIGN_UP_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// clear state

export const clearStateAuthen = createAction<any>(
  signInEnum.CLEAR_SIGN_IN_STATE
);
