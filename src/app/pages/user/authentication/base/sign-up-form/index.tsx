import { Button, Form, Input, Modal } from 'antd';
import {
  clearStateAuthen,
  signUpRequest,
} from 'app/pages/user/authentication/screen/action';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types/RootState';
import './style.scss';

export const SignUpForm: React.FC<any> = (props: any) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const [messageError, setMessageError] = useState('');

  const state = useSelector((state: RootState) => state?.signInReducer);
  useEffect(() => {
    if (state?.status !== '' && state?.messageRegister !== '') {
      if (state?.messageRegister === 'registed') {
        setMessageError('Register Successfully');
      } else setMessageError(state?.messageRegister);
    }
  }, [state?.status]);

  const onFinish = (values: any) => {
    if (values?.password !== values?.confirmPassword) {
      setMessageError('Password and confirm password do not match!');
      return;
    } else {
      const body = {
        email: values?.email,
        password: values?.password,
        username: values?.username,
      };
      dispatch(signUpRequest(body));
    }
  };

  const clearDataAndMessage = () => {
    setMessageError('');
    form?.resetFields();
    dispatch(clearStateAuthen(''));
  };

  useEffect(() => {
    clearDataAndMessage();
  }, []);

  return (
    <Fragment>
      <Modal
        visible={props?.isModalSignUpVisible}
        onCancel={() => {
          clearDataAndMessage();
          props?.setModalSignUpVisible(false);
        }}
        className="modal__style"
        width={'900px'}
        footer={null}
      >
        <div className="signUpForm__container">
          <div className="signUpForm__image">
            <div className="signUpForm__image__title">
              <p className="signUpForm__title">Shop App</p>
              <i className="fa-brands fa-shopify signUpForm__image-shop"></i>
            </div>
          </div>
          <div className="signUpForm__rightSide">
            <i
              className="fa-solid fa-x signUpForm__rightSide-x-icon"
              onClick={() => {
                props?.setModalSignUpVisible(false);
              }}
            ></i>
            <div className="signInForm__content">
              <div>
                <p className="signUpForm__label">SIGN UP</p>

                <Form
                  name="basic"
                  layout="vertical"
                  onFinish={onFinish}
                  form={form}
                  autoComplete="off"
                  style={{
                    width: 300,
                  }}
                >
                  <Form.Item
                    name="username"
                    rules={[
                      {
                        required: true,
                        message: 'Please enter username  !',
                      },
                    ]}
                  >
                    <Input
                      size="middle"
                      style={{ width: '100%' }}
                      placeholder="Enter username"
                    />
                  </Form.Item>
                  <Form.Item
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: 'Please enter email Address!',
                      },
                      {
                        pattern:
                          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'Please enter the correct email format!',
                      },
                    ]}
                  >
                    <Input
                      size="middle"
                      style={{ width: '100%' }}
                      placeholder="Enter Email Address"
                    />
                  </Form.Item>

                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: 'Please enter password!',
                      },
                      {
                        pattern:
                          /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                        message:
                          'Password must be at least eight characters, at least one letter, one number and one special character!',
                      },
                    ]}
                  >
                    <Input.Password
                      placeholder="Enter Password"
                      size="middle"
                    />
                  </Form.Item>

                  <Form.Item
                    name="confirmPassword"
                    rules={[
                      {
                        required: true,
                        message: 'Please enter retype password!',
                      },
                      {
                        pattern:
                          /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                        message:
                          'Password must be at least eight characters, at least one letter, one number and one special character!',
                      },
                    ]}
                  >
                    <Input.Password
                      placeholder="Enter Confirm Password"
                      size="middle"
                    />
                  </Form.Item>

                  {messageError ? (
                    <div
                      className={
                        state?.status === 201 ? 'color-success' : 'color-error'
                      }
                    >
                      {messageError}
                    </div>
                  ) : (
                    ''
                  )}

                  <Button
                    loading={state?.loading}
                    htmlType="submit"
                    size="middle"
                    className="signInForm__btn"
                  >
                    Register
                  </Button>
                </Form>
                <div className="signInForm__indicateSignUp">
                  <span
                    onClick={() => {
                      clearDataAndMessage();
                      props?.setModalSignUpVisible(false);
                      props?.setModalSignInVisible(true);
                    }}
                  >
                    Login
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </Fragment>
  );
};
