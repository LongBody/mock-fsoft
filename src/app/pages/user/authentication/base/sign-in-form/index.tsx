import { LockOutlined } from '@ant-design/icons';
import { Button, Form, Input, Modal } from 'antd';
import {
  signInRequest,
  clearStateAuthen,
} from 'app/pages/user/authentication/screen/action';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import { Link, useHistory } from 'react-router-dom';
import './style.scss';

export const SignInForm: React.FC<any> = (props: any) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const history = useHistory();

  const [messageError, setMessageError] = useState('');

  const state = useSelector((state: RootState) => state?.signInReducer);

  useEffect(() => {
    // if status === 401 or 404 , has error and show message
    if (state?.status !== '' && state?.message !== '') {
      setMessageError(state?.message);
    }
    if (
      state?.status === 200 &&
      state?.message === '' &&
      state.dataResponse?.role === 'admin'
    ) {
      history.push('/admin/dashboard');
    }
    if (
      state?.status === 200 &&
      state?.message === '' &&
      state.dataResponse?.role === 'user'
    ) {
      clearDataAndMessage();
      props?.setModalSignInVisible(false);
      history.push('/');
    }
  }, [state?.status]);

  const clearDataAndMessage = () => {
    setMessageError('');
    form?.resetFields();
    dispatch(clearStateAuthen(''));
  };

  useEffect(() => {
    clearDataAndMessage();
  }, []);

  const onFinish = (values: any) => {
    const body = {
      email: values?.email,
      password: values?.password,
      deviceId: `deviceId-${values?.email}`,
    };
    dispatch(signInRequest(body));
  };

  return (
    <Fragment>
      <Modal
        visible={props?.isModalSignInVisible}
        onCancel={() => {
          clearDataAndMessage();
          props?.setModalSignInVisible(false);
        }}
        className="modal__style"
        width={'800px'}
        footer={null}
      >
        <div className="signInForm__container">
          <div className="signInForm__image">
            <div className="signInForm__image__title">
              <p className="signInForm__title">Shop App</p>
              <i className="fa-brands fa-shopify signInForm__image-shop"></i>
            </div>
          </div>
          <div className="signInForm__rightSide">
            <i
              className="fa-solid fa-x signInForm__rightSide-x-icon"
              onClick={() => {
                props?.setModalSignInVisible(false);
              }}
            ></i>
            <div className="signInForm__content">
              <p className="signInForm__label">SIGN IN</p>

              <Form
                name="basic"
                layout="vertical"
                onFinish={onFinish}
                form={form}
                autoComplete="off"
                style={{
                  width: 300,
                }}
              >
                <Form.Item
                  name="email"
                  label="Email Address"
                  rules={[
                    {
                      required: true,
                      message: 'Enter Email Address please !',
                    },
                  ]}
                >
                  <Input
                    size="large"
                    style={{ width: '100%' }}
                    placeholder="Email Address"
                  />
                </Form.Item>

                <Form.Item
                  name="password"
                  label="Password"
                  rules={[
                    {
                      required: true,
                      message: 'Enter Password please !',
                    },
                  ]}
                >
                  <Input.Password placeholder="Enter Password" size="large" />
                </Form.Item>

                {messageError ? (
                  <div className="color-error">{messageError}</div>
                ) : (
                  ''
                )}

                <Button
                  loading={state?.loading}
                  htmlType="submit"
                  size="large"
                  className="signInForm__btn"
                >
                  LOGIN
                </Button>
              </Form>
              <div className="signInForm__indicateSignUp">
                <span
                  onClick={() => {
                    clearDataAndMessage();
                    props?.setModalSignInVisible(false);
                    props?.setModalSignUpVisible(true);
                  }}
                >
                  New customer? Register
                </span>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </Fragment>
  );
};
