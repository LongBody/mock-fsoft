import React, { useState, useEffect } from 'react';
import { Dropdown, Menu, MenuProps, message, Space, Button } from 'antd';
// type Props = {}
import { getListProductRequest } from 'app/pages/user/home-page/screen/action';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import { ProductItem } from 'app/components/product-items';
import shoesImg from 'img/shoes.jpg';
import './style.scss';

const handleMenuClick: MenuProps['onClick'] = (e) => {
  message.info('Click on menu item.');
  console.log('click', e);
};

export const BestsellerList: React.FC<any> = () => {
  const dispatch = useDispatch();
  const state = useSelector((state: RootState) => state?.productListReducer);
  const [data, setData] = useState([]);

  useEffect(() => {
    dispatch(getListProductRequest({ size: 4 }));
  }, []);

  useEffect(() => {
    if (state?.status === 200) {
      setData(state?.dataResponse?.result);
    }
  }, [state]);

  return (
    <div className="bestseller-container">
      <div className="bestseller-header">
        <p className="title">Bestsellers</p>
        <div className="showmore-btn">Show more...</div>
      </div>
      <div className="product-container">
        {data.map((item: any) => {
          return (
            <ProductItem 
              key={item.id}
              id={item.id}
              name={item.name}
              img={item.images[0].url}
              // img={shoesImg}
              price={item.price}
              rating={parseFloat(item.rating)}
              percentOff={50}
              status={
                parseInt(item.countInStock) > 0 ? 'Available' : 'Out of Stock'
              }
            />
          );
        })}
      </div>
    </div>
  );
};
