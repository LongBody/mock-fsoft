import React, { Fragment } from 'react';
import { Row, Col, Image } from 'antd';
import './style.scss';
import image from 'img/test-img.jpg';

export const ProductCarousel: React.FC<any> = () => {
  return (
    <Fragment>
      <div>
        <Row>
          <Col span={24}>
            <Image className="lg-img" height={300} width={822} src={image} />
          </Col>
        </Row>
        <Row gutter={[10, 10]}>
          <Col span={8}>
            <Image height={200} src={image} />
          </Col>
          <Col span={8}>
            <Image height={200} src={image} />
          </Col>
          <Col span={8}>
            <Image height={200} src={image} />
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};
