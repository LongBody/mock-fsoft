import {
  getListCategoryRequest,
  getListCategorySuccess,
  getListCategoryFailure,
  getListProductRequest,
  getListProductSuccess,
  getListProductFailure
} from 'app/pages/user/home-page/screen/action';
import { categoryListEnum, productListEnum } from 'app/pages/user/home-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';


export function* getCategoryListSaga({
  payload,
}: ReturnType<typeof getListCategoryRequest>) {
  try {
    let url = '';
    if (payload?.status) {
      url = `${API_URL}${API_CALL?.API_GET_PRODUCT_CATEGORIES}`;
    } else {
      url = `${API_URL}${API_CALL?.API_GET_PRODUCT_CATEGORIES}`;
    }
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(getListCategorySuccess(response?.data));
  } catch (error: any) {
    yield put(getListCategoryFailure(error?.response, error.message));
  }
}

export function* getProductListSaga({
  payload,
}: ReturnType<typeof getListProductRequest>) {
  try {
    // console.log('in try')
    let url = `${API_URL}${API_CALL?.API_PRODUCTS}`;
    if (payload?.status) {
      url = `${API_URL}${API_CALL?.API_PRODUCTS}?size=${payload?.size}`;
    } else {
      url = `${API_URL}${API_CALL?.API_PRODUCTS}?size=${payload?.size}`;
    }
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(getListProductSuccess(response?.data));
  } catch (error: any) {
    yield put(getListProductFailure(error?.response, error.message));
  }
}



export default function* root() {
  yield all([
    takeLatest(categoryListEnum.GET_LIST_CATEGORY_REQUEST, getCategoryListSaga),
    takeLatest(productListEnum.GET_LIST_PRODUCT_REQUEST, getProductListSaga)
  ]);
}