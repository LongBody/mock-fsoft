import { createReducer } from "@reduxjs/toolkit";
import { categoryListEnum, productListEnum } from "app/pages/user/home-page/screen/types";

export const initialState: any = {
  loading: false,
  message: '',
  status: '',
  dataResponse: {}
};

export const categoryListReducer = createReducer(initialState, (builder) => {
  builder.addCase(categoryListEnum.GET_LIST_CATEGORY_REQUEST, (state, action) => {
    state.dataResponse = {};
    state.message = '';
    state.status = '';
    state.loading = true;
    return state;
  })
  .addCase(categoryListEnum.GET_LIST_CATEGORY_SUCCESS, (state, action: any) => {
    state.dataResponse = action?.payload?.data;
    state.message = action?.payload?.message;
    state.status = action?.payload?.status;
    state.loading = false;
    return state;
  })
  .addCase(categoryListEnum.GET_LIST_CATEGORY_FAILURE, (state, action: any) => {
    state.status = action?.payload?.status;
    state.loading = false;
    return state;
  })
})

export const productListReducer = createReducer(initialState, (builder) => {
  builder.addCase(productListEnum.GET_LIST_PRODUCT_REQUEST, (state, action) => {
    state.dataResponse = {};
    state.message = '';
    state.status = '';
    state.loading = true;
    return state;
  })
  .addCase(productListEnum.GET_LIST_PRODUCT_SUCCESS, (state, action: any) => {
    state.dataResponse = action?.payload?.data;
    state.message = action?.payload?.message;
    state.status = action?.payload?.status;
    state.loading = false;
    return state;
  })
  .addCase(productListEnum.GET_LIST_PRODUCT_FAILURE, (state, action: any) => {
    state.status = action?.payload?.status;
    state.loading = false;
    return state;
  })
})