import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet-async';
import { Navbar } from 'app/components/navbar';
import { SideBar } from '../../../../components/side-bar/index';
import { ProductCarousel } from 'app/pages/user/home-page/base/product-carousel';
import { ProductItem } from '../../../../components/product-items';
import './style.scss';
import shoesImg from 'img/shoes.jpg';
import { BestsellerList } from 'app/pages/user/home-page/base/bestseller-list';

export const HomePage: React.FC<any> = () => {
  return (
    <Fragment>
      <Helmet>
        <title>Home page</title>
        <meta name="description" content="Home page" />
      </Helmet>

      <Navbar />
      <div className="container">
        <div className="container-header">
          <SideBar />
          <ProductCarousel />
        </div>
        <div className="freeshipping-container">
          <div className="freeshipping-item">
            <i className="fa-solid fa-dolly fa-2xl"></i>
            <div className="content-container">
              <div className="title">Free Shipping</div>
              <div className="content">For orders from 50%</div>
            </div>
          </div>
          <div className="freeshipping-item">
            <i className="fa-solid fa-dolly fa-2xl"></i>
            <div className="content-container">
              <div className="title">Free Shipping</div>
              <div className="content">For orders from 50%</div>
            </div>
          </div>
          <div className="freeshipping-item">
            <i className="fa-solid fa-dolly fa-2xl"></i>
            <div className="content-container">
              <div className="title">Free Shipping</div>
              <div className="content">For orders from 50%</div>
            </div>
          </div>
          <div className="freeshipping-item">
            <i className="fa-solid fa-dolly fa-2xl"></i>
            <div className="content-container">
              <div className="title">Free Shipping</div>
              <div className="content">For orders from 50%</div>
            </div>
          </div>
        </div>
        <BestsellerList />
      </div>
      <footer></footer>
    </Fragment>
  );
};
