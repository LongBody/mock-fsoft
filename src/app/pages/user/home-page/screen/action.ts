import { createAction } from '@reduxjs/toolkit';
import {
  categoryListEnum,
  productListEnum,
} from 'app/pages/user/home-page/screen/types';
import { actionPayload } from 'helper/index';

export const getListCategoryRequest = createAction<any>(
  categoryListEnum.GET_LIST_CATEGORY_REQUEST
);
export const getListCategorySuccess = createAction(
  categoryListEnum.GET_LIST_CATEGORY_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getListCategoryFailure = createAction(
  categoryListEnum.GET_LIST_CATEGORY_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

export const getListProductRequest = createAction<any>(
  productListEnum.GET_LIST_PRODUCT_REQUEST
);
export const getListProductSuccess = createAction(
  productListEnum.GET_LIST_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getListProductFailure = createAction(
  productListEnum.GET_LIST_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);
