import { createAction } from '@reduxjs/toolkit';
import { searchingProductEnum, searchingByKeywordProductEnum } from 'app/pages/user/product-page/screen/types';
import { actionPayload } from 'helper/index';

export const getCategoryProductRequest = createAction<any>(
  searchingProductEnum.GET_CATEGORY_PRODUCT_REQUEST
);
export const getCategoryProductSuccess = createAction(
  searchingProductEnum.GET_CATEGORY_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getCategoryProductFailure = createAction(
  searchingProductEnum.GET_CATEGORY_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

export const getKeywordProductRequest = createAction<any>(
  searchingByKeywordProductEnum.GET_KEYWORD_PRODUCT_REQUEST
);
export const getKeywordProductSuccess = createAction(
  searchingByKeywordProductEnum.GET_KEYWORD_PRODUCT_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getKeywordProductFailure = createAction(
  searchingByKeywordProductEnum.GET_KEYWORD_PRODUCT_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);
