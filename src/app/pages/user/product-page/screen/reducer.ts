import { createReducer } from '@reduxjs/toolkit';
import { searchingProductEnum, searchingByKeywordProductEnum } from 'app/pages/user/product-page/screen/types';

export const initialState: any = {
  loading: false,
  message: '',
  status: '',
  dataResponse: {}, 
};

export const categoryProductReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(searchingProductEnum.GET_CATEGORY_PRODUCT_REQUEST, (state, action) => {
      state.dataResponse = {};
      state.message = '';
      state.status = '';
      state.loading = true;
      return state;
    })
    .addCase(
      searchingProductEnum.GET_CATEGORY_PRODUCT_SUCCESS,
      (state, action: any) => {
        state.dataResponse = action?.payload?.data;
        state.message = action?.payload?.message;
        state.status = action?.payload?.status;
        state.loading = false;
        return state;
      }
    )
    .addCase(
      searchingProductEnum.GET_CATEGORY_PRODUCT_FAILURE,
      (state, action: any) => {
        state.status = action?.payload?.status;
        state.loading = false;
        return state;
      }
    );
});

export const keywordProductReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(searchingByKeywordProductEnum.GET_KEYWORD_PRODUCT_REQUEST, (state, action) => {
      state.dataResponse = {};
      state.message = '';
      state.status = '';
      state.loading = true;
      return state;
    })
    .addCase(
      searchingByKeywordProductEnum.GET_KEYWORD_PRODUCT_SUCCESS,
      (state, action: any) => {
        state.dataResponse = action?.payload?.data;
        state.message = action?.payload?.message;
        state.status = action?.payload?.status;
        state.loading = false;
        return state;
      }
    )
    .addCase(
      searchingByKeywordProductEnum.GET_KEYWORD_PRODUCT_FAILURE,
      (state, action: any) => {
        state.status = action?.payload?.status;
        state.loading = false;
        return state;
      }
    );
});
