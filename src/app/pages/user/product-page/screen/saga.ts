import {
  getCategoryProductRequest,
  getCategoryProductSuccess,
  getCategoryProductFailure,
  getKeywordProductRequest,
  getKeywordProductSuccess,
  getKeywordProductFailure
} from 'app/pages/user/product-page/screen/action';
import { searchingProductEnum, searchingByKeywordProductEnum } from 'app/pages/user/product-page/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';


export function* getCategoryProductSaga({
  payload,
}: ReturnType<typeof getCategoryProductRequest>) {
  try {
    // console.log(payload)
    let url = '';
    if (payload?.status) {
      url = `${API_URL}${API_CALL?.API_PRODUCTS}?category=${payload?.category}`;
    } else {
      url = `${API_URL}${API_CALL?.API_PRODUCTS}?category=${payload?.category}`;
    }
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(getCategoryProductSuccess(response?.data));
  } catch (error: any) {
    yield put(getCategoryProductFailure(error?.response, error.message));
  }
}

export function* getKeywordProductSaga({
  payload,
}: ReturnType<typeof getKeywordProductRequest>) {
  try {
    console.log(payload)
    let url = '';
    if (payload?.status) {
      url = `${API_URL}${API_CALL?.API_SEARCH_PRODUCT}?keyword=${payload?.keyword}`;
    } else {
      url = `${API_URL}${API_CALL?.API_SEARCH_PRODUCT}?keyword=${payload?.keyword}`;
    }
    const response = yield call(requestAPIWithToken, url, apiMethod.GET, '');
    yield put(getKeywordProductSuccess(response?.data));
  } catch (error: any) {
    yield put(getKeywordProductFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(searchingProductEnum.GET_CATEGORY_PRODUCT_REQUEST, getCategoryProductSaga),
    takeLatest(searchingByKeywordProductEnum.GET_KEYWORD_PRODUCT_REQUEST, getKeywordProductSaga)
  ]);
}