import React, { Fragment, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { Navbar } from 'app/components/navbar';
import { Row, Col, Spin } from 'antd';
import { SideBar } from 'app/components/side-bar/index';
import { ProductItem } from '../../../../components/product-items';
import './style.scss';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types';
import {
  getCategoryProductRequest,
  getKeywordProductRequest,
} from 'app/pages/user/product-page/screen/action';
import shoesImg from 'img/shoes.jpg';

// const productData = [
//     {
//       id: 123,
//       name: 'Adidas Shoes',
//       img: shoesImg,
//       price: 120,
//       percentOff: 50,
//       status: 'Available',
//     },
//     {
//       id: 123,
//       name: 'Adidas Shoes',
//       img: shoesImg,
//       price: 120,
//       percentOff: 50,
//       status: 'Available',
//     },
//     // {
//     //   id: 123,
//     //   name: 'Adidas Shoes',
//     //   img: shoesImg,
//     //   price: 120,
//     //   percentOff: 50,
//     //   status: 'Available',
//     // },
//     // {
//     //   id: 123,
//     //   name: 'Adidas Shoes',
//     //   img: shoesImg,
//     //   price: 120,
//     //   percentOff: 50,
//     //   status: 'Available',
//     // },
//   ];

export const ProductPage: React.FC<any> = () => {
  const categoryState = useSelector(
    (state: RootState) => state?.categoryProductReducer
  );

  const keywordState = useSelector(
    (state: RootState) => state?.keywordProductReducer
  );

  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const category = urlParams.get('category');
  const keyword = urlParams.get('search');

  useEffect(() => {
    if (category) {
      dispatch(getCategoryProductRequest({ category }));
    } else if (keyword) {
      dispatch(getKeywordProductRequest({ keyword }));
    }
  }, [category, keyword]);

  useEffect(() => {
    if (categoryState?.dataResponse?.result !== undefined) {
      setData(categoryState?.dataResponse?.result);
    } else {
      setData([]);
    }
  }, [categoryState]);

  useEffect(() => {
    if (keywordState?.dataResponse?.products?.result !== undefined) {
      setData(keywordState?.dataResponse?.products?.result);
    } else {
      setData([]);
    }
  }, [keywordState]);

  return (
    <Fragment>
      <Helmet>
        <title>Home page</title>
        <meta name="description" content="Home page" />
      </Helmet>

      <Navbar searchValue={keyword} />
      <div className="container">
        <Spin
          spinning={categoryState?.loading || keywordState?.loading}
          delay={100}
        >
          <div className="container-header">
            <SideBar />
            <div className="list-item-container">
              <Row gutter={[0, 32]}>
                {data.map((item: any) => {
                  return (
                    <Col span={8}>
                      <ProductItem
                        key={item.id}
                        id={item.id}
                        name={item.name}
                        // img={shoesImg}
                        img={item.images[0].url}
                        price={item.price}
                        rating={parseFloat(item.rating)}
                        percentOff={50}
                        status={
                          parseInt(item.countInStock) > 0
                            ? 'Available'
                            : 'Out of Stock'
                        }
                      />
                    </Col>
                  );
                })}
              </Row>
            </div>
          </div>
        </Spin>
      </div>
      <footer></footer>
    </Fragment>
  );
};
