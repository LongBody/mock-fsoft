import { Redirect, Route } from 'react-router-dom';
import { NotFoundPage } from 'app/pages/user/not-found-page/screen/Loadable';
import { HomePage } from 'app/pages/user/home-page/screen/Loadable';
import { ProductDetailPage } from './product-detail-page/screen';
import { ShoppingCartPage } from './shopping-cart/screen';
import { CheckoutPage } from './checkout-page/screen';
import { MyAccountPage } from './my-account-page/screen';
import { ProductPage } from './product-page/screen';

export const PrivateUserRoute: React.FC<any> = (props) => {
  // const token = getCookie('token');
  const token = localStorage.getItem('accessToken');
  const userInfoCookies = localStorage.getItem('user');
  // const userInfoCookies = getCookie('user-info');
  let userInfo: any;

  if (userInfoCookies) {
    userInfo = JSON.parse(atob(userInfoCookies));
  }

  console.log(userInfo);
  

  const { component: Component, ...restProps } = props;

  if (!Component) return null;

  return (
    <Route
      {...restProps}
      render={(routeRenderProps) =>
        (token && userInfo?.role == 'user') ||
        (token && userInfo?.role == 'admin') ? (
          <Component {...routeRenderProps} />
        ) : (
          <Redirect
            to={{
              pathname: '/',
              state: { from: routeRenderProps.location },
            }}
          />
        )
      }
    />
  );
};

export const userRouter = [
  { path: '/', component: HomePage, private: false },
  // { component: NotFoundPage, private: false },
  { path: '/detail', component: ProductDetailPage, private: false },
  { path: '/shoppingcart', component: ShoppingCartPage, private: true },
  { path: '/checkout', component: CheckoutPage, private: true },
  { path: '/my-account', component: MyAccountPage, private: true },
  { path: '/product', component: ProductPage, private: false },
];
