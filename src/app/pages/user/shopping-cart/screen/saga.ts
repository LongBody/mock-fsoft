import {
  getDetailCartRequest,
  getDetailCartSuccess,
  getDetailCartFailure,
  getMyCartRequest,
  getMyCartSuccess,
  getMyCartFailure,
  updateItemCartRequest,
  updateItemCartSuccess,
  updateItemCartFailure,
  deleteItemCartRequest,
  deleteItemCartSuccess,
  deleteItemCartFailure,
  deleteCartRequest,
  deleteCartSuccess,
  deleteCartFailure,
  updateCartRequest,
  updateCartSuccess,
  updateCartFailure,
  createOrderRequest,
  createOrderSuccess,
  createOrderFailure,
} from 'app/pages/user/shopping-cart/screen/action';
import { userCartEnum } from 'app/pages/user/shopping-cart/screen/types';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { apiMethod, requestAPIWithToken } from 'utils/request';
import { API_URL } from 'utils/config';
import { API_CALL } from 'utils/api';

/**
 * get my cart
 *
 * @param {Object} action
 *
 */

export function* getMyCartSaga({
  payload,
}: ReturnType<typeof getMyCartRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_GET_MY_CART}`,
      apiMethod.GET,
      ''
    );
    yield put(getMyCartSuccess(response?.data));
  } catch (error: any) {
    yield put(getMyCartFailure(error?.response, error.message));
  }
}

/**
 * get detail cart
 *
 * @param {Object} action
 *
 */

export function* getDetailCartSaga({
  payload,
}: ReturnType<typeof getDetailCartRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_DETAIL_CART}/${payload?.id}`,
      apiMethod.GET,
      ''
    );
    yield put(getDetailCartSuccess(response?.data));
  } catch (error: any) {
    yield put(getDetailCartFailure(error?.response, error.message));
  }
}

/**
 * update item cart
 *
 * @param {Object} action
 *
 */

export function* updateItemCartSaga({
  payload,
}: ReturnType<typeof updateItemCartRequest>) {
  try {
    const body = {
      quantity: payload?.quantity,
      total: payload?.total,
    };
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_MANAGE_ITEM_CART}/${payload?.id}`,
      apiMethod.PATCH,
      body
    );
    yield put(updateItemCartSuccess(response?.data));
  } catch (error: any) {
    yield put(updateItemCartFailure(error?.response, error.message));
  }
}

/**
 * update cart
 *
 * @param {Object} action
 *
 */

export function* updateCartSaga({
  payload,
}: ReturnType<typeof updateCartRequest>) {
  try {
    const body = {
      totalPrice: payload?.total,
    };
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_DETAIL_CART}/${payload?.id}`,
      apiMethod.PATCH,
      body
    );
    yield put(updateCartSuccess(response?.data));
  } catch (error: any) {
    yield put(updateCartFailure(error?.response, error.message));
  }
}

/**
 * delete item cart
 *
 * @param {Object} action
 *
 */

export function* deleteItemCartSaga({
  payload,
}: ReturnType<typeof deleteItemCartRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_MANAGE_ITEM_CART}/${payload?.id}`,
      apiMethod.DELETE,
      ''
    );
    yield put(deleteItemCartSuccess(response?.data));
  } catch (error: any) {
    yield put(deleteItemCartFailure(error?.response, error.message));
  }
}

/**
 * delete cart
 *
 * @param {Object} action
 *
 */

export function* deleteCartSaga({
  payload,
}: ReturnType<typeof deleteCartRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_DETAIL_CART}/${payload?.id}`,
      apiMethod.DELETE,
      ''
    );
    yield put(deleteCartSuccess(response?.data));
  } catch (error: any) {
    yield put(deleteCartFailure(error?.response, error.message));
  }
}

/**
 * create order
 *
 * @param {Object} action
 *
 */

export function* createOrderSaga({
  payload,
}: ReturnType<typeof createOrderRequest>) {
  try {
    const response = yield call(
      requestAPIWithToken,
      `${API_URL}${API_CALL?.API_GET_ORDERS}`,
      apiMethod.POST,
      payload
    );
    yield put(createOrderSuccess(response?.data));
  } catch (error: any) {
    yield put(createOrderFailure(error?.response, error.message));
  }
}

export default function* root() {
  yield all([
    takeLatest(userCartEnum.GET_MY_CART_REQUEST, getMyCartSaga),
    takeLatest(userCartEnum.GET_DETAIL_CART_REQUEST, getDetailCartSaga),
    takeLatest(userCartEnum.UPDATE_ITEM_CART_REQUEST, updateItemCartSaga),
    takeLatest(userCartEnum.UPDATE_CART_REQUEST, updateCartSaga),
    takeLatest(userCartEnum.DELETE_CART_REQUEST, deleteCartSaga),
    takeLatest(userCartEnum.DELETE_ITEM_CART_REQUEST, deleteItemCartSaga),
    takeLatest(userCartEnum.CREATE_ORDER_REQUEST, createOrderSaga),
  ]);
}
