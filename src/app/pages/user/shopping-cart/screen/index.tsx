import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Button, Col, Input, Modal, Row, Spin, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { CustomBreadcrumb } from 'app/components/breadcrumb';
import { Navbar } from 'app/components/navbar';
import {
  clearStateCart,
  clearStateCartItem,
  deleteCartRequest,
  deleteItemCartRequest,
  getDetailCartRequest,
  getMyCartRequest,
  updateCartRequest,
  updateItemCartRequest,
} from 'app/pages/user/shopping-cart/screen/action';
import React, { Fragment, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types';
import './style.scss';

const price = 120;
const shipping = 20;
const total = price + shipping;

interface DataType {
  key: string;
  img: string;
  product: string;
  price: number;
  quantity: number;
  total: number;
}

const buttonStyle = {
  fontWeight: 'bold',
  color: '#212529',
  backgroundColor: '#ffd333',
  borderColor: '#ffd333',
  borderRadius: '5px',
  padding: '0px 20px',
};

export const ShoppingCartPage: React.FC<any> = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const state = useSelector((state: RootState) => state?.userCartReducer);
  // const [record, setRecord]: any = useState({});

  const dataSource = state?.item?.length > 0 ? state?.item : [];

  const columns: ColumnsType<DataType> = [
    {
      title: 'Image',
      dataIndex: 'img',
      key: 'img',
      render: (img, record: any) => (
        <img
          src={record?.itemCartInfo?.images[0]?.url}
          style={{ height: 60, width: 60 }}
        />
      ),
    },
    {
      title: 'Product',
      dataIndex: 'product',
      key: 'product',
      render: (product, record: any) => (
        <span>{record?.itemCartInfo?.name}</span>
      ),
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      render: (price) => <p>${price.toFixed(1)}</p>,
    },
    {
      title: 'Quantity',
      key: 'quantity',
      dataIndex: 'quantity',
      align: 'center',
      width: '10%',
      render: (quantity, record: any) => {
        return (
          <div
            style={{
              display: 'flex',
              width: 105,
              height: 35,
              justifyContent: 'space-between',
              border: '1px solid #959595',
            }}
          >
            <span
              style={{
                display: 'flex',
                width: 35,
                justifyContent: 'center',
                alignItems: 'center',
                fontSize: 18,
                color: '#33A0FF',
                fontWeight: 'bold',
                cursor: 'pointer',
              }}
              onClick={() => {
                const newQuantity: any = record?.quantity - 1;
                let totalPrice = 0;
                dataSource?.map((item: any) => {
                  if (item?.id === record?.id) {
                    totalPrice += parseInt(newQuantity) * record?.price;
                  } else totalPrice += parseInt(item?.quantity) * item?.price;
                });
                // delete own cart
                if (record?.quantity === 1 && dataSource?.length === 1) {
                  Modal.confirm({
                    title: 'Confirm!',
                    icon: <ExclamationCircleOutlined />,
                    content: 'Are you sure to delete this item from your cart!',
                    okText: 'Ok',
                    cancelText: 'Cancel',
                    onOk: () => {
                      dispatch(
                        deleteCartRequest({
                          id: state?.cart?.id,
                        })
                      );
                    },
                  });
                } else if (record?.quantity === 1 && dataSource?.length > 1) {
                  Modal.confirm({
                    title: 'Confirm!',
                    icon: <ExclamationCircleOutlined />,
                    content: 'Are you sure to delete this item from your cart!',
                    okText: 'Ok',
                    cancelText: 'Cancel',
                    onOk: () => {
                      dispatch(
                        deleteItemCartRequest({
                          id: record?.id,
                        })
                      );
                      dispatch(
                        updateCartRequest({
                          id: state?.cart?.id,
                          total: totalPrice,
                        })
                      );
                    },
                  });
                } else {
                  dispatch(
                    updateItemCartRequest({
                      id: record?.id,
                      total: newQuantity * record?.price,
                      quantity: newQuantity,
                    })
                  );
                  dispatch(
                    updateCartRequest({
                      id: state?.cart?.id,
                      total: totalPrice,
                    })
                  );
                }
              }}
            >
              -
            </span>
            <span
              style={{
                display: 'flex',
                width: 35,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              {quantity}
            </span>
            <span
              style={{
                display: 'flex',
                width: 35,
                justifyContent: 'center',
                alignItems: 'center',
                fontWeight: 'bold',
                color: '#33A0FF',
                cursor: 'pointer',
              }}
              onClick={() => {
                const newQuantity = record?.quantity + 1;
                let totalPrice = 0;
                dataSource?.map((item: any) => {
                  if (item?.id === record?.id) {
                    totalPrice += parseInt(newQuantity) * record?.price;
                  } else totalPrice += parseInt(item?.quantity) * item?.price;
                });
                if (record?.id) {
                  dispatch(
                    updateItemCartRequest({
                      id: record?.id,
                      total: newQuantity * record?.price,
                      quantity: newQuantity,
                    })
                  );
                  dispatch(
                    updateCartRequest({
                      id: state?.cart?.id,
                      total: totalPrice,
                    })
                  );
                }
              }}
            >
              +
            </span>
          </div>
        );
      },
    },
    {
      title: 'Total',
      key: 'total',
      align: 'center',
      dataIndex: 'total',
      render: (num) => <p>${num.toFixed(1)}</p>,
    },
  ];

  useEffect(() => {
    if (
      (state?.message === 'updated' &&
        state.messageCartTotalUpdate === 'cart-updated') ||
      (state?.message === 'deleted' &&
        state.messageCartTotalUpdate === 'cart-updated')
    ) {
      dispatch(clearStateCart(''));
      if (state?.idCart) {
        dispatch(
          getDetailCartRequest({
            id: state?.idCart,
          })
        );
      }
    }
    if (state?.message === 'deleted-cart') {
      dispatch(clearStateCartItem(''));
      dispatch(getMyCartRequest(''));
    }
  }, [state?.message, state.messageCartTotalUpdate]);

  return (
    <Fragment>
      <Helmet>
        <title>Shopping Cart Page</title>
        <meta name="description" content="Shopping Cart Page" />
      </Helmet>

      <Navbar />
      <CustomBreadcrumb currentPage="Shopping Cart" />
      <Spin spinning={state?.loading} delay={100}>
        {dataSource?.length > 0 ? (
          <div className="container__cart">
            <h2>Shopping Cart</h2>
            <div>
              <Table
                columns={columns}
                dataSource={dataSource}
                pagination={false}
                scroll={{ x: 500 }}
              />
            </div>
            <div className="below-container">
              <Row
                style={{
                  width: '100%',
                }}
              >
                {' '}
                <Col xl={12} xs={24} style={{
                  paddingBottom:10
                }}>
                  <div className="coupon-container">
                    <div>
                      <Input
                        placeholder="Coupon Code"
                        style={{ borderRadius: '5px', width: '200px' }}
                      />
                    </div>
                    <div className="coupon-btn">
                      <Button type="primary" style={buttonStyle}>
                        Apply Coupon
                      </Button>
                    </div>
                  </div>
                </Col>
                <Col xl={12} xs={24} style={{
                  paddingBottom:10
                }}>
                  <div className="checkout-info-container">
                    <p className="title">Cart Totals</p>
                    <div className="content">
                      <p className="title">Subtotal</p>
                      <p className="value">
                        ${state?.cart?.totalPrice?.toFixed(1)}
                      </p>
                    </div>
                    <div className="content">
                      <p className="title">Shipping</p>
                      <p className="value">$0</p>
                    </div>
                    <div className="content">
                      <p className="total-title">Total</p>
                      <p className="value bold">
                        ${state?.cart?.totalPrice?.toFixed(1)}
                      </p>
                    </div>
                    <div className="checkout-btn">
                      <Button
                        type="primary"
                        style={{
                          ...buttonStyle,
                          width: '80%',
                          fontSize: '20px',
                          padding: '20px 0px',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                        onClick={() => {
                          history.push('/checkout');
                        }}
                      >
                        Proceed to checkout
                      </Button>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        ) : (
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 30,
              fontSize: 20,
              fontWeight: 'bold',
            }}
          >
            You don't have any product in your cart!
          </div>
        )}
        <footer className="footer"></footer>
      </Spin>
    </Fragment>
  );
};
