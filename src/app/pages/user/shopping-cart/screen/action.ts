import { createAction } from '@reduxjs/toolkit';
import { userCartEnum } from 'app/pages/user/shopping-cart/screen/types';
import { actionPayload } from 'helper/index';

// get my cart

export const getMyCartRequest = createAction<any>(
  userCartEnum.GET_MY_CART_REQUEST
);
export const getMyCartSuccess = createAction(
  userCartEnum.GET_MY_CART_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getMyCartFailure = createAction(
  userCartEnum.GET_MY_CART_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// get detail cart

export const getDetailCartRequest = createAction<any>(
  userCartEnum.GET_DETAIL_CART_REQUEST
);
export const getDetailCartSuccess = createAction(
  userCartEnum.GET_DETAIL_CART_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const getDetailCartFailure = createAction(
  userCartEnum.GET_DETAIL_CART_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// update item cart

export const updateItemCartRequest = createAction<any>(
  userCartEnum.UPDATE_ITEM_CART_REQUEST
);
export const updateItemCartSuccess = createAction(
  userCartEnum.UPDATE_ITEM_CART_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updateItemCartFailure = createAction(
  userCartEnum.UPDATE_ITEM_CART_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// update cart

export const updateCartRequest = createAction<any>(
  userCartEnum.UPDATE_CART_REQUEST
);
export const updateCartSuccess = createAction(
  userCartEnum.UPDATE_CART_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const updateCartFailure = createAction(
  userCartEnum.UPDATE_CART_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// delete item cart

export const deleteItemCartRequest = createAction<any>(
  userCartEnum.DELETE_ITEM_CART_REQUEST
);
export const deleteItemCartSuccess = createAction(
  userCartEnum.DELETE_ITEM_CART_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const deleteItemCartFailure = createAction(
  userCartEnum.DELETE_ITEM_CART_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// delete own cart

export const deleteCartRequest = createAction<any>(
  userCartEnum.DELETE_CART_REQUEST
);
export const deleteCartSuccess = createAction(
  userCartEnum.DELETE_CART_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const deleteCartFailure = createAction(
  userCartEnum.DELETE_CART_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// create order

export const createOrderRequest = createAction<any>(
  userCartEnum.CREATE_ORDER_REQUEST
);
export const createOrderSuccess = createAction(
  userCartEnum.CREATE_ORDER_SUCCESS,
  (payload: Record<string, any>[]) => actionPayload(payload)
);
export const createOrderFailure = createAction(
  userCartEnum.CREATE_ORDER_FAILURE,
  (payload: string, query: string) => actionPayload(payload, { query })
);

// clear state user cart item

export const clearStateCartItem = createAction<any>(
  userCartEnum.CLEAR_USER_CART_ITEM_STATE
);

// clear state user cart

export const clearStateCart = createAction<any>(
  userCartEnum.CLEAR_USER_CART_STATE
);
