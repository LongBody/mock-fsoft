import { createReducer } from '@reduxjs/toolkit';
import { userCartEnum } from 'app/pages/user/shopping-cart/screen/types';
// The initial state of the App
export const initialState: any = {
  loading: false,
  loadingBtnCreateOrder: false,
  message: '',
  cart: {},
  item: {},
  idCart: '',
  status: '',
  messageCartTotalUpdate: '',
};

export const userCartReducer = createReducer(initialState, (builder) => {
  builder

    // get my cart
    .addCase(userCartEnum.GET_MY_CART_REQUEST, (state, action) => {
      state.status = '';
      state.loading = true;
      return state;
    })
    .addCase(userCartEnum.GET_MY_CART_SUCCESS, (state, action: any) => {
      const cart = action?.payload?.data?.carts?.result;
      const currentCart = cart[0];
      if (currentCart?.id) {
        state.loading = true;
      } else {
        state.loading = false;
      }

      state.idCart = currentCart?.id;
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(userCartEnum.GET_MY_CART_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.loading = false;
      return state;
    })

    // get detail cart
    .addCase(userCartEnum.GET_DETAIL_CART_REQUEST, (state, action) => {
      state.status = '';
      state.loading = true;
      return state;
    })
    .addCase(userCartEnum.GET_DETAIL_CART_SUCCESS, (state, action: any) => {
      state.loading = false;
      state.message = '';
      state.item = action?.payload?.data?.items;
      state.cart = action?.payload?.data?.cart;
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(userCartEnum.GET_DETAIL_CART_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.loading = false;
      return state;
    })

    // update item cart
    .addCase(userCartEnum.UPDATE_ITEM_CART_REQUEST, (state, action) => {
      state.status = '';
      state.message = '';
      state.loading = true;
      return state;
    })
    .addCase(userCartEnum.UPDATE_ITEM_CART_SUCCESS, (state, action: any) => {
      state.loading = true;
      state.message = 'updated';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(userCartEnum.UPDATE_ITEM_CART_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.loading = false;
      return state;
    })

    // update cart
    .addCase(userCartEnum.UPDATE_CART_REQUEST, (state, action) => {
      state.status = '';
      state.messageCartTotalUpdate = '';
      state.loading = true;
      return state;
    })
    .addCase(userCartEnum.UPDATE_CART_SUCCESS, (state, action: any) => {
      state.loading = true;
      state.messageCartTotalUpdate = 'cart-updated';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(userCartEnum.UPDATE_CART_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.messageCartTotalUpdate = action?.payload?.data?.message;
      state.loading = false;
      return state;
    })

    // delete item cart
    .addCase(userCartEnum.DELETE_ITEM_CART_REQUEST, (state, action) => {
      state.status = '';
      state.loading = true;
      return state;
    })
    .addCase(userCartEnum.DELETE_ITEM_CART_SUCCESS, (state, action: any) => {
      state.loading = true;
      state.message = 'deleted';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(userCartEnum.DELETE_ITEM_CART_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.loading = false;
      return state;
    })

    // delete own cart
    .addCase(userCartEnum.DELETE_CART_REQUEST, (state, action) => {
      state.status = '';
      state.loading = true;
      return state;
    })
    .addCase(userCartEnum.DELETE_CART_SUCCESS, (state, action: any) => {
      state.loading = true;
      state.message = 'deleted-cart';
      state.idCart = '';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(userCartEnum.DELETE_CART_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.loading = false;
      return state;
    })

    // create order
    .addCase(userCartEnum.CREATE_ORDER_REQUEST, (state, action) => {
      state.status = '';
      state.loadingBtnCreateOrder = true;
      return state;
    })
    .addCase(userCartEnum.CREATE_ORDER_SUCCESS, (state, action: any) => {
      state.loadingBtnCreateOrder = false;
      state.message = 'order-created';
      state.status = action?.payload?.status;
      return state;
    })
    .addCase(userCartEnum.CREATE_ORDER_FAILURE, (state, action: any) => {
      state.status = action?.payload?.data?.status;
      state.message = action?.payload?.data?.message;
      state.loadingBtnCreateOrder = false;
      return state;
    })

    // clear state

    .addCase(userCartEnum.CLEAR_USER_CART_STATE, (state) => {
      state.loading = false;
      state.message = '';
      state.messageCartTotalUpdate = '';
      state.status = '';
      state.messageSucess = '';
      return state;
    })

    // clear cart and item

    .addCase(userCartEnum.CLEAR_USER_CART_ITEM_STATE, (state) => {
      state.item = {};
      state.cart = {};
      state.loading = false;
      state.message = '';
      state.messageCartTotalUpdate = '';
      state.status = '';
      state.messageSucess = '';
      return state;
    });
});
